# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to root", type: :routing do

  it "routing get / to root#index" do
    expect(get: "/").to route_to(controller: "root", action: "index")
  end

end
