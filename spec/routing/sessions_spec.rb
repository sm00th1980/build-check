# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to session", type: :routing do

  it "routing get /login to sessions#new" do
    expect(get: "/login").to route_to(controller: "sessions", action: "new")
  end

  it "routing post /login to sessions#create" do
    expect(post: "/login").to route_to(controller: "sessions", action: "create")
  end

  it "routing delete /logout to sessions#destroy" do
    expect(delete: "/logout").to route_to(controller: "sessions", action: "destroy")
  end

end
