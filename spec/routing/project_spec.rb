# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to project", type: :routing do

  #new
  it "routing get /project/new to project#new" do
    expect(get: "/project/new").to route_to(controller: "project", action: "new")
  end

  #create
  it "routing post /project to /project#create" do
    expect(post: "/project").to route_to(controller: "project", action: "create")
  end

  #edit
  it "routing get /project/:id to project#edit" do
    expect(get: "/project/31").to route_to(controller: "project", action: "edit", id: "31")
  end

  #update
  it "routing post /project/:id to project#update" do
    expect(post: "/project/31").to route_to(controller: "project", action: "update", id: "31")
  end

  #destroy
  it "routing delete /project/:id to project#destroy" do
    expect(delete: "/project/31").to route_to(controller: "project", action: "destroy", id: "31")
  end

  #upload
  it "routing post /project/:id/upload to project#upload" do
    expect(post: "/project/31/upload").to route_to(controller: "project/upload", action: "upload", id: "31")
  end

  #check
  it "routing get /project/check to project#check" do
    expect(get: "/project/check").to route_to(controller: "project/check", action: "check")
  end

  #show
  it "routing get /project/:id/show to project#show" do
    expect(get: "/project/31/show").to route_to(controller: "project/show", action: "index", id: "31")
  end

end
