# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "last version with 2 length" do
    before(:all) do
      @project = create(:project, last_version: '1.1')

      expect(@project).to_not be_nil
      expect(@project.last_version).to eq '1.1'
    end

    it "should be valid last version with 2 length" do
      expect(@project.valid?).to eq true
    end

  end

  describe "last version with 3 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24')

      expect(@project).to_not be_nil
      expect(@project.last_version).to eq '1.0.24'
    end

    it "should be valid last version with 3 length" do
      expect(@project.valid?).to eq true
    end

  end

  describe "last version with 4 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24.1')

      expect(@project).to_not be_nil
      expect(@project.last_version).to eq '1.0.24.1'
    end

    it "should be valid last version with 4 length" do
      expect(@project.valid?).to eq true
    end

  end

  describe "last version with 5 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24.1.34')

      expect(@project).to_not be_nil
      expect(@project.last_version).to eq '1.0.24.1.34'
    end

    it "should be valid last version with 5 length" do
      expect(@project.valid?).to eq true
    end

  end

  describe "last version with 6 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24.1.34.7')

      expect(@project).to_not be_nil
      expect(@project.last_version).to eq '1.0.24.1.34.7'
    end

    it "should be valid last version with 6 length" do
      expect(@project.valid?).to eq true
    end

  end

  describe "last version with 7 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24.1.34.7.9')

      expect(@project).to_not be_nil
      expect(@project.last_version).to eq '1.0.24.1.34.7.9'
    end

    it "should be valid last version with 7 length" do
      expect(@project.valid?).to eq true
    end

  end

end
