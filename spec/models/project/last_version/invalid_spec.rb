# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "last version with string" do
    before(:all) do
      @project = create(:project)
      expect(@project).to_not be_nil
    end

    it "should be invalid last version with string" do
      @project.last_version = 'bvjkdfnjkvfd'

      expect(@project.save).to eq false
      expect(@project.valid?).to eq false
      expect(@project.errors.full_messages).to eq [I18n.t('validation.project.last_version_is_invalid') % @project.last_version]
    end

    it "should be invalid last version with blank" do
      @project.last_version = ''

      expect(@project.save).to eq false
      expect(@project.valid?).to eq false
      expect(@project.errors.full_messages).to eq [I18n.t('validation.project.last_version_is_invalid') % @project.last_version]
    end

    it "should be invalid last version with nil" do
      @project.last_version = nil

      expect(@project.save).to eq false
      expect(@project.valid?).to eq false
      expect(@project.errors.full_messages).to eq [I18n.t('validation.project.last_version_is_invalid') % @project.last_version]
    end

    it "should be invalid last version with -1" do
      @project.last_version = -1

      expect(@project.save).to eq false
      expect(@project.valid?).to eq false
      expect(@project.errors.full_messages).to eq [I18n.t('validation.project.last_version_is_invalid') % @project.last_version]
    end

    it "should be invalid last version with 1." do
      @project.last_version = '1.'

      expect(@project.save).to eq false
      expect(@project.valid?).to eq false
      expect(@project.errors.full_messages).to eq [I18n.t('validation.project.last_version_is_invalid') % @project.last_version]
    end

  end

end
