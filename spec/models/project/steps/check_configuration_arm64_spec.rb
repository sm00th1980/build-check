# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "with check_configuration_arm64" do
    before(:all) do
      @project = create(:project_mimedia, check_configuration_arm64: true)

      expect(@project).to_not be_nil
      expect(@project.check_configuration_arm64).to eq true
    end

    it "should include step_37" do
      expect(@project.steps.include?(AppstoreCheck.step_37)).to eq true
    end

  end

  describe "without check_configuration_arm64" do
    before(:all) do
      @project = create(:project_mimedia, check_configuration_arm64: false)

      expect(@project).to_not be_nil
      expect(@project.check_configuration_arm64).to eq false
    end

    it "should not include step_37" do
      expect(@project.steps.include?(AppstoreCheck.step_37)).to eq false
    end

  end

end
