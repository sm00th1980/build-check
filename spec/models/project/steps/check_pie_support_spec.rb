# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "with check_pie_support" do
    before(:all) do
      @project = create(:project_mimedia, check_pie_support: true)

      expect(@project).to_not be_nil
      expect(@project.check_pie_support).to eq true
    end

    it "should include step_20" do
      expect(@project.steps.include?(AppstoreCheck.step_20)).to eq true
    end

  end

  describe "without check_pie_support" do
    before(:all) do
      @project = create(:project_mimedia, check_pie_support: false)

      expect(@project).to_not be_nil
      expect(@project.check_pie_support).to eq false
    end

    it "should not include step_20" do
      expect(@project.steps.include?(AppstoreCheck.step_20)).to eq false
    end

  end

end
