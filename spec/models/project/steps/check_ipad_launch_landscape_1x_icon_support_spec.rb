# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "with check_ipad_launch_landscape_1x_icon_support" do
    before(:all) do
      @project = create(:project_mimedia, check_ipad_launch_landscape_1x_icon_support: true)

      expect(@project).to_not be_nil
      expect(@project.check_ipad_launch_landscape_1x_icon_support).to eq true
    end

    it "should include step_36" do
      expect(@project.steps.include?(AppstoreCheck.step_36)).to eq true
    end

  end

  describe "without check_ipad_launch_landscape_1x_icon_support" do
    before(:all) do
      @project = create(:project_mimedia, check_ipad_launch_landscape_1x_icon_support: false)

      expect(@project).to_not be_nil
      expect(@project.check_ipad_launch_landscape_1x_icon_support).to eq false
    end

    it "should not include step_34" do
      expect(@project.steps.include?(AppstoreCheck.step_36)).to eq false
    end

  end

end
