# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "with check_armv7_support" do
    before(:all) do
      @project = create(:project_mimedia, check_armv7_support: true)

      expect(@project).to_not be_nil
      expect(@project.check_armv7_support).to eq true
    end

    it "should include step_22" do
      expect(@project.steps.include?(AppstoreCheck.step_22)).to eq true
    end

  end

  describe "without check_armv7_support" do
    before(:all) do
      @project = create(:project_mimedia, check_armv7_support: false)

      expect(@project).to_not be_nil
      expect(@project.check_armv7_support).to eq false
    end

    it "should not include step_22" do
      expect(@project.steps.include?(AppstoreCheck.step_22)).to eq false
    end

  end

end
