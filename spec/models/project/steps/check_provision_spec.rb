# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "with check_provision" do
    before(:all) do
      @project = create(:project_mimedia, check_provision: true)

      expect(@project).to_not be_nil
      expect(@project.check_provision).to eq true
    end

    it "should include step_4" do
      expect(@project.steps.include?(AppstoreCheck.step_4)).to eq true
    end

  end

  describe "without check_provision" do
    before(:all) do
      @project = create(:project_mimedia, check_provision: false)

      expect(@project).to_not be_nil
      expect(@project.check_provision).to eq false
    end

    it "should not include step_4" do
      expect(@project.steps.include?(AppstoreCheck.step_4)).to eq false
    end

  end

end
