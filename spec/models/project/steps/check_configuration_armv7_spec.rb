# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "with check_configuration_armv7" do
    before(:all) do
      @project = create(:project_mimedia, check_configuration_armv7: true)

      expect(@project).to_not be_nil
      expect(@project.check_configuration_armv7).to eq true
    end

    it "should include step_3" do
      expect(@project.steps.include?(AppstoreCheck.step_3)).to eq true
    end

  end

  describe "without check_configuration_armv7" do
    before(:all) do
      @project = create(:project_mimedia, check_configuration_armv7: false)

      expect(@project).to_not be_nil
      expect(@project.check_configuration_armv7).to eq false
    end

    it "should not include step_3" do
      expect(@project.steps.include?(AppstoreCheck.step_3)).to eq false
    end

  end

end
