# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "app_dir" do
    before(:all) do
      @node = create(:node)
      expect(@node).to_not be_nil
      expect(@node.upload_dir).to_not be_nil

      @project = create(:project, app_name: 'MiMedia')

      expect(@project).to_not be_nil
      expect(@project.app_name).to eq 'MiMedia'
    end

    it "should has correct app_dir" do
      expect(@project.app_dir).to eq "#{@node.upload_dir}/#{@project.app_name}.app"
    end

  end

end
