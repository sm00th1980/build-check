# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project, type: :model do

  describe "short version 3 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24')

      expect(@project).to_not be_nil
      expect(@project.last_version).to_not be_nil
    end

    it "should has correct short version for 3 length" do
      expect(@project.short_version).to eq '1.0.24'
    end

  end

  describe "short version 4 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24.1')

      expect(@project).to_not be_nil
      expect(@project.last_version).to_not be_nil
    end

    it "should has correct short version for 4 length" do
      expect(@project.short_version).to eq '1.0.24'
    end

  end

  describe "short version 5 length" do
    before(:all) do
      @project = create(:project, last_version: '1.0.24.1.6')

      expect(@project).to_not be_nil
      expect(@project.last_version).to_not be_nil
    end

    it "should has correct short version for 5 length" do
      expect(@project.short_version).to eq '1.0.24'
    end

  end

end
