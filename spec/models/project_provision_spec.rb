# -*- encoding : utf-8 -*-
require 'rails_helper'

RSpec.describe ProjectProvision, type: :model do

  describe "provision" do
    before(:all) do
      @node = create(:node)
      expect(@node).to_not be_nil
      expect(@node.upload_dir).to_not be_nil

      @project = create(:project, app_name: 'MiMedia')

      expect(@project).to_not be_nil
      expect(@project.app_name).to eq 'MiMedia'

      @provision = create(:project_provision, project: @project)
      expect(@provision).to_not be_nil
    end

    it "should has correct path" do
      expect(@provision.path).to eq "#{@node.upload_dir}/#{@project.app_name}.app/embedded.mobileprovision"
    end

  end
end
