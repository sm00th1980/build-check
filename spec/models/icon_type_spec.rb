# -*- encoding : utf-8 -*-
require 'rails_helper'

describe IconType, type: :model do

  before(:all) do
    IconType.delete_all
  end

  describe "iphone_1x" do

    before(:all) do
      create(:icon_type_iphone_1x)
      @type = IconType.iphone_1x
    end

    it "should iphone_1x be iphone_1x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_1x'
      expect(@type.size).to eq '60x60'
      expect(@type.iphone_1x?).to eq true
    end

  end

  describe "iphone_2x" do

    before(:all) do
      create(:icon_type_iphone_2x)
      @type = IconType.iphone_2x
    end

    it "should iphone_2x be iphone_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_2x'
      expect(@type.size).to eq '120x120'
      expect(@type.iphone_2x?).to eq true
    end

  end

  describe "iphone_3x" do

    before(:all) do
      create(:icon_type_iphone_3x)
      @type = IconType.iphone_3x
    end

    it "should iphone_3x be iphone_3x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_3x'
      expect(@type.size).to eq '180x180'
      expect(@type.iphone_3x?).to eq true
    end

  end

  describe "ipad_1x" do

    before(:all) do
      create(:icon_type_ipad_1x)
      @type = IconType.ipad_1x
    end

    it "should ipad_1x be ipad_1x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_1x'
      expect(@type.size).to eq '76x76'
      expect(@type.ipad_1x?).to eq true
    end

  end

  describe "ipad_2x" do

    before(:all) do
      create(:icon_type_ipad_2x)
      @type = IconType.ipad_2x
    end

    it "should ipad_2x be ipad_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_2x'
      expect(@type.size).to eq '152x152'
      expect(@type.ipad_2x?).to eq true
    end

  end

  describe "iphone_settings_1x" do

    before(:all) do
      create(:icon_type_iphone_settings_1x)
      @type = IconType.iphone_settings_1x
    end

    it "should iphone_settings_1x be iphone_settings_1x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_settings_1x'
      expect(@type.size).to eq '29x29'
      expect(@type.iphone_settings_1x?).to eq true
    end

  end

  describe "iphone_settings_2x" do

    before(:all) do
      create(:icon_type_iphone_settings_2x)
      @type = IconType.iphone_settings_2x
    end

    it "should iphone_settings_2x be iphone_settings_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_settings_2x'
      expect(@type.size).to eq '58x58'
      expect(@type.iphone_settings_2x?).to eq true
    end

  end

  describe "ipad_settings_1x" do

    before(:all) do
      create(:icon_type_ipad_settings_1x)
      @type = IconType.ipad_settings_1x
    end

    it "should ipad_settings_1x be ipad_settings_1x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_settings_1x'
      expect(@type.size).to eq '29x29'
      expect(@type.ipad_settings_1x?).to eq true
    end

  end

  describe "ipad_settings_2x" do

    before(:all) do
      create(:icon_type_ipad_settings_2x)
      @type = IconType.ipad_settings_2x
    end

    it "should ipad_settings_2x be ipad_settings_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_settings_2x'
      expect(@type.size).to eq '58x58'
      expect(@type.ipad_settings_2x?).to eq true
    end

  end

  describe "iphone_launch_1x" do

    before(:all) do
      create(:icon_type_iphone_launch_1x)
      @type = IconType.iphone_launch_1x
    end

    it "should iphone_launch_1x be iphone_launch_1x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_launch_1x'
      expect(@type.size).to eq '320x480'
      expect(@type.iphone_launch_1x?).to eq true
    end

  end

  describe "iphone_launch_2x" do

    before(:all) do
      create(:icon_type_iphone_launch_2x)
      @type = IconType.iphone_launch_2x
    end

    it "should iphone_launch_2x be iphone_launch_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_launch_2x'
      expect(@type.size).to eq '640x960'
      expect(@type.iphone_launch_2x?).to eq true
    end

  end

  describe "iphone_launch_568h_2x" do

    before(:all) do
      create(:icon_type_iphone_launch_568h_2x)
      @type = IconType.iphone_launch_568h_2x
    end

    it "should iphone_launch_568h_2x be iphone_launch_568h_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_launch_568h_2x'
      expect(@type.size).to eq '640x1136'
      expect(@type.iphone_launch_568h_2x?).to eq true
    end

  end

  describe "iphone_launch_667h_2x" do

    before(:all) do
      create(:icon_type_iphone_launch_667h_2x)
      @type = IconType.iphone_launch_667h_2x
    end

    it "should iphone_launch_667h_2x be iphone_launch_667h_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_launch_667h_2x'
      expect(@type.size).to eq '750x1334'
      expect(@type.iphone_launch_667h_2x?).to eq true
    end

  end

  describe "iphone_launch_portrait_736h_3x" do

    before(:all) do
      create(:icon_type_iphone_launch_portrait_736h_3x)
      @type = IconType.iphone_launch_portrait_736h_3x
    end

    it "should iphone_launch_portrait_736h_3x be iphone_launch_portrait_736h_3x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_launch_portrait_736h_3x'
      expect(@type.size).to eq '1242x2208'
      expect(@type.iphone_launch_portrait_736h_3x?).to eq true
    end

  end

  describe "iphone_launch_landscape_736h_3x" do

    before(:all) do
      create(:icon_type_iphone_launch_landscape_736h_3x)
      @type = IconType.iphone_launch_landscape_736h_3x
    end

    it "should iphone_launch_landscape_736h_3x be iphone_launch_landscape_736h_3x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'iphone_launch_landscape_736h_3x'
      expect(@type.size).to eq '2208x1242'
      expect(@type.iphone_launch_landscape_736h_3x?).to eq true
    end

  end

  describe "ipad_launch_portrait_1x" do

    before(:all) do
      create(:icon_type_ipad_launch_portrait_1x)
      @type = IconType.ipad_launch_portrait_1x
    end

    it "should ipad_launch_portrait_1x be ipad_launch_portrait_1x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_launch_portrait_1x'
      expect(@type.size).to eq '768x1024'
      expect(@type.ipad_launch_portrait_1x?).to eq true
    end

  end

  describe "ipad_launch_landscape_1x" do

    before(:all) do
      create(:icon_type_ipad_launch_landscape_1x)
      @type = IconType.ipad_launch_landscape_1x
    end

    it "should ipad_launch_landscape_1x be ipad_launch_landscape_1x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_launch_landscape_1x'
      expect(@type.size).to eq '1024x768'
      expect(@type.ipad_launch_landscape_1x?).to eq true
    end

  end

  describe "ipad_launch_portrait_2x" do

    before(:all) do
      create(:icon_type_ipad_launch_portrait_2x)
      @type = IconType.ipad_launch_portrait_2x
    end

    it "should ipad_launch_portrait_2x be ipad_launch_portrait_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_launch_portrait_2x'
      expect(@type.size).to eq '1536x2048'
      expect(@type.ipad_launch_portrait_2x?).to eq true
    end

  end

  describe "ipad_launch_landscape_2x" do

    before(:all) do
      create(:icon_type_ipad_launch_landscape_2x)
      @type = IconType.ipad_launch_landscape_2x
    end

    it "should ipad_launch_landscape_2x be ipad_launch_landscape_2x" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'ipad_launch_landscape_2x'
      expect(@type.size).to eq '2048x1536'
      expect(@type.ipad_launch_landscape_2x?).to eq true
    end

  end

end
