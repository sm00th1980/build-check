# -*- encoding : utf-8 -*-
require 'rails_helper'

describe User, type: :model do

  describe "has_project?" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.projects.empty?).to eq false
    end

    it "should has project? to be true" do
      expect(@user.has_projects?).to eq true
    end
  end

  describe "has_project?" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      @user.projects.destroy_all
      expect(@user.projects.empty?).to eq true
    end

    it "should has project? to be false" do
      expect(@user.has_projects?).to eq false
    end
  end

end
