# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Node, type: :model do

  describe "appstore node" do

    before(:all) do
      Node.delete_all
      @nodes = 5.times.collect { create(:node) }
      expect(@nodes.empty?).to eq false
    end

    it "should appstore_node be first" do
      expect(Node.appstore_node).to eq @nodes.first
      expect(Node.appstore_node.disabled?).to eq false
    end
  end

end
