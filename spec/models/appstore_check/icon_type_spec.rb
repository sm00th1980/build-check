# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "icon_type filled" do

    before(:all) do
      @step = AppstoreCheck.step_24

      expect(@step).to_not be_nil
      expect(@step.icon_type_id).to_not be_nil
    end

    it "should step with filled icon_type_id has icon_type" do
      expect(@step.icon_type).to_not be_nil
      expect(@step.icon_type).to eq IconType.find_by(id: @step.icon_type_id)
    end

  end

  describe "icon_type blank" do

    before(:all) do
      @step = AppstoreCheck.step_1

      expect(@step).to_not be_nil
      expect(@step.icon_type_id).to be_nil
    end

    it "should step with blank icon_type_id has not icon_type" do
      expect(@step.icon_type).to be_nil
    end

  end

end
