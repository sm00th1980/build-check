# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 31" do

    before(:all) do
      @step_31 = AppstoreCheck.step_31
    end

    it "should step_31 be step_31" do
      expect(@step_31).to_not be_nil
      expect(@step_31.internal_name).to eq 'step_31'
      expect(@step_31.klass).to eq 'Steps::Step_31'
      expect(@step_31.sort).to eq 31
    end

  end

end
