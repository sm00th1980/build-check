# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 20" do

    before(:all) do
      @step_20 = AppstoreCheck.step_20
    end

    it "should step_20 be step_20" do
      expect(@step_20).to_not be_nil
      expect(@step_20.internal_name).to eq 'step_20'
      expect(@step_20.klass).to eq 'Steps::Step_20'
      expect(@step_20.sort).to eq 20
    end

  end

end
