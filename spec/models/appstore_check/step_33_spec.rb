# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 33" do

    before(:all) do
      @step_33 = AppstoreCheck.step_33
    end

    it "should step_33 be step_33" do
      expect(@step_33).to_not be_nil
      expect(@step_33.internal_name).to eq 'step_33'
      expect(@step_33.klass).to eq 'Steps::Step_33'
      expect(@step_33.sort).to eq 33
    end

  end

end
