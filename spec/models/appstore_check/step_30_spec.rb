# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 30" do

    before(:all) do
      @step_30 = AppstoreCheck.step_30
    end

    it "should step_30 be step_30" do
      expect(@step_30).to_not be_nil
      expect(@step_30.internal_name).to eq 'step_30'
      expect(@step_30.klass).to eq 'Steps::Step_30'
      expect(@step_30.sort).to eq 30
    end

  end

end
