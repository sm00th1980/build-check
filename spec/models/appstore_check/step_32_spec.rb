# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 32" do

    before(:all) do
      @step_32 = AppstoreCheck.step_32
    end

    it "should step_32 be step_32" do
      expect(@step_32).to_not be_nil
      expect(@step_32.internal_name).to eq 'step_32'
      expect(@step_32.klass).to eq 'Steps::Step_32'
      expect(@step_32.sort).to eq 32
    end

  end

end
