# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 21" do

    before(:all) do
      @step_21 = AppstoreCheck.step_21
    end

    it "should step_21 be step_21" do
      expect(@step_21).to_not be_nil
      expect(@step_21.internal_name).to eq 'step_21'
      expect(@step_21.klass).to eq 'Steps::Step_21'
      expect(@step_21.sort).to eq 21
    end

  end

end
