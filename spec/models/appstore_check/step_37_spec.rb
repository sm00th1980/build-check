# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 37" do

    before(:all) do
      @step_37 = AppstoreCheck.step_37
    end

    it "should step_37 be step_37" do
      expect(@step_37).to_not be_nil
      expect(@step_37.internal_name).to eq 'step_37'
      expect(@step_37.klass).to eq 'Steps::Step_37'
      expect(@step_37.sort).to eq 37
    end

  end

end
