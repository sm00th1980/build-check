# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 27" do

    before(:all) do
      @step_27 = AppstoreCheck.step_27
    end

    it "should step_27 be step_27" do
      expect(@step_27).to_not be_nil
      expect(@step_27.internal_name).to eq 'step_27'
      expect(@step_27.klass).to eq 'Steps::Step_27'
      expect(@step_27.sort).to eq 27
    end

  end

end
