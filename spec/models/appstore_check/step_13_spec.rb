# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 13" do

    before(:all) do
      @step_13 = AppstoreCheck.step_13
    end

    it "should step_13 be step_13" do
      expect(@step_13).to_not be_nil
      expect(@step_13.internal_name).to eq 'step_13'
      expect(@step_13.klass).to eq 'Steps::Step_13'
      expect(@step_13.sort).to eq 13
    end

  end

end
