# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 8" do

    before(:all) do
      @step_8 = AppstoreCheck.step_8
    end

    it "should step_7 be step_7" do
      expect(@step_8).to_not be_nil
      expect(@step_8.internal_name).to eq 'step_8'
      expect(@step_8.klass).to eq 'Steps::Step_8'
      expect(@step_8.sort).to eq 8
    end

  end

end
