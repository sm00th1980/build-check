# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "showing_checks" do

    before(:all) do
      expect(AppstoreCheck.prepare).to_not be_nil
      expect(AppstoreCheck.step_1).to_not be_nil
      expect(AppstoreCheck.step_3).to_not be_nil
      expect(AppstoreCheck.step_4).to_not be_nil
      expect(AppstoreCheck.step_5).to_not be_nil
      expect(AppstoreCheck.step_6).to_not be_nil
      expect(AppstoreCheck.step_7).to_not be_nil
      expect(AppstoreCheck.step_8).to_not be_nil
      expect(AppstoreCheck.step_9).to_not be_nil
      expect(AppstoreCheck.step_10).to_not be_nil
      expect(AppstoreCheck.step_11).to_not be_nil
      expect(AppstoreCheck.step_12).to_not be_nil
      expect(AppstoreCheck.step_13).to_not be_nil
      expect(AppstoreCheck.step_14).to_not be_nil
      expect(AppstoreCheck.step_15).to_not be_nil
      expect(AppstoreCheck.step_16).to_not be_nil
      expect(AppstoreCheck.step_19).to_not be_nil
      expect(AppstoreCheck.step_20).to_not be_nil
      expect(AppstoreCheck.step_21).to_not be_nil
      expect(AppstoreCheck.step_22).to_not be_nil
      expect(AppstoreCheck.step_23).to_not be_nil
      expect(AppstoreCheck.step_24).to_not be_nil
      expect(AppstoreCheck.step_25).to_not be_nil
      expect(AppstoreCheck.step_26).to_not be_nil
      expect(AppstoreCheck.step_27).to_not be_nil
      expect(AppstoreCheck.step_28).to_not be_nil
      expect(AppstoreCheck.step_29).to_not be_nil
      expect(AppstoreCheck.step_30).to_not be_nil
      expect(AppstoreCheck.step_31).to_not be_nil
      expect(AppstoreCheck.step_32).to_not be_nil
      expect(AppstoreCheck.step_33).to_not be_nil
      expect(AppstoreCheck.step_34).to_not be_nil
      expect(AppstoreCheck.step_35).to_not be_nil
      expect(AppstoreCheck.step_36).to_not be_nil
      expect(AppstoreCheck.step_37).to_not be_nil

      @project = create(:project)

      @showing_steps = AppstoreCheck.showing_steps(@project)
    end

    it "should showing_steps consiste only steps and have prepare" do
      expect do
        expect(@showing_steps).to_not be_nil
        expect(@showing_steps.empty?).to be false
        expect do
          AppstoreCheck.showing_steps(@project)
        end.not_to raise_error
      end

    end

  end
end
