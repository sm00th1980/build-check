# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "streaming_checks" do

    before(:all) do
      expect(AppstoreCheck.prepare).to_not be_nil
      expect(AppstoreCheck.step_1).to_not be_nil
      expect(AppstoreCheck.step_3).to_not be_nil
      expect(AppstoreCheck.step_4).to_not be_nil
      expect(AppstoreCheck.step_5).to_not be_nil
      expect(AppstoreCheck.step_6).to_not be_nil
      expect(AppstoreCheck.step_7).to_not be_nil
      expect(AppstoreCheck.step_8).to_not be_nil
      expect(AppstoreCheck.step_9).to_not be_nil
      expect(AppstoreCheck.step_10).to_not be_nil
      expect(AppstoreCheck.step_11).to_not be_nil
      expect(AppstoreCheck.step_12).to_not be_nil
      expect(AppstoreCheck.step_13).to_not be_nil
      expect(AppstoreCheck.step_14).to_not be_nil
      expect(AppstoreCheck.step_15).to_not be_nil
      expect(AppstoreCheck.step_16).to_not be_nil
      expect(AppstoreCheck.step_19).to_not be_nil
      expect(AppstoreCheck.step_20).to_not be_nil
      expect(AppstoreCheck.step_21).to_not be_nil
      expect(AppstoreCheck.step_22).to_not be_nil
      expect(AppstoreCheck.step_23).to_not be_nil
      expect(AppstoreCheck.step_24).to_not be_nil
      expect(AppstoreCheck.step_25).to_not be_nil
      expect(AppstoreCheck.step_26).to_not be_nil
      expect(AppstoreCheck.step_27).to_not be_nil
      expect(AppstoreCheck.step_28).to_not be_nil
      expect(AppstoreCheck.step_29).to_not be_nil
      expect(AppstoreCheck.step_30).to_not be_nil
      expect(AppstoreCheck.step_31).to_not be_nil
      expect(AppstoreCheck.step_32).to_not be_nil
      expect(AppstoreCheck.step_33).to_not be_nil
      expect(AppstoreCheck.step_34).to_not be_nil
      expect(AppstoreCheck.step_35).to_not be_nil
      expect(AppstoreCheck.step_36).to_not be_nil
      expect(AppstoreCheck.step_37).to_not be_nil

      @streaming_checks = AppstoreCheck.streaming_checks
    end

    it "should steps consiste only steps and not have prepare" do
      expect(@streaming_checks).to_not be_nil
      expect(@streaming_checks.empty?).to be false
      expect(@streaming_checks).to eq [
                                          AppstoreCheck.step_1,
                                          AppstoreCheck.step_3,
                                          AppstoreCheck.step_4,
                                          AppstoreCheck.step_5,
                                          AppstoreCheck.step_6,
                                          AppstoreCheck.step_7,
                                          AppstoreCheck.step_8,
                                          AppstoreCheck.step_9,
                                          AppstoreCheck.step_10,
                                          AppstoreCheck.step_11,
                                          AppstoreCheck.step_12,
                                          AppstoreCheck.step_13,
                                          AppstoreCheck.step_14,
                                          AppstoreCheck.step_15,
                                          AppstoreCheck.step_16,
                                          AppstoreCheck.step_19,
                                          AppstoreCheck.step_20,
                                          AppstoreCheck.step_21,
                                          AppstoreCheck.step_22,
                                          AppstoreCheck.step_23,
                                          AppstoreCheck.step_24,
                                          AppstoreCheck.step_25,
                                          AppstoreCheck.step_26,
                                          AppstoreCheck.step_27,
                                          AppstoreCheck.step_28,
                                          AppstoreCheck.step_29,
                                          AppstoreCheck.step_30,
                                          AppstoreCheck.step_31,
                                          AppstoreCheck.step_32,
                                          AppstoreCheck.step_33,
                                          AppstoreCheck.step_34,
                                          AppstoreCheck.step_35,
                                          AppstoreCheck.step_36,
                                          AppstoreCheck.step_37
                                      ]
    end

  end

end
