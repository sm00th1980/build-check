# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 24" do

    before(:all) do
      @step_24 = AppstoreCheck.step_24
    end

    it "should step_24 be step_24" do
      expect(@step_24).to_not be_nil
      expect(@step_24.internal_name).to eq 'step_24'
      expect(@step_24.klass).to eq 'Steps::Step_24'
      expect(@step_24.sort).to eq 24
    end

  end

end
