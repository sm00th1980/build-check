# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 28" do

    before(:all) do
      @step_28 = AppstoreCheck.step_28
    end

    it "should step_28 be step_28" do
      expect(@step_28).to_not be_nil
      expect(@step_28.internal_name).to eq 'step_28'
      expect(@step_28.klass).to eq 'Steps::Step_28'
      expect(@step_28.sort).to eq 28
    end

  end

end
