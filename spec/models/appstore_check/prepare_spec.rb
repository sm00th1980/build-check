# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "prepare step" do

    before(:all) do
      @prepare = AppstoreCheck.prepare
    end

    it "should prepare be prepare" do
      expect(@prepare).to_not be_nil
      expect(@prepare.internal_name).to eq 'prepare'
      expect(@prepare.klass).to eq 'Steps::Prepare'
      expect(@prepare.sort).to eq 0
    end

  end

end
