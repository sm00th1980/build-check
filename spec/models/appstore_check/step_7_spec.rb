# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 7" do

    before(:all) do
      @step_7 = AppstoreCheck.step_7
    end

    it "should step_7 be step_7" do
      expect(@step_7).to_not be_nil
      expect(@step_7.internal_name).to eq 'step_7'
      expect(@step_7.klass).to eq 'Steps::Step_7'
      expect(@step_7.sort).to eq 7
    end

  end

end
