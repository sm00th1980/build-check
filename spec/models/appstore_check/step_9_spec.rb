# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 9" do

    before(:all) do
      @step_9 = AppstoreCheck.step_9
    end

    it "should step_9 be step_9" do
      expect(@step_9).to_not be_nil
      expect(@step_9.internal_name).to eq 'step_9'
      expect(@step_9.klass).to eq 'Steps::Step_9'
      expect(@step_9.sort).to eq 9
    end

  end

end
