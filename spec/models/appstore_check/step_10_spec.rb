# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 10" do

    before(:all) do
      @step_10 = AppstoreCheck.step_10
    end

    it "should step_10 be step_10" do
      expect(@step_10).to_not be_nil
      expect(@step_10.internal_name).to eq 'step_10'
      expect(@step_10.klass).to eq 'Steps::Step_10'
      expect(@step_10.sort).to eq 10
    end

  end

end
