# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 14" do

    before(:all) do
      @step_14 = AppstoreCheck.step_14
    end

    it "should step_14 be step_14" do
      expect(@step_14).to_not be_nil
      expect(@step_14.internal_name).to eq 'step_14'
      expect(@step_14.klass).to eq 'Steps::Step_14'
      expect(@step_14.sort).to eq 14
    end

  end

end
