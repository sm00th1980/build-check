# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 25" do

    before(:all) do
      @step_25 = AppstoreCheck.step_25
    end

    it "should step_25 be step_25" do
      expect(@step_25).to_not be_nil
      expect(@step_25.internal_name).to eq 'step_25'
      expect(@step_25.klass).to eq 'Steps::Step_25'
      expect(@step_25.sort).to eq 25
    end

  end

end
