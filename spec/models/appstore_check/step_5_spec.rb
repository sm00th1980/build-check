# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 5" do

    before(:all) do
      @step_5 = AppstoreCheck.step_5
    end

    it "should step_5 be step_5" do
      expect(@step_5).to_not be_nil
      expect(@step_5.internal_name).to eq 'step_5'
      expect(@step_5.klass).to eq 'Steps::Step_5'
      expect(@step_5.sort).to eq 5
    end

  end

end
