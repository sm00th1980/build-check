# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 19" do

    before(:all) do
      @step_19 = AppstoreCheck.step_19
    end

    it "should step_19 be step_19" do
      expect(@step_19).to_not be_nil
      expect(@step_19.internal_name).to eq 'step_19'
      expect(@step_19.klass).to eq 'Steps::Step_19'
      expect(@step_19.sort).to eq 19
    end

  end

end
