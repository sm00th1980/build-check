# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 26" do

    before(:all) do
      @step_26 = AppstoreCheck.step_26
    end

    it "should step_26 be step_26" do
      expect(@step_26).to_not be_nil
      expect(@step_26.internal_name).to eq 'step_26'
      expect(@step_26.klass).to eq 'Steps::Step_26'
      expect(@step_26.sort).to eq 26
    end

  end

end
