# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 6" do

    before(:all) do
      @step_6 = AppstoreCheck.step_6
    end

    it "should step_6 be step_6" do
      expect(@step_6).to_not be_nil
      expect(@step_6.internal_name).to eq 'step_6'
      expect(@step_6.klass).to eq 'Steps::Step_6'
      expect(@step_6.sort).to eq 6
    end

  end

end
