# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 34" do

    before(:all) do
      @step_34 = AppstoreCheck.step_34
    end

    it "should step_34 be step_34" do
      expect(@step_34).to_not be_nil
      expect(@step_34.internal_name).to eq 'step_34'
      expect(@step_34.klass).to eq 'Steps::Step_34'
      expect(@step_34.sort).to eq 34
    end

  end

end
