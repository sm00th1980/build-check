# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 23" do

    before(:all) do
      @step_23 = AppstoreCheck.step_23
    end

    it "should step_23 be step_23" do
      expect(@step_23).to_not be_nil
      expect(@step_23.internal_name).to eq 'step_23'
      expect(@step_23.klass).to eq 'Steps::Step_23'
      expect(@step_23.sort).to eq 23
    end

  end

end
