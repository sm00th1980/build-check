# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 15" do

    before(:all) do
      @step_15 = AppstoreCheck.step_15
    end

    it "should step_15 be step_15" do
      expect(@step_15).to_not be_nil
      expect(@step_15.internal_name).to eq 'step_15'
      expect(@step_15.klass).to eq 'Steps::Step_15'
      expect(@step_15.sort).to eq 15
    end

  end

end
