# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 12" do

    before(:all) do
      @step_12 = AppstoreCheck.step_12
    end

    it "should step_12 be step_12" do
      expect(@step_12).to_not be_nil
      expect(@step_12.internal_name).to eq 'step_12'
      expect(@step_12.klass).to eq 'Steps::Step_12'
      expect(@step_12.sort).to eq 12
    end

  end

end
