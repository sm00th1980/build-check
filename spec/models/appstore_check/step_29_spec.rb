# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 29" do

    before(:all) do
      @step_29 = AppstoreCheck.step_29
    end

    it "should step_29 be step_29" do
      expect(@step_29).to_not be_nil
      expect(@step_29.internal_name).to eq 'step_29'
      expect(@step_29.klass).to eq 'Steps::Step_29'
      expect(@step_29.sort).to eq 29
    end

  end

end
