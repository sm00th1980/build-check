# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 4" do

    before(:all) do
      @step_4 = AppstoreCheck.step_4
    end

    it "should step_4 be step_4" do
      expect(@step_4).to_not be_nil
      expect(@step_4.internal_name).to eq 'step_4'
      expect(@step_4.klass).to eq 'Steps::Step_4'
      expect(@step_4.sort).to eq 4
    end

  end

end
