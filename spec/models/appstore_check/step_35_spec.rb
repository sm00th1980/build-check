# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 35" do

    before(:all) do
      @step_35 = AppstoreCheck.step_35
    end

    it "should step_35 be step_35" do
      expect(@step_35).to_not be_nil
      expect(@step_35.internal_name).to eq 'step_35'
      expect(@step_35.klass).to eq 'Steps::Step_35'
      expect(@step_35.sort).to eq 35
    end

  end

end
