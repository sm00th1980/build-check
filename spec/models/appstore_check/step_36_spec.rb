# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 36" do

    before(:all) do
      @step_36 = AppstoreCheck.step_36
    end

    it "should step_35 be step_35" do
      expect(@step_36).to_not be_nil
      expect(@step_36.internal_name).to eq 'step_36'
      expect(@step_36.klass).to eq 'Steps::Step_36'
      expect(@step_36.sort).to eq 36
    end

  end

end
