# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 3" do

    before(:all) do
      @step_3 = AppstoreCheck.step_3
    end

    it "should step_3 be step_3" do
      expect(@step_3).to_not be_nil
      expect(@step_3.internal_name).to eq 'step_3'
      expect(@step_3.klass).to eq 'Steps::Step_3'
      expect(@step_3.sort).to eq 3
    end

  end

end
