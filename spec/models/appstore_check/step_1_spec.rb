# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 1" do

    before(:all) do
      @step_1 = AppstoreCheck.step_1
    end

    it "should step_1 be step_1" do
      expect(@step_1).to_not be_nil
      expect(@step_1.internal_name).to eq 'step_1'
      expect(@step_1.klass).to eq 'Steps::Step_1'
      expect(@step_1.sort).to eq 1
    end

  end

end
