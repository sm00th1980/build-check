# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 16" do

    before(:all) do
      @step_16 = AppstoreCheck.step_16
    end

    it "should step_16 be step_16" do
      expect(@step_16).to_not be_nil
      expect(@step_16.internal_name).to eq 'step_16'
      expect(@step_16.klass).to eq 'Steps::Step_16'
      expect(@step_16.sort).to eq 16
    end

  end

end
