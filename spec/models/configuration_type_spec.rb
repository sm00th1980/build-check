# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ConfigurationType, type: :model do

  before(:all) do
    ConfigurationType.delete_all
  end

  describe "armv7" do

    before(:all) do
      create(:configuration_type_armv7)
      @type = ConfigurationType.armv7
    end

    it "should armv7 be armv7" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'armv7'
      expect(@type.armv7?).to eq true
      expect(@type.arm64?).to eq false
    end

  end

  describe "arm64" do

    before(:all) do
      create(:configuration_type_arm64)
      @type = ConfigurationType.arm64
    end

    it "should arm64 be arm64" do
      expect(@type).to_not be_nil
      expect(@type.internal_name).to eq 'arm64'
      expect(@type.armv7?).to eq false
      expect(@type.arm64?).to eq true
    end

  end

end
