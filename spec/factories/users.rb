# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user_#{n}@mercdev.com" }
    password '12345678'
    password_confirmation '12345678'

    after(:create) do |user|
      2.times { create(:project, user: user) }
    end
  end
end
