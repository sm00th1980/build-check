# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :appstore_check do
    expected nil
    command nil
    icon_type_id nil

    factory :appstore_check_prepare do
      name 'prepare'
      internal_name 'prepare'
      sort 0
      klass 'Steps::Prepare'
    end

    factory :appstore_check_step_1 do
      name 'step_1'
      internal_name 'step_1'
      sort 1
      klass 'Steps::Step_1'
      command 'codesign --verify --no-strict -vvvv %{app}'
      expected '%{app}: valid on disk
%{app}: satisfies its Designated Requirement
'
    end

    factory :appstore_check_step_3 do
      name 'Check configuration ARMv7'
      command 'otool -L -vh - %{configuration_path}'
      expected '%{configuration_value}'
      sort 3
      internal_name 'step_3'
      klass 'Steps::Step_3'
    end

    factory :appstore_check_step_4 do
      name 'Check provision'
      command 'openssl smime -inform der -verify -noverify -in %{provision_path}'
      expected '%{provision_value}'
      sort 4
      internal_name 'step_4'
      klass 'Steps::Step_4'
    end

    factory :appstore_check_step_5 do
      name 'Check Bundle ID'
      command 'defaults read %{app}/Info.plist CFBundleIdentifier'
      expected "%{bundle_id}\n"
      sort 5
      internal_name 'step_5'
      klass 'Steps::Step_5'
    end

    factory :appstore_check_step_6 do
      name 'Check version'
      command 'defaults read %{app}/Info.plist CFBundleVersion'
      expected "%{version}\n"
      sort 6
      internal_name 'step_6'
      klass 'Steps::Step_6'
    end

    factory :appstore_check_step_7 do
      name 'Check platform version'
      command 'defaults read %{app}/Info.plist DTPlatformVersion'
      expected "%{platform_version}\n"
      sort 7
      internal_name 'step_7'
      klass 'Steps::Step_7'
    end

    factory :appstore_check_step_8 do
      name 'Check platform build'
      command 'defaults read %{app}/Info.plist DTPlatformBuild'
      expected "%{platform_build}\n"
      sort 8
      internal_name 'step_8'
      klass 'Steps::Step_8'
    end

    factory :appstore_check_step_9 do
      name 'Check DTXcode'
      command 'defaults read %{app}/Info.plist DTXcode'
      expected "%{xcode_version}\n"
      sort 9
      internal_name 'step_9'
      klass 'Steps::Step_9'
    end

    factory :appstore_check_step_10 do
      name 'Check DTXcodeBuild'
      command 'defaults read %{app}/Info.plist DTXcodeBuild'
      expected "%{xcode_build}\n"
      sort 10
      internal_name 'step_10'
      klass 'Steps::Step_10'
    end

    factory :appstore_check_step_11 do
      name 'Check iPhone/iPod 2x Icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 11
      internal_name 'step_11'
      klass 'Steps::Step_11'
    end

    factory :appstore_check_step_12 do
      name 'Check iPad 1x icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 12
      internal_name 'step_12'
      klass 'Steps::Step_12'
    end

    factory :appstore_check_step_13 do
      name 'Check iPhone settings 1x icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 13
      internal_name 'step_13'
      klass 'Steps::Step_13'
    end

    factory :appstore_check_step_14 do
      name 'Check iPhone/iPod launch 1x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 14
      internal_name 'step_14'
      klass 'Steps::Step_14'
    end

    factory :appstore_check_step_15 do
      name 'Check iPad launch portrait 1x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 15
      internal_name 'step_15'
      klass 'Steps::Step_15'
    end

    factory :appstore_check_step_16 do
      name 'Check iTunes Artworks are not present'
      command '-'
      expected "iTunesArtwork file is not present\niTunesArtwork@2x file is not present"
      sort 16
      internal_name 'step_16'
      klass 'Steps::Step_16'
    end

    factory :appstore_check_step_19 do
      name 'Check architecture ARM64 support'
      command 'otool -vh %{executable}'
      expected 'Architecture ARM64 has been supported'
      sort 19
      internal_name 'step_19'
      klass 'Steps::Step_19'
    end

    factory :appstore_check_step_20 do
      name 'Check PIE binary'
      command 'otool -vh %{executable}'
      expected "armv7 is present and has PIE binary support\narm64 is present and has PIE binary support"
      sort 20
      internal_name 'step_20'
      klass 'Steps::Step_20'
    end

    factory :appstore_check_step_21 do
      name 'Check short version'
      command 'defaults read %{app}/Info.plist CFBundleShortVersionString'
      expected "%{short_version}\n"
      sort 21
      internal_name 'step_21'
      klass 'Steps::Step_21'
    end

    factory :appstore_check_step_22 do
      name 'Check architecture ARMv7 support'
      command 'otool -vh %{executable}'
      expected 'Architecture ARMv7 has been supported'
      sort 22
      internal_name 'step_22'
      klass 'Steps::Step_22'
    end

    factory :appstore_check_step_23 do
      name 'Check iPhone/iPod 3x Icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 23
      internal_name 'step_23'
      klass 'Steps::Step_23'
    end

    factory :appstore_check_step_24 do
      name 'Check iPhone/iPod 1x Icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 24
      internal_name 'step_24'
      klass 'Steps::Step_24'
      icon_type_id { IconType.iphone_1x.id }
    end

    factory :appstore_check_step_25 do
      name 'Check iPad 2x Icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 25
      internal_name 'step_25'
      klass 'Steps::Step_25'
    end

    factory :appstore_check_step_26 do
      name 'Check iPhone settings 2x icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 26
      internal_name 'step_26'
      klass 'Steps::Step_26'
    end

    factory :appstore_check_step_27 do
      name 'Check iPad settings 1x icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 27
      internal_name 'step_27'
      klass 'Steps::Step_27'
    end

    factory :appstore_check_step_28 do
      name 'Check iPad settings 2x icon'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 28
      internal_name 'step_28'
      klass 'Steps::Step_28'
    end

    factory :appstore_check_step_29 do
      name 'Check iPhone/iPod launch landscape 736h 3x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 29
      internal_name 'step_29'
      klass 'Steps::Step_29'
    end

    factory :appstore_check_step_30 do
      name 'Check iPhone/iPod launch portrait 736h 3x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 30
      internal_name 'step_30'
      klass 'Steps::Step_30'
    end

    factory :appstore_check_step_31 do
      name 'Check iPhone/iPod launch 667h 2x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 31
      internal_name 'step_31'
      klass 'Steps::Step_31'
    end

    factory :appstore_check_step_32 do
      name 'Check iPhone/iPod launch 568h 2x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 32
      internal_name 'step_32'
      klass 'Steps::Step_32'
    end

    factory :appstore_check_step_33 do
      name 'Check iPhone/iPod launch 2x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 33
      internal_name 'step_33'
      klass 'Steps::Step_33'
    end

    factory :appstore_check_step_34 do
      name 'Check iPad launch landscape 2x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 34
      internal_name 'step_34'
      klass 'Steps::Step_34'
    end

    factory :appstore_check_step_35 do
      name 'Check iPad launch portrait 2x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 35
      internal_name 'step_35'
      klass 'Steps::Step_35'
    end

    factory :appstore_check_step_36 do
      name 'Check iPad launch landscape 1x image'
      command '-'
      expected '%{name} (MD5 = %{md5}, SIZE = %{size})'
      sort 36
      internal_name 'step_36'
      klass 'Steps::Step_36'
    end

    factory :appstore_check_step_37 do
      name 'Check configuration ARM64'
      command 'otool -L -vh - %{configuration_path}'
      expected '%{configuration_value}'
      sort 37
      internal_name 'step_37'
      klass 'Steps::Step_37'
    end

  end

end
