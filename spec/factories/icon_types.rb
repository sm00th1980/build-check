# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :icon_type do

    factory :icon_type_iphone_1x do
      name 'iphone_1x'
      internal_name 'iphone_1x'
      size '60x60'
    end

    factory :icon_type_iphone_2x do
      name 'iphone_2x'
      internal_name 'iphone_2x'
      size '120x120'
    end

    factory :icon_type_iphone_3x do
      name 'iphone_3x'
      internal_name 'iphone_3x'
      size '180x180'
    end

    factory :icon_type_ipad_1x do
      name 'ipad_1x'
      internal_name 'ipad_1x'
      size '76x76'
    end

    factory :icon_type_ipad_2x do
      name 'ipad_2x'
      internal_name 'ipad_2x'
      size '152x152'
    end

    factory :icon_type_iphone_settings_1x do
      name 'iphone_settings_1x'
      internal_name 'iphone_settings_1x'
      size '29x29'
    end

    factory :icon_type_iphone_settings_2x do
      name 'iphone_settings_2x'
      internal_name 'iphone_settings_2x'
      size '58x58'
    end

    factory :icon_type_ipad_settings_1x do
      name 'ipad_settings_1x'
      internal_name 'ipad_settings_1x'
      size '29x29'
    end

    factory :icon_type_ipad_settings_2x do
      name 'ipad_settings_2x'
      internal_name 'ipad_settings_2x'
      size '58x58'
    end

    factory :icon_type_iphone_launch_1x do
      name 'iphone_launch_1x'
      internal_name 'iphone_launch_1x'
      size '320x480'
    end

    factory :icon_type_iphone_launch_2x do
      name 'iphone_launch_2x'
      internal_name 'iphone_launch_2x'
      size '640x960'
    end

    factory :icon_type_iphone_launch_568h_2x do
      name 'iphone_launch_568h_2x'
      internal_name 'iphone_launch_568h_2x'
      size '640x1136'
    end

    factory :icon_type_iphone_launch_667h_2x do
      name 'iphone_launch_667h_2x'
      internal_name 'iphone_launch_667h_2x'
      size '750x1334'
    end

    factory :icon_type_iphone_launch_portrait_736h_3x do
      name 'iphone_launch_portrait_736h_3x'
      internal_name 'iphone_launch_portrait_736h_3x'
      size '1242x2208'
    end

    factory :icon_type_iphone_launch_landscape_736h_3x do
      name 'iphone_launch_landscape_736h_3x'
      internal_name 'iphone_launch_landscape_736h_3x'
      size '2208x1242'
    end

    factory :icon_type_ipad_launch_portrait_1x do
      name 'ipad_launch_portrait_1x'
      internal_name 'ipad_launch_portrait_1x'
      size '768x1024'
    end

    factory :icon_type_ipad_launch_landscape_1x do
      name 'ipad_launch_landscape_1x'
      internal_name 'ipad_launch_landscape_1x'
      size '1024x768'
    end

    factory :icon_type_ipad_launch_portrait_2x do
      name 'ipad_launch_portrait_2x'
      internal_name 'ipad_launch_portrait_2x'
      size '1536x2048'
    end

    factory :icon_type_ipad_launch_landscape_2x do
      name 'ipad_launch_landscape_2x'
      internal_name 'ipad_launch_landscape_2x'
      size '2048x1536'
    end

  end

end
