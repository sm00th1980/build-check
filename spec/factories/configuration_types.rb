# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :configuration_type do

    factory :configuration_type_armv7 do
      name 'armv7'
      internal_name 'armv7'
    end

    factory :configuration_type_arm64 do
      name 'arm64'
      internal_name 'arm64'
    end

  end

end
