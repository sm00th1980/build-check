# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :project do
    sequence(:name) { |n| "project_#{n}" }
    sequence(:app_name) { |n| "app_name_#{n}" }
    sequence(:zip_name) { |n| "app_name_#{n}" }
    sequence(:last_version) { |n| "1.0.#{n}" }
    platform_version '9.0'
    platform_build '7A1001'
    xcode_version '0701'
    xcode_build '7A1002'
    bundle_id 'com.mimedia.iOS'
    check_armv7_support false
    check_arm64_support false
    check_pie_support false
    check_iphone_1x_icon_support false
    check_iphone_2x_icon_support false
    check_iphone_3x_icon_support false
    check_ipad_1x_icon_support false
    check_ipad_2x_icon_support false
    check_iphone_settings_1x_icon_support false
    check_iphone_settings_2x_icon_support false
    check_ipad_settings_1x_icon_support false
    check_ipad_settings_2x_icon_support false
    check_iphone_launch_1x_icon_support false
    check_iphone_launch_2x_icon_support false
    check_iphone_launch_568h_2x_icon_support false
    check_iphone_launch_667h_2x_icon_support false
    check_iphone_launch_portrait_736h_3x_icon_support false
    check_iphone_launch_landscape_736h_3x_icon_support false

    check_ipad_launch_portrait_1x_icon_support false
    check_ipad_launch_landscape_1x_icon_support false
    check_ipad_launch_portrait_2x_icon_support false
    check_ipad_launch_landscape_2x_icon_support false

    check_provision false

    check_configuration_armv7 false
    check_configuration_arm64 false

    with_extensions false

    user

    factory :project_mimedia do
      name "MiMedia"
      app_name "MiMedia"
      zip_name "MiMedia"
      last_version "1.0.24"
      platform_version '8.1'
      platform_build '12B411'
      xcode_version '0611'
      xcode_build '6A2008a'
      bundle_id 'com.mimedia.iOSv2'
      check_armv7_support true
      check_arm64_support true
      check_pie_support true
      check_iphone_1x_icon_support false
      check_iphone_2x_icon_support true
      check_iphone_3x_icon_support true
      check_ipad_1x_icon_support true
      check_ipad_2x_icon_support true
      check_iphone_settings_1x_icon_support true
      check_iphone_settings_2x_icon_support true
      check_ipad_settings_1x_icon_support true
      check_ipad_settings_2x_icon_support true
      check_iphone_launch_1x_icon_support true
      check_iphone_launch_2x_icon_support true
      check_iphone_launch_568h_2x_icon_support true
      check_iphone_launch_667h_2x_icon_support true
      check_iphone_launch_portrait_736h_3x_icon_support true
      check_iphone_launch_landscape_736h_3x_icon_support true

      check_ipad_launch_portrait_1x_icon_support true
      check_ipad_launch_landscape_1x_icon_support true
      check_ipad_launch_portrait_2x_icon_support true
      check_ipad_launch_landscape_2x_icon_support true

      check_provision true

      check_configuration_armv7 true
      check_configuration_arm64 true

      after(:create) do |project|
        ProjectIcon.create!(name: 'AppIcon60x60@2x.png', md5: 'e73ebed50450ba4b742c23bfbdcfd49c', type: IconType.iphone_2x, project: project)
        ProjectIcon.create!(name: 'AppIcon60x60@3x.png', md5: 'd676993636dd01b57deb8f8d18df3299', type: IconType.iphone_3x, project: project)

        ProjectIcon.create!(name: 'AppIcon76x76~ipad.png', md5: 'efadc0a3dcfe7d8243ebaed6c05e3ef4', type: IconType.ipad_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon76x76@2x~ipad.png', md5: '73533b632acafccbd51505c506085a76', type: IconType.ipad_2x, project: project)

        ProjectIcon.create!(name: 'AppIcon29x29.png', md5: '6270381645d8a22adab8af5a5f8f7c29', type: IconType.iphone_settings_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29@2x.png', md5: 'bb39a9fbdee6ed15eccf4183ea64cea9', type: IconType.iphone_settings_2x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29~ipad.png', md5: '6270381645d8a22adab8af5a5f8f7c29', type: IconType.ipad_settings_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29@2x~ipad.png', md5: 'bb39a9fbdee6ed15eccf4183ea64cea9', type: IconType.ipad_settings_2x, project: project)

        ProjectIcon.create!(name: 'LaunchImage.png', md5: '89817fb95ab12d69b0f010450ad29428', type: IconType.iphone_launch_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage@2x.png', md5: '278d5a9802193ae28d8698ef5dc7240a', type: IconType.iphone_launch_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-568h@2x.png', md5: '6e41359534fd8a60f4a419c5e2caa429', type: IconType.iphone_launch_568h_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-667h@2x.png', md5: 'b7cf9b3955f9b49ff4ecd39577947861', type: IconType.iphone_launch_667h_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-Portrait-736h@3x.png', md5: '45e1109fedc69abd44a8f3452659feb1', type: IconType.iphone_launch_portrait_736h_3x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-Landscape-736h@3x.png', md5: '3a26e026cf7000a50d98f6ab54310b19', type: IconType.iphone_launch_landscape_736h_3x, project: project)

        ProjectIcon.create!(name: 'LaunchImage-Portrait~ipad.png', md5: 'dcf60180b258defb566de5042ce79bbd', type: IconType.ipad_launch_portrait_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Landscape~ipad.png', md5: '36047b1203297e4c7288092ff3e9aa62', type: IconType.ipad_launch_landscape_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Portrait@2x~ipad.png', md5: '2217217bac18a20935025fe19c27be93', type: IconType.ipad_launch_portrait_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Landscape@2x~ipad.png', md5: 'bf91364ae71b4dab8a0c79e64903b220', type: IconType.ipad_launch_landscape_2x, project: project)

        create(:project_provision_mimedia, project: project)

        create(:project_configuration_armv7_mimedia, project: project)
        create(:project_configuration_arm64_mimedia, project: project)
      end
    end

    factory :project_oddsfinder do
      name "oddsfinder"
      app_name "R-OFer"
      zip_name "OddsFinder"
      last_version "1.0.24"
      platform_version '8.1'
      platform_build '12B411'
      xcode_version '0611'
      xcode_build '6A2008a'
      bundle_id 'com.mimedia.iOSv2'
      check_armv7_support true
      check_arm64_support true
      check_pie_support true
      check_iphone_1x_icon_support false
      check_iphone_2x_icon_support true
      check_iphone_3x_icon_support true
      check_ipad_1x_icon_support true
      check_ipad_2x_icon_support true
      check_iphone_settings_1x_icon_support true
      check_iphone_settings_2x_icon_support true
      check_ipad_settings_1x_icon_support true
      check_ipad_settings_2x_icon_support true
      check_iphone_launch_1x_icon_support true
      check_iphone_launch_2x_icon_support true
      check_iphone_launch_568h_2x_icon_support true
      check_iphone_launch_667h_2x_icon_support true
      check_iphone_launch_portrait_736h_3x_icon_support true
      check_iphone_launch_landscape_736h_3x_icon_support true

      check_ipad_launch_portrait_1x_icon_support true
      check_ipad_launch_landscape_1x_icon_support true
      check_ipad_launch_portrait_2x_icon_support true
      check_ipad_launch_landscape_2x_icon_support true

      check_provision true

      after(:create) do |project|
        ProjectIcon.create!(name: 'AppIcon60x60@2x.png', md5: 'e73ebed50450ba4b742c23bfbdcfd49c', type: IconType.iphone_2x, project: project)
        ProjectIcon.create!(name: 'AppIcon60x60@3x.png', md5: 'd676993636dd01b57deb8f8d18df3299', type: IconType.iphone_3x, project: project)

        ProjectIcon.create!(name: 'AppIcon76x76~ipad.png', md5: 'efadc0a3dcfe7d8243ebaed6c05e3ef4', type: IconType.ipad_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon76x76@2x~ipad.png', md5: '73533b632acafccbd51505c506085a76', type: IconType.ipad_2x, project: project)

        ProjectIcon.create!(name: 'AppIcon29x29.png', md5: '6270381645d8a22adab8af5a5f8f7c29', type: IconType.iphone_settings_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29@2x.png', md5: 'bb39a9fbdee6ed15eccf4183ea64cea9', type: IconType.iphone_settings_2x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29~ipad.png', md5: '6270381645d8a22adab8af5a5f8f7c29', type: IconType.ipad_settings_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29@2x~ipad.png', md5: 'bb39a9fbdee6ed15eccf4183ea64cea9', type: IconType.ipad_settings_2x, project: project)

        ProjectIcon.create!(name: 'LaunchImage.png', md5: '89817fb95ab12d69b0f010450ad29428', type: IconType.iphone_launch_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage@2x.png', md5: '278d5a9802193ae28d8698ef5dc7240a', type: IconType.iphone_launch_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-568h@2x.png', md5: '6e41359534fd8a60f4a419c5e2caa429', type: IconType.iphone_launch_568h_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-667h@2x.png', md5: 'b7cf9b3955f9b49ff4ecd39577947861', type: IconType.iphone_launch_667h_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-Portrait-736h@3x.png', md5: '45e1109fedc69abd44a8f3452659feb1', type: IconType.iphone_launch_portrait_736h_3x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-Landscape-736h@3x.png', md5: '3a26e026cf7000a50d98f6ab54310b19', type: IconType.iphone_launch_landscape_736h_3x, project: project)

        ProjectIcon.create!(name: 'LaunchImage-Portrait~ipad.png', md5: 'dcf60180b258defb566de5042ce79bbd', type: IconType.ipad_launch_portrait_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Landscape~ipad.png', md5: '36047b1203297e4c7288092ff3e9aa62', type: IconType.ipad_launch_landscape_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Portrait@2x~ipad.png', md5: '2217217bac18a20935025fe19c27be93', type: IconType.ipad_launch_portrait_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Landscape@2x~ipad.png', md5: 'bf91364ae71b4dab8a0c79e64903b220', type: IconType.ipad_launch_landscape_2x, project: project)

        create(:project_provision_oddsfinder, project: project)
      end
    end

    factory :project_with_extensions do
      name "MiMedia"
      app_name "MiMedia"
      zip_name "MiMedia"
      last_version "1.0.24"
      platform_version '8.1'
      platform_build '12B411'
      xcode_version '0611'
      xcode_build '6A2008a'
      bundle_id 'com.mimedia.iOSv2'
      check_armv7_support true
      check_arm64_support true
      check_pie_support true
      check_iphone_1x_icon_support false
      check_iphone_2x_icon_support true
      check_iphone_3x_icon_support true
      check_ipad_1x_icon_support true
      check_ipad_2x_icon_support true
      check_iphone_settings_1x_icon_support true
      check_iphone_settings_2x_icon_support true
      check_ipad_settings_1x_icon_support true
      check_ipad_settings_2x_icon_support true
      check_iphone_launch_1x_icon_support true
      check_iphone_launch_2x_icon_support true
      check_iphone_launch_568h_2x_icon_support true
      check_iphone_launch_667h_2x_icon_support true
      check_iphone_launch_portrait_736h_3x_icon_support true
      check_iphone_launch_landscape_736h_3x_icon_support true

      check_ipad_launch_portrait_1x_icon_support true
      check_ipad_launch_landscape_1x_icon_support true
      check_ipad_launch_portrait_2x_icon_support true
      check_ipad_launch_landscape_2x_icon_support true

      check_provision true

      check_configuration_armv7 true
      check_configuration_arm64 true

      with_extensions true

      after(:create) do |project|
        ProjectIcon.create!(name: 'AppIcon60x60@2x.png', md5: 'e73ebed50450ba4b742c23bfbdcfd49c', type: IconType.iphone_2x, project: project)
        ProjectIcon.create!(name: 'AppIcon60x60@3x.png', md5: 'd676993636dd01b57deb8f8d18df3299', type: IconType.iphone_3x, project: project)

        ProjectIcon.create!(name: 'AppIcon76x76~ipad.png', md5: 'efadc0a3dcfe7d8243ebaed6c05e3ef4', type: IconType.ipad_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon76x76@2x~ipad.png', md5: '73533b632acafccbd51505c506085a76', type: IconType.ipad_2x, project: project)

        ProjectIcon.create!(name: 'AppIcon29x29.png', md5: '6270381645d8a22adab8af5a5f8f7c29', type: IconType.iphone_settings_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29@2x.png', md5: 'bb39a9fbdee6ed15eccf4183ea64cea9', type: IconType.iphone_settings_2x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29~ipad.png', md5: '6270381645d8a22adab8af5a5f8f7c29', type: IconType.ipad_settings_1x, project: project)
        ProjectIcon.create!(name: 'AppIcon29x29@2x~ipad.png', md5: 'bb39a9fbdee6ed15eccf4183ea64cea9', type: IconType.ipad_settings_2x, project: project)

        ProjectIcon.create!(name: 'LaunchImage.png', md5: '89817fb95ab12d69b0f010450ad29428', type: IconType.iphone_launch_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage@2x.png', md5: '278d5a9802193ae28d8698ef5dc7240a', type: IconType.iphone_launch_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-568h@2x.png', md5: '6e41359534fd8a60f4a419c5e2caa429', type: IconType.iphone_launch_568h_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-667h@2x.png', md5: 'b7cf9b3955f9b49ff4ecd39577947861', type: IconType.iphone_launch_667h_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-Portrait-736h@3x.png', md5: '45e1109fedc69abd44a8f3452659feb1', type: IconType.iphone_launch_portrait_736h_3x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-800-Landscape-736h@3x.png', md5: '3a26e026cf7000a50d98f6ab54310b19', type: IconType.iphone_launch_landscape_736h_3x, project: project)

        ProjectIcon.create!(name: 'LaunchImage-Portrait~ipad.png', md5: 'dcf60180b258defb566de5042ce79bbd', type: IconType.ipad_launch_portrait_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Landscape~ipad.png', md5: '36047b1203297e4c7288092ff3e9aa62', type: IconType.ipad_launch_landscape_1x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Portrait@2x~ipad.png', md5: '2217217bac18a20935025fe19c27be93', type: IconType.ipad_launch_portrait_2x, project: project)
        ProjectIcon.create!(name: 'LaunchImage-Landscape@2x~ipad.png', md5: 'bf91364ae71b4dab8a0c79e64903b220', type: IconType.ipad_launch_landscape_2x, project: project)

        create(:project_provision_mimedia, project: project)

        create(:project_configuration_armv7_mimedia, project: project)
        create(:project_configuration_arm64_mimedia, project: project)
      end
    end

  end

end
