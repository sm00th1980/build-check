# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :step_file do
    sequence(:name) { |n| "name_#{n}" }
    project
    checked false

    factory :step_file_with_mimedia do
      project { create(:project_mimedia) }
    end

    factory :step_file_with_oddsfinder do
      project { create(:project_oddsfinder) }
    end

    factory :step_file_with_extensions do
      project { create(:project_with_extensions) }
    end

  end

end
