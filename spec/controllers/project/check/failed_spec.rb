# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project::CheckController, type: :controller do

  render_views

  describe "check already checked before step" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      @project = create(:project_mimedia, user: @user, last_version: '1.0.24', app_name: 'MiMedia')

      @step_file = create(:step_file, checked: true, project: @project, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip').to_s)
      expect(@step_file).to_not be_nil

      expect(@step_file.checked).to eq true
      expect(@step_file.project.provision).to_not be_nil
    end

    it "should not check already checked step_file" do
      sign_in @user
      get :check, {id: @step_file.id}
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)).to eq ({"success" => false, "error" => (I18n.t('step_file.failure.not_found_or_already_checked') % @step_file.id)})
      expect(@step_file.reload.checked).to eq true
    end

  end

  describe "check not existing step" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
    end

    it "should show error if not existing id" do
      sign_in @user
      get :check, {id: -1}
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)).to eq ({"success" => false, "error" => (I18n.t('step_file.failure.not_found_or_already_checked') % -1)})
    end
  end

end
