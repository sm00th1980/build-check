# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project::CheckController, type: :controller do

  render_views

  describe "GET check steps" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      @project = create(:project_mimedia, user: @user, last_version: '1.0.24', app_name: 'MiMedia')

      @step_file = create(:step_file, project: @project, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip').to_s)
      expect(@step_file).to_not be_nil

      expect(@step_file.checked).to eq false
      expect(@step_file.project.provision).to_not be_nil
    end

    it "should redirect to new_session if non-auth user" do
      get :check, {id: @step_file.id}
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show checking steps" do
      sign_in @user
      get :check, {id: @step_file.id}
      expect(response).to have_http_status(:success)
      expect(response.headers['Content-Type']).to eq 'text/event-stream'
      expect(response.stream.closed?).to be true
      expect(parse(response.body)).to eq result

      expect(@step_file.reload.checked).to eq true
    end

    private
    def result
      step_1_expected = AppstoreCheck.step_1.read_attribute_before_type_cast('expected') % {app: @project.app_path}
      step_1_got = AppstoreCheck.step_1.read_attribute_before_type_cast('expected') % {app: @project.app_path}
      step_1_command = AppstoreCheck.step_1.read_attribute_before_type_cast('command') % {app: @project.app_path}

      configuration = @project.configurations.select { |c| c.type.armv7? }.first
      step_3_expected = AppstoreCheck.step_3.read_attribute_before_type_cast('expected') % {configuration_path: @project.app_path, configuration_value: configuration.value}
      step_3_got = AppstoreCheck.step_3.read_attribute_before_type_cast('expected') % {configuration_path: @project.app_path, configuration_value: configuration.value}
      step_3_command = AppstoreCheck.step_3.read_attribute_before_type_cast('command') % {configuration_path: @project.app_path, configuration_value: configuration.value}

      step_4_expected = AppstoreCheck.step_4.read_attribute_before_type_cast('expected') % {provision_path: @project.provision.path, provision_value: @project.provision.value}
      step_4_got = AppstoreCheck.step_4.read_attribute_before_type_cast('expected') % {provision_path: @project.provision.path, provision_value: @project.provision.value}
      step_4_command = AppstoreCheck.step_4.read_attribute_before_type_cast('command') % {provision_path: @project.provision.path, provision_value: @project.provision.value}

      step_5_expected = AppstoreCheck.step_5.read_attribute_before_type_cast('expected') % {app: @project.app_dir, bundle_id: @project.bundle_id}
      step_5_got = AppstoreCheck.step_5.read_attribute_before_type_cast('expected') % {app: @project.app_dir, bundle_id: @project.bundle_id}
      step_5_command = AppstoreCheck.step_5.read_attribute_before_type_cast('command') % {app: @project.app_dir, bundle_id: @project.bundle_id}

      step_6_expected = AppstoreCheck.step_6.read_attribute_before_type_cast('expected') % {app: @project.app_dir, version: @project.last_version}
      step_6_got = AppstoreCheck.step_6.read_attribute_before_type_cast('expected') % {app: @project.app_dir, version: @project.last_version}
      step_6_command = AppstoreCheck.step_6.read_attribute_before_type_cast('command') % {app: @project.app_dir, version: @project.last_version}

      step_7_expected = AppstoreCheck.step_7.read_attribute_before_type_cast('expected') % {app: @project.app_dir, platform_version: @project.platform_version}
      step_7_got = AppstoreCheck.step_7.read_attribute_before_type_cast('expected') % {app: @project.app_dir, platform_version: @project.platform_version}
      step_7_command = AppstoreCheck.step_7.read_attribute_before_type_cast('command') % {app: @project.app_dir, platform_version: @project.platform_version}

      step_8_expected = AppstoreCheck.step_8.read_attribute_before_type_cast('expected') % {app: @project.app_dir, platform_build: @project.platform_build}
      step_8_got = AppstoreCheck.step_8.read_attribute_before_type_cast('expected') % {app: @project.app_dir, platform_build: @project.platform_build}
      step_8_command = AppstoreCheck.step_8.read_attribute_before_type_cast('command') % {app: @project.app_dir, platform_build: @project.platform_build}

      step_9_expected = AppstoreCheck.step_9.read_attribute_before_type_cast('expected') % {app: @project.app_dir, xcode_version: @project.xcode_version}
      step_9_got = AppstoreCheck.step_9.read_attribute_before_type_cast('expected') % {app: @project.app_dir, xcode_version: @project.xcode_version}
      step_9_command = AppstoreCheck.step_9.read_attribute_before_type_cast('command') % {app: @project.app_dir, xcode_version: @project.xcode_version}

      step_10_expected = AppstoreCheck.step_10.read_attribute_before_type_cast('expected') % {app: @project.app_dir, xcode_build: @project.xcode_build}
      step_10_got = AppstoreCheck.step_10.read_attribute_before_type_cast('expected') % {app: @project.app_dir, xcode_build: @project.xcode_build}
      step_10_command = AppstoreCheck.step_10.read_attribute_before_type_cast('command') % {app: @project.app_dir, xcode_build: @project.xcode_build}

      icon = @project.icons.select { |icon| icon.type.iphone_2x? }.first
      step_11_expected = AppstoreCheck.step_11.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_11_got = AppstoreCheck.step_11.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_11_command = AppstoreCheck.step_11.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_1x? }.first
      step_12_expected = AppstoreCheck.step_12.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_12_got = AppstoreCheck.step_12.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_12_command = AppstoreCheck.step_12.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_settings_1x? }.first
      step_13_expected = AppstoreCheck.step_13.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_13_got = AppstoreCheck.step_13.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_13_command = AppstoreCheck.step_13.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_launch_1x? }.first
      step_14_expected = AppstoreCheck.step_14.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_14_got = AppstoreCheck.step_14.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_14_command = AppstoreCheck.step_14.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_launch_portrait_1x? }.first
      step_15_expected = AppstoreCheck.step_15.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_15_got = AppstoreCheck.step_15.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_15_command = AppstoreCheck.step_15.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      step_16_expected = AppstoreCheck.step_16.read_attribute_before_type_cast('expected')
      step_16_got = AppstoreCheck.step_16.read_attribute_before_type_cast('expected')
      step_16_command = AppstoreCheck.step_16.read_attribute_before_type_cast('command')

      step_19_expected = AppstoreCheck.step_19.read_attribute_before_type_cast('expected') % {executable: @project.app_path}
      step_19_got = AppstoreCheck.step_19.read_attribute_before_type_cast('expected') % {executable: @project.app_path}
      step_19_command = AppstoreCheck.step_19.read_attribute_before_type_cast('command') % {executable: @project.app_path}

      step_20_expected = AppstoreCheck.step_20.read_attribute_before_type_cast('expected') % {executable: @project.app_path}
      step_20_got = AppstoreCheck.step_20.read_attribute_before_type_cast('expected') % {executable: @project.app_path}
      step_20_command = AppstoreCheck.step_20.read_attribute_before_type_cast('command') % {executable: @project.app_path}

      step_21_expected = AppstoreCheck.step_21.read_attribute_before_type_cast('expected') % {app: @project.app_dir, short_version: @project.short_version}
      step_21_got = AppstoreCheck.step_21.read_attribute_before_type_cast('expected') % {app: @project.app_dir, short_version: @project.short_version}
      step_21_command = AppstoreCheck.step_21.read_attribute_before_type_cast('command') % {app: @project.app_dir, short_version: @project.short_version}

      step_22_expected = AppstoreCheck.step_22.read_attribute_before_type_cast('expected') % {executable: @project.app_path}
      step_22_got = AppstoreCheck.step_22.read_attribute_before_type_cast('expected') % {executable: @project.app_path}
      step_22_command = AppstoreCheck.step_22.read_attribute_before_type_cast('command') % {executable: @project.app_path}

      icon = @project.icons.select { |icon| icon.type.iphone_3x? }.first
      step_23_expected = AppstoreCheck.step_23.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_23_got = AppstoreCheck.step_23.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_23_command = AppstoreCheck.step_23.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_2x? }.first
      step_25_expected = AppstoreCheck.step_25.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_25_got = AppstoreCheck.step_25.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_25_command = AppstoreCheck.step_25.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_settings_2x? }.first
      step_26_expected = AppstoreCheck.step_26.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_26_got = AppstoreCheck.step_26.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_26_command = AppstoreCheck.step_26.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_settings_1x? }.first
      step_27_expected = AppstoreCheck.step_27.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_27_got = AppstoreCheck.step_27.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_27_command = AppstoreCheck.step_27.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_settings_2x? }.first
      step_28_expected = AppstoreCheck.step_28.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_28_got = AppstoreCheck.step_28.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_28_command = AppstoreCheck.step_28.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_launch_landscape_736h_3x? }.first
      step_29_expected = AppstoreCheck.step_29.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_29_got = AppstoreCheck.step_29.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_29_command = AppstoreCheck.step_29.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_launch_portrait_736h_3x? }.first
      step_30_expected = AppstoreCheck.step_30.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_30_got = AppstoreCheck.step_30.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_30_command = AppstoreCheck.step_30.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_launch_667h_2x? }.first
      step_31_expected = AppstoreCheck.step_31.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_31_got = AppstoreCheck.step_31.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_31_command = AppstoreCheck.step_31.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_launch_568h_2x? }.first
      step_32_expected = AppstoreCheck.step_32.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_32_got = AppstoreCheck.step_32.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_32_command = AppstoreCheck.step_32.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.iphone_launch_2x? }.first
      step_33_expected = AppstoreCheck.step_33.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_33_got = AppstoreCheck.step_33.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_33_command = AppstoreCheck.step_33.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_launch_landscape_2x? }.first
      step_34_expected = AppstoreCheck.step_34.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_34_got = AppstoreCheck.step_34.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_34_command = AppstoreCheck.step_34.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_launch_portrait_2x? }.first
      step_35_expected = AppstoreCheck.step_35.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_35_got = AppstoreCheck.step_35.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_35_command = AppstoreCheck.step_35.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      icon = @project.icons.select { |icon| icon.type.ipad_launch_landscape_1x? }.first
      step_36_expected = AppstoreCheck.step_36.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_36_got = AppstoreCheck.step_36.read_attribute_before_type_cast('expected') % {name: icon.name, md5: icon.md5, size: icon.type.size}
      step_36_command = AppstoreCheck.step_36.read_attribute_before_type_cast('command') % {name: icon.name, md5: icon.md5, size: icon.type.size}

      configuration = @project.configurations.select { |c| c.type.arm64? }.first
      step_37_expected = AppstoreCheck.step_37.read_attribute_before_type_cast('expected') % {configuration_path: @project.app_path, configuration_value: configuration.value}
      step_37_got = AppstoreCheck.step_37.read_attribute_before_type_cast('expected') % {configuration_path: @project.app_path, configuration_value: configuration.value}
      step_37_command = AppstoreCheck.step_37.read_attribute_before_type_cast('command') % {configuration_path: @project.app_path, configuration_value: configuration.value}

      [
          {"id" => AppstoreCheck.prepare.id, "status" => "running", "name" => AppstoreCheck.prepare.name, "expected" => nil, "command" => "-", "got" => nil},
          {"id" => AppstoreCheck.prepare.id, "status" => "passed", "name" => AppstoreCheck.prepare.name, "expected" => "-", "command" => "-", "got" => "-"},
          {"id" => AppstoreCheck.step_1.id, "status" => "running", "name" => AppstoreCheck.step_1.name, "expected" => nil, "command" => step_1_command, "got" => nil},
          {"id" => AppstoreCheck.step_1.id, "status" => "passed", "name" => AppstoreCheck.step_1.name, "expected" => step_1_expected, "command" => step_1_command, "got" => step_1_got},
          {"id" => AppstoreCheck.step_3.id, "status" => "running", "name" => AppstoreCheck.step_3.name, "expected" => nil, "command" => step_3_command, "got" => nil},
          {"id" => AppstoreCheck.step_3.id, "status" => "passed", "name" => AppstoreCheck.step_3.name, "expected" => step_3_expected, "command" => step_3_command, "got" => step_3_got},
          {"id" => AppstoreCheck.step_4.id, "status" => "running", "name" => AppstoreCheck.step_4.name, "expected" => nil, "command" => step_4_command, "got" => nil},
          {"id" => AppstoreCheck.step_4.id, "status" => "passed", "name" => AppstoreCheck.step_4.name, "expected" => step_4_expected, "command" => step_4_command, "got" => step_4_got},
          {"id" => AppstoreCheck.step_5.id, "status" => "running", "name" => AppstoreCheck.step_5.name, "expected" => nil, "command" => step_5_command, "got" => nil},
          {"id" => AppstoreCheck.step_5.id, "status" => "passed", "name" => AppstoreCheck.step_5.name, "expected" => step_5_expected, "command" => step_5_command, "got" => step_5_got},
          {"id" => AppstoreCheck.step_6.id, "status" => "running", "name" => AppstoreCheck.step_6.name, "expected" => nil, "command" => step_6_command, "got" => nil},
          {"id" => AppstoreCheck.step_6.id, "status" => "passed", "name" => AppstoreCheck.step_6.name, "expected" => step_6_expected, "command" => step_6_command, "got" => step_6_got},
          {"id" => AppstoreCheck.step_7.id, "status" => "running", "name" => AppstoreCheck.step_7.name, "expected" => nil, "command" => step_7_command, "got" => nil},
          {"id" => AppstoreCheck.step_7.id, "status" => "passed", "name" => AppstoreCheck.step_7.name, "expected" => step_7_expected, "command" => step_7_command, "got" => step_7_got},
          {"id" => AppstoreCheck.step_8.id, "status" => "running", "name" => AppstoreCheck.step_8.name, "expected" => nil, "command" => step_8_command, "got" => nil},
          {"id" => AppstoreCheck.step_8.id, "status" => "passed", "name" => AppstoreCheck.step_8.name, "expected" => step_8_expected, "command" => step_8_command, "got" => step_8_got},
          {"id" => AppstoreCheck.step_9.id, "status" => "running", "name" => AppstoreCheck.step_9.name, "expected" => nil, "command" => step_9_command, "got" => nil},
          {"id" => AppstoreCheck.step_9.id, "status" => "passed", "name" => AppstoreCheck.step_9.name, "expected" => step_9_expected, "command" => step_9_command, "got" => step_9_got},
          {"id" => AppstoreCheck.step_10.id, "status" => "running", "name" => AppstoreCheck.step_10.name, "expected" => nil, "command" => step_10_command, "got" => nil},
          {"id" => AppstoreCheck.step_10.id, "status" => "passed", "name" => AppstoreCheck.step_10.name, "expected" => step_10_expected, "command" => step_10_command, "got" => step_10_got},
          {"id" => AppstoreCheck.step_11.id, "status" => "running", "name" => AppstoreCheck.step_11.name, "expected" => nil, "command" => step_11_command, "got" => nil},
          {"id" => AppstoreCheck.step_11.id, "status" => "passed", "name" => AppstoreCheck.step_11.name, "expected" => step_11_expected, "command" => step_11_command, "got" => step_11_got},
          {"id" => AppstoreCheck.step_12.id, "status" => "running", "name" => AppstoreCheck.step_12.name, "expected" => nil, "command" => step_12_command, "got" => nil},
          {"id" => AppstoreCheck.step_12.id, "status" => "passed", "name" => AppstoreCheck.step_12.name, "expected" => step_12_expected, "command" => step_12_command, "got" => step_12_got},
          {"id" => AppstoreCheck.step_13.id, "status" => "running", "name" => AppstoreCheck.step_13.name, "expected" => nil, "command" => step_13_command, "got" => nil},
          {"id" => AppstoreCheck.step_13.id, "status" => "passed", "name" => AppstoreCheck.step_13.name, "expected" => step_13_expected, "command" => step_13_command, "got" => step_13_got},
          {"id" => AppstoreCheck.step_14.id, "status" => "running", "name" => AppstoreCheck.step_14.name, "expected" => nil, "command" => step_14_command, "got" => nil},
          {"id" => AppstoreCheck.step_14.id, "status" => "passed", "name" => AppstoreCheck.step_14.name, "expected" => step_14_expected, "command" => step_14_command, "got" => step_14_got},
          {"id" => AppstoreCheck.step_15.id, "status" => "running", "name" => AppstoreCheck.step_15.name, "expected" => nil, "command" => step_15_command, "got" => nil},
          {"id" => AppstoreCheck.step_15.id, "status" => "passed", "name" => AppstoreCheck.step_15.name, "expected" => step_15_expected, "command" => step_15_command, "got" => step_15_got},
          {"id" => AppstoreCheck.step_16.id, "status" => "running", "name" => AppstoreCheck.step_16.name, "expected" => nil, "command" => step_16_command, "got" => nil},
          {"id" => AppstoreCheck.step_16.id, "status" => "passed", "name" => AppstoreCheck.step_16.name, "expected" => step_16_expected, "command" => step_16_command, "got" => step_16_got},
          {"id" => AppstoreCheck.step_19.id, "status" => "running", "name" => AppstoreCheck.step_19.name, "expected" => nil, "command" => step_19_command, "got" => nil},
          {"id" => AppstoreCheck.step_19.id, "status" => "passed", "name" => AppstoreCheck.step_19.name, "expected" => step_19_expected, "command" => step_19_command, "got" => step_19_got},
          {"id" => AppstoreCheck.step_20.id, "status" => "running", "name" => AppstoreCheck.step_20.name, "expected" => nil, "command" => step_20_command, "got" => nil},
          {"id" => AppstoreCheck.step_20.id, "status" => "passed", "name" => AppstoreCheck.step_20.name, "expected" => step_20_expected, "command" => step_20_command, "got" => step_20_got},
          {"id" => AppstoreCheck.step_21.id, "status" => "running", "name" => AppstoreCheck.step_21.name, "expected" => nil, "command" => step_21_command, "got" => nil},
          {"id" => AppstoreCheck.step_21.id, "status" => "passed", "name" => AppstoreCheck.step_21.name, "expected" => step_21_expected, "command" => step_21_command, "got" => step_21_got},
          {"id" => AppstoreCheck.step_22.id, "status" => "running", "name" => AppstoreCheck.step_22.name, "expected" => nil, "command" => step_22_command, "got" => nil},
          {"id" => AppstoreCheck.step_22.id, "status" => "passed", "name" => AppstoreCheck.step_22.name, "expected" => step_22_expected, "command" => step_22_command, "got" => step_22_got},
          {"id" => AppstoreCheck.step_23.id, "status" => "running", "name" => AppstoreCheck.step_23.name, "expected" => nil, "command" => step_23_command, "got" => nil},
          {"id" => AppstoreCheck.step_23.id, "status" => "passed", "name" => AppstoreCheck.step_23.name, "expected" => step_23_expected, "command" => step_23_command, "got" => step_23_got},
          {"id" => AppstoreCheck.step_25.id, "status" => "running", "name" => AppstoreCheck.step_25.name, "expected" => nil, "command" => step_25_command, "got" => nil},
          {"id" => AppstoreCheck.step_25.id, "status" => "passed", "name" => AppstoreCheck.step_25.name, "expected" => step_25_expected, "command" => step_25_command, "got" => step_25_got},
          {"id" => AppstoreCheck.step_26.id, "status" => "running", "name" => AppstoreCheck.step_26.name, "expected" => nil, "command" => step_26_command, "got" => nil},
          {"id" => AppstoreCheck.step_26.id, "status" => "passed", "name" => AppstoreCheck.step_26.name, "expected" => step_26_expected, "command" => step_26_command, "got" => step_26_got},
          {"id" => AppstoreCheck.step_27.id, "status" => "running", "name" => AppstoreCheck.step_27.name, "expected" => nil, "command" => step_27_command, "got" => nil},
          {"id" => AppstoreCheck.step_27.id, "status" => "passed", "name" => AppstoreCheck.step_27.name, "expected" => step_27_expected, "command" => step_27_command, "got" => step_27_got},
          {"id" => AppstoreCheck.step_28.id, "status" => "running", "name" => AppstoreCheck.step_28.name, "expected" => nil, "command" => step_28_command, "got" => nil},
          {"id" => AppstoreCheck.step_28.id, "status" => "passed", "name" => AppstoreCheck.step_28.name, "expected" => step_28_expected, "command" => step_28_command, "got" => step_28_got},
          {"id" => AppstoreCheck.step_29.id, "status" => "running", "name" => AppstoreCheck.step_29.name, "expected" => nil, "command" => step_29_command, "got" => nil},
          {"id" => AppstoreCheck.step_29.id, "status" => "passed", "name" => AppstoreCheck.step_29.name, "expected" => step_29_expected, "command" => step_29_command, "got" => step_29_got},
          {"id" => AppstoreCheck.step_30.id, "status" => "running", "name" => AppstoreCheck.step_30.name, "expected" => nil, "command" => step_30_command, "got" => nil},
          {"id" => AppstoreCheck.step_30.id, "status" => "passed", "name" => AppstoreCheck.step_30.name, "expected" => step_30_expected, "command" => step_30_command, "got" => step_30_got},
          {"id" => AppstoreCheck.step_31.id, "status" => "running", "name" => AppstoreCheck.step_31.name, "expected" => nil, "command" => step_31_command, "got" => nil},
          {"id" => AppstoreCheck.step_31.id, "status" => "passed", "name" => AppstoreCheck.step_31.name, "expected" => step_31_expected, "command" => step_31_command, "got" => step_31_got},
          {"id" => AppstoreCheck.step_32.id, "status" => "running", "name" => AppstoreCheck.step_32.name, "expected" => nil, "command" => step_32_command, "got" => nil},
          {"id" => AppstoreCheck.step_32.id, "status" => "passed", "name" => AppstoreCheck.step_32.name, "expected" => step_32_expected, "command" => step_32_command, "got" => step_32_got},
          {"id" => AppstoreCheck.step_33.id, "status" => "running", "name" => AppstoreCheck.step_33.name, "expected" => nil, "command" => step_33_command, "got" => nil},
          {"id" => AppstoreCheck.step_33.id, "status" => "passed", "name" => AppstoreCheck.step_33.name, "expected" => step_33_expected, "command" => step_33_command, "got" => step_33_got},
          {"id" => AppstoreCheck.step_34.id, "status" => "running", "name" => AppstoreCheck.step_34.name, "expected" => nil, "command" => step_34_command, "got" => nil},
          {"id" => AppstoreCheck.step_34.id, "status" => "passed", "name" => AppstoreCheck.step_34.name, "expected" => step_34_expected, "command" => step_34_command, "got" => step_34_got},
          {"id" => AppstoreCheck.step_35.id, "status" => "running", "name" => AppstoreCheck.step_35.name, "expected" => nil, "command" => step_35_command, "got" => nil},
          {"id" => AppstoreCheck.step_35.id, "status" => "passed", "name" => AppstoreCheck.step_35.name, "expected" => step_35_expected, "command" => step_35_command, "got" => step_35_got},
          {"id" => AppstoreCheck.step_36.id, "status" => "running", "name" => AppstoreCheck.step_36.name, "expected" => nil, "command" => step_36_command, "got" => nil},
          {"id" => AppstoreCheck.step_36.id, "status" => "passed", "name" => AppstoreCheck.step_36.name, "expected" => step_36_expected, "command" => step_36_command, "got" => step_36_got},
          {"id" => AppstoreCheck.step_37.id, "status" => "running", "name" => AppstoreCheck.step_37.name, "expected" => nil, "command" => step_37_command, "got" => nil},
          {"id" => AppstoreCheck.step_37.id, "status" => "passed", "name" => AppstoreCheck.step_37.name, "expected" => step_37_expected, "command" => step_37_command, "got" => step_37_got},
          {"finished" => true}
      ]
    end

    def parse(text)
      text.split('data: ').select { |el| el.present? }.map { |el| JSON.parse(el) }
    end

  end

end
