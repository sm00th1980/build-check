# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ProjectController, type: :controller do
  include ApplicationHelper

  render_views

  describe "update project" do
    before(:each) do
      @user = User.all.sample
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @project = @user.projects.first

      @params = {
          id: @project.id,
          name: Faker::Name.name,
          app_name: Faker::Name.name,
          zip_name: Faker::Name.name,
          last_version: '1.0.24',
          platform_version: '9.0',
          platform_build: '12B411',
          xcode_version: '0611',
          xcode_build: '12B412',
          bundle_id: 'com.mimedia.iOSv2',
          check_armv7_support: [true, false].sample,
          check_arm64_support: [true, false].sample,
          check_pie_support: [true, false].sample,
          with_extensions: [true, false].sample,
          check_provision: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_configuration_armv7: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_configuration_arm64: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_1x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@1x.png',
              md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_2x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@2x.png',
              md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_3x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@3x.png',
              md5: 'd676993636dd01b57deb8f8d18df3299'
          },
          check_ipad_1x_icon_support: {
              enabled: true,
              name: 'AppIcon76x76~ipad.png',
              md5: 'efadc0a3dcfe7d8243ebaed6c05e3ef4'
          },
          check_ipad_2x_icon_support: {
              enabled: true,
              name: 'AppIcon76x76@2x~ipad.png',
              md5: '73533b632acafccbd51505c506085a76'
          },
          check_iphone_settings_1x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29.png',
              md5: '6270381645d8a22adab8af5a5f8f7c29'
          },
          check_iphone_settings_2x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29@2x.png',
              md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
          },
          check_ipad_settings_1x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29~ipad.png',
              md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
          },
          check_ipad_settings_2x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29@2x~ipad.png',
              md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
          },
          check_iphone_launch_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage.png',
              md5: '89817fb95ab12d69b0f010450ad29428'
          },
          check_iphone_launch_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage@2x.png',
              md5: '278d5a9802193ae28d8698ef5dc7240a'
          },
          check_iphone_launch_568h_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-568h@2x.png',
              md5: '6e41359534fd8a60f4a419c5e2caa429'
          },
          check_iphone_launch_667h_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-667h@2x.png',
              md5: 'b7cf9b3955f9b49ff4ecd39577947861'
          },
          check_iphone_launch_portrait_736h_3x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-Portrait-736h@3x.png',
              md5: '45e1109fedc69abd44a8f3452659feb1'
          },
          check_iphone_launch_landscape_736h_3x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-Landscape-736h@3x.png',
              md5: '3a26e026cf7000a50d98f6ab54310b19'
          },
          check_ipad_launch_portrait_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Portrait~ipad.png',
              md5: 'dcf60180b258defb566de5042ce79bbd'
          },
          check_ipad_launch_landscape_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Landscape~ipad.png',
              md5: '36047b1203297e4c7288092ff3e9aa62'
          },
          check_ipad_launch_portrait_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Portrait@2x~ipad.png',
              md5: '2217217bac18a20935025fe19c27be93'
          },
          check_ipad_launch_landscape_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Landscape@2x~ipad.png',
              md5: 'bf91364ae71b4dab8a0c79e64903b220'
          }
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should not update project for not exist project id" do
      sign_in @user

      post :update, @params.merge(id: -1)

      expect(assigns(:project)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(show_project_path(@user.projects.first))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('project.failure.not_found') % -1
    end

    it "should not update project without name" do
      check_not_update_project(@project, :name, "Name can't be blank")
    end

    it "should not update project without app_name" do
      check_not_update_project(@project, :app_name, "App name can't be blank")
    end

    it "should not update project without zip_name" do
      check_not_update_project(@project, :zip_name, "Zip name can't be blank")
    end

    it "should not update project without last_version" do
      check_not_update_project(@project, :last_version, I18n.t('validation.project.last_version_is_invalid') % '')
    end

    it "should not update project without platform_version" do
      check_not_update_project(@project, :platform_version, "Platform version can't be blank")
    end

    it "should not update project without platform_build" do
      check_not_update_project(@project, :platform_build, "Platform build can't be blank")
    end

    it "should not update project without xcode_version" do
      check_not_update_project(@project, :xcode_version, "XCode version can't be blank")
    end

    it "should not update project without xcode_build" do
      check_not_update_project(@project, :xcode_build, "XCode build can't be blank")
    end

    it "should not update project without bundle_id" do
      check_not_update_project(@project, :bundle_id, "Bundle ID can't be blank")
    end

    it "should not update project without check_armv7_support" do
      check_not_update_project(@project, :check_armv7_support, "Check ARMv7 support can't be blank")
    end

    it "should not update project without check_arm64_support" do
      check_not_update_project(@project, :check_arm64_support, "Check ARM64 support can't be blank")
    end

    it "should not update project without check_pie_support" do
      check_not_update_project(@project, :check_pie_support, "Check PIE support can't be blank")
    end

    it "should not update project without with_extensions" do
      check_not_update_project(@project, :with_extensions, "With extensions can't be blank")
    end

    it "should not update project without check_provision" do
      check_not_update_project(@project, :check_provision, "Check provision can't be blank")
    end

    it "should not update project without check_configuration_armv7" do
      check_not_update_project(@project, :check_configuration_armv7, "Check configuration ARMv7 can't be blank")
    end

    it "should not update project without check_configuration_arm64" do
      check_not_update_project(@project, :check_configuration_arm64, "Check configuration ARM64 can't be blank")
    end

    it "should not update project without check_iphone_1x_icon_support" do
      check_not_update_project(@project, :check_iphone_1x_icon_support, "Check iPhone 1x icon support can't be blank")
    end

    it "should not update project without check_iphone_2x_icon_support" do
      check_not_update_project(@project, :check_iphone_2x_icon_support, "Check iPhone 2x icon support can't be blank")
    end

    it "should not create new project when check_iphone_2x_icon_support name is blank" do
      @params = @params.merge!(check_iphone_2x_icon_support: {
                                   enabled: true,
                                   name: '',
                                   md5: 'vfdvfd'
                               })
      check_not_update_project(@project, nil, I18n.t('project.check_iphone_2x_icon_support.failure'))
    end

    it "should not create new project when check_iphone_2x_icon_support md5 is blank" do
      @params = @params.merge!(check_iphone_2x_icon_support: {
                                   enabled: true,
                                   name: 'vmldfmvlkf',
                                   md5: ''
                               })
      check_not_update_project(@project, nil, I18n.t('project.check_iphone_2x_icon_support.failure'))
    end

    it "should not update project without check_iphone_3x_icon_support" do
      check_not_update_project(@project, :check_iphone_3x_icon_support, "Check iPhone 3x icon support can't be blank")
    end

    it "should not update project without check_ipad_1x_icon_support" do
      check_not_update_project(@project, :check_ipad_1x_icon_support, "Check iPad 1x icon support can't be blank")
    end

    it "should not update project without check_ipad_2x_icon_support" do
      check_not_update_project(@project, :check_ipad_2x_icon_support, "Check iPad 2x icon support can't be blank")
    end

    it "should not update project without check_iphone_settings_1x_icon_support" do
      check_not_update_project(@project, :check_iphone_settings_1x_icon_support, "Check iPhone settings 1x icon support can't be blank")
    end

    it "should not update project without check_iphone_settings_2x_icon_support" do
      check_not_update_project(@project, :check_iphone_settings_2x_icon_support, "Check iPhone settings 2x icon support can't be blank")
    end

    it "should not update project without check_ipad_settings_1x_icon_support" do
      check_not_update_project(@project, :check_ipad_settings_1x_icon_support, "Check iPad settings 1x icon support can't be blank")
    end

    it "should not update project without check_ipad_settings_2x_icon_support" do
      check_not_update_project(@project, :check_ipad_settings_2x_icon_support, "Check iPad settings 2x icon support can't be blank")
    end

    it "should not update project without check_iphone_launch_1x_icon_support" do
      check_not_update_project(@project, :check_iphone_launch_1x_icon_support, "Check iPhone launch 1x image support can't be blank")
    end

    it "should not update project without check_iphone_launch_2x_icon_support" do
      check_not_update_project(@project, :check_iphone_launch_2x_icon_support, "Check iPhone launch 2x image support can't be blank")
    end

    it "should not update project without check_iphone_launch_568h_2x_icon_support" do
      check_not_update_project(@project, :check_iphone_launch_568h_2x_icon_support, "Check iPhone launch 568h 2x image support can't be blank")
    end

    it "should not update project without check_iphone_launch_667h_2x_icon_support" do
      check_not_update_project(@project, :check_iphone_launch_667h_2x_icon_support, "Check iPhone launch 667h 2x image support can't be blank")
    end

    it "should not update project without check_iphone_launch_portrait_736h_3x_icon_support" do
      check_not_update_project(@project, :check_iphone_launch_portrait_736h_3x_icon_support, "Check iPhone launch portrait 736h 3x image support can't be blank")
    end

    it "should not update project without check_iphone_launch_landscape_736h_3x_icon_support" do
      check_not_update_project(@project, :check_iphone_launch_landscape_736h_3x_icon_support, "Check iPhone launch landscape 736h 3x image support can't be blank")
    end

    it "should not update project without check_ipad_launch_portrait_1x_icon_support" do
      check_not_update_project(@project, :check_ipad_launch_portrait_1x_icon_support, "Check iPad launch portrait 1x image support can't be blank")
    end

    it "should not update project without check_ipad_launch_landscape_1x_icon_support" do
      check_not_update_project(@project, :check_ipad_launch_landscape_1x_icon_support, "Check iPad launch landscape 1x image support can't be blank")
    end

    it "should not update project without check_ipad_launch_portrait_2x_icon_support" do
      check_not_update_project(@project, :check_ipad_launch_portrait_2x_icon_support, "Check iPad launch portrait 2x image support can't be blank")
    end

    it "should not update project without check_ipad_launch_landscape_2x_icon_support" do
      check_not_update_project(@project, :check_ipad_launch_landscape_2x_icon_support, "Check iPad launch landscape 2x image support can't be blank")
    end

    private
    def check_not_update_project(project, param, alert)
      old_project = project

      sign_in @user
      post :update, @params.except(param)
      expect(assigns(:project)).to eq project

      expect(response).to have_http_status(:success)
      json = JSON.parse(response.body)
      expect(json['success']).to eq false
      expect(json['message']).to eq [alert]

      expect(project.reload.name).to eq old_project.name
      expect(project.reload.app_name).to eq old_project.app_name
      expect(project.reload.last_version).to eq old_project.last_version
    end
  end

end
