# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ProjectController, type: :controller do
  include ApplicationHelper

  render_views

  describe "update project" do
    before(:each) do
      @user = User.all.sample
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @project = @user.projects.first

      @params = {
          id: @project.id,
          name: Faker::Name.name,
          app_name: Faker::Name.name,
          zip_name: Faker::Name.name,
          last_version: '1.0.24',
          platform_version: '9.0',
          platform_build: '12B411',
          xcode_version: '0611',
          xcode_build: '12B412',
          bundle_id: 'com.mimedia.iOSv2',
          check_armv7_support: [true, false].sample,
          check_arm64_support: [true, false].sample,
          check_pie_support: [true, false].sample,
          with_extensions: [true, false].sample,
          check_provision: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_configuration_armv7: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_configuration_arm64: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_1x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@1x.png',
              md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_2x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@2x.png',
              md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_3x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@3x.png',
              md5: 'd676993636dd01b57deb8f8d18df3299'
          },
          check_ipad_1x_icon_support: {
              enabled: true,
              name: 'AppIcon76x76~ipad.png',
              md5: 'efadc0a3dcfe7d8243ebaed6c05e3ef4'
          },
          check_ipad_2x_icon_support: {
              enabled: true,
              name: 'AppIcon76x76@2x~ipad.png',
              md5: '73533b632acafccbd51505c506085a76'
          },
          check_iphone_settings_1x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29.png',
              md5: '6270381645d8a22adab8af5a5f8f7c29'
          },
          check_iphone_settings_2x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29@2x.png',
              md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
          },
          check_ipad_settings_1x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29~ipad.png',
              md5: '6270381645d8a22adab8af5a5f8f7c29'
          },
          check_ipad_settings_2x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29@2x~ipad.png',
              md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
          },
          check_iphone_launch_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage.png',
              md5: '89817fb95ab12d69b0f010450ad29428'
          },
          check_iphone_launch_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage@2x.png',
              md5: '278d5a9802193ae28d8698ef5dc7240a'
          },
          check_iphone_launch_568h_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-568h@2x.png',
              md5: '6e41359534fd8a60f4a419c5e2caa429'
          },
          check_iphone_launch_667h_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-667h@2x.png',
              md5: 'b7cf9b3955f9b49ff4ecd39577947861'
          },
          check_iphone_launch_portrait_736h_3x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-Portrait-736h@3x.png',
              md5: '45e1109fedc69abd44a8f3452659feb1'
          },
          check_iphone_launch_landscape_736h_3x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-Landscape-736h@3x.png',
              md5: '3a26e026cf7000a50d98f6ab54310b19'
          },
          check_ipad_launch_portrait_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Portrait~ipad.png',
              md5: 'dcf60180b258defb566de5042ce79bbd'
          },
          check_ipad_launch_landscape_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Landscape~ipad.png',
              md5: '36047b1203297e4c7288092ff3e9aa62'
          },
          check_ipad_launch_portrait_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Portrait@2x~ipad.png',
              md5: '2217217bac18a20935025fe19c27be93'
          },
          check_ipad_launch_landscape_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Landscape@2x~ipad.png',
              md5: 'bf91364ae71b4dab8a0c79e64903b220'
          }
      }
    end

    it "should update project" do
      sign_in @user
      post :update, @params
      expect(assigns(:project)).to eq @project

      check_response_with_message(response, true, I18n.t('project.success.updated'))

      expect(@project.reload.name).to eq @params[:name]
      expect(@project.reload.app_name).to eq @params[:app_name]
      expect(@project.reload.zip_name).to eq @params[:zip_name]
      expect(@project.reload.last_version).to eq @params[:last_version]
      expect(@project.reload.platform_version).to eq @params[:platform_version]
      expect(@project.reload.platform_build).to eq @params[:platform_build]
      expect(@project.reload.xcode_version).to eq @params[:xcode_version]
      expect(@project.reload.xcode_build).to eq @params[:xcode_build]
      expect(@project.reload.bundle_id).to eq @params[:bundle_id]
      expect(@project.reload.check_armv7_support).to eq to_bool!(@params[:check_armv7_support])
      expect(@project.reload.check_arm64_support).to eq to_bool!(@params[:check_arm64_support])
      expect(@project.reload.check_pie_support).to eq to_bool!(@params[:check_pie_support])
      expect(@project.reload.with_extensions).to eq to_bool!(@params[:with_extensions])

      expect(@project.reload.check_provision).to eq true
      expect(@project.reload.provision.value).to eq @params[:check_provision][:value]

      expect(@project.reload.check_configuration_armv7).to eq true
      expect(@project.reload.configurations.select { |c| c.type.armv7? }.first.value).to eq @params[:check_configuration_armv7][:value]

      expect(@project.reload.check_configuration_arm64).to eq true
      expect(@project.reload.configurations.select { |c| c.type.arm64? }.first.value).to eq @params[:check_configuration_arm64][:value]

      expect(@project.reload.check_iphone_1x_icon_support).to eq true
      expect(@project.reload.check_iphone_2x_icon_support).to eq true
      expect(@project.reload.check_iphone_3x_icon_support).to eq true

      expect(@project.reload.check_ipad_1x_icon_support).to eq true
      expect(@project.reload.check_ipad_2x_icon_support).to eq true

      expect(@project.reload.check_iphone_settings_1x_icon_support).to eq true
      expect(@project.reload.check_iphone_settings_2x_icon_support).to eq true
      expect(@project.reload.check_ipad_settings_1x_icon_support).to eq true
      expect(@project.reload.check_ipad_settings_2x_icon_support).to eq true

      expect(@project.reload.check_iphone_launch_1x_icon_support).to eq true
      expect(@project.reload.check_iphone_launch_2x_icon_support).to eq true
      expect(@project.reload.check_iphone_launch_568h_2x_icon_support).to eq true
      expect(@project.reload.check_iphone_launch_667h_2x_icon_support).to eq true
      expect(@project.reload.check_iphone_launch_portrait_736h_3x_icon_support).to eq true
      expect(@project.reload.check_iphone_launch_landscape_736h_3x_icon_support).to eq true

      expect(@project.reload.check_ipad_launch_portrait_1x_icon_support).to eq true
      expect(@project.reload.check_ipad_launch_landscape_1x_icon_support).to eq true
      expect(@project.reload.check_ipad_launch_portrait_2x_icon_support).to eq true
      expect(@project.reload.check_ipad_launch_landscape_2x_icon_support).to eq true

      iphone_1x = ProjectIcon.find_by(project: @project, type: IconType.iphone_1x)
      expect(iphone_1x.name).to eq @params[:check_iphone_1x_icon_support][:name]
      expect(iphone_1x.md5).to eq @params[:check_iphone_1x_icon_support][:md5]

      iphone_2x = ProjectIcon.find_by(project: @project, type: IconType.iphone_2x)
      expect(iphone_2x.name).to eq @params[:check_iphone_2x_icon_support][:name]
      expect(iphone_2x.md5).to eq @params[:check_iphone_2x_icon_support][:md5]

      iphone_3x = ProjectIcon.find_by(project: @project, type: IconType.iphone_3x)
      expect(iphone_3x.name).to eq @params[:check_iphone_3x_icon_support][:name]
      expect(iphone_3x.md5).to eq @params[:check_iphone_3x_icon_support][:md5]

      ipad_1x = ProjectIcon.find_by(project: @project, type: IconType.ipad_1x)
      expect(ipad_1x.name).to eq @params[:check_ipad_1x_icon_support][:name]
      expect(ipad_1x.md5).to eq @params[:check_ipad_1x_icon_support][:md5]

      ipad_2x = ProjectIcon.find_by(project: @project, type: IconType.ipad_2x)
      expect(ipad_2x.name).to eq @params[:check_ipad_2x_icon_support][:name]
      expect(ipad_2x.md5).to eq @params[:check_ipad_2x_icon_support][:md5]

      iphone_settings_1x = ProjectIcon.find_by(project: @project, type: IconType.iphone_settings_1x)
      expect(iphone_settings_1x.name).to eq @params[:check_iphone_settings_1x_icon_support][:name]
      expect(iphone_settings_1x.md5).to eq @params[:check_iphone_settings_1x_icon_support][:md5]

      iphone_settings_2x = ProjectIcon.find_by(project: @project, type: IconType.iphone_settings_2x)
      expect(iphone_settings_2x.name).to eq @params[:check_iphone_settings_2x_icon_support][:name]
      expect(iphone_settings_2x.md5).to eq @params[:check_iphone_settings_2x_icon_support][:md5]

      ipad_settings_1x = ProjectIcon.find_by(project: @project, type: IconType.ipad_settings_1x)
      expect(ipad_settings_1x.name).to eq @params[:check_ipad_settings_1x_icon_support][:name]
      expect(ipad_settings_1x.md5).to eq @params[:check_ipad_settings_1x_icon_support][:md5]

      ipad_settings_2x = ProjectIcon.find_by(project: @project, type: IconType.ipad_settings_2x)
      expect(ipad_settings_2x.name).to eq @params[:check_ipad_settings_2x_icon_support][:name]
      expect(ipad_settings_2x.md5).to eq @params[:check_ipad_settings_2x_icon_support][:md5]

      iphone_launch_1x = ProjectIcon.find_by(project: @project, type: IconType.iphone_launch_1x)
      expect(iphone_launch_1x.name).to eq @params[:check_iphone_launch_1x_icon_support][:name]
      expect(iphone_launch_1x.md5).to eq @params[:check_iphone_launch_1x_icon_support][:md5]

      iphone_launch_2x = ProjectIcon.find_by(project: @project, type: IconType.iphone_launch_2x)
      expect(iphone_launch_2x.name).to eq @params[:check_iphone_launch_2x_icon_support][:name]
      expect(iphone_launch_2x.md5).to eq @params[:check_iphone_launch_2x_icon_support][:md5]

      iphone_launch_568h_2x = ProjectIcon.find_by(project: @project, type: IconType.iphone_launch_568h_2x)
      expect(iphone_launch_568h_2x.name).to eq @params[:check_iphone_launch_568h_2x_icon_support][:name]
      expect(iphone_launch_568h_2x.md5).to eq @params[:check_iphone_launch_568h_2x_icon_support][:md5]

      iphone_launch_667h_2x = ProjectIcon.find_by(project: @project, type: IconType.iphone_launch_667h_2x)
      expect(iphone_launch_667h_2x.name).to eq @params[:check_iphone_launch_667h_2x_icon_support][:name]
      expect(iphone_launch_667h_2x.md5).to eq @params[:check_iphone_launch_667h_2x_icon_support][:md5]

      iphone_launch_portrait_736h_3x = ProjectIcon.find_by(project: @project, type: IconType.iphone_launch_portrait_736h_3x)
      expect(iphone_launch_portrait_736h_3x.name).to eq @params[:check_iphone_launch_portrait_736h_3x_icon_support][:name]
      expect(iphone_launch_portrait_736h_3x.md5).to eq @params[:check_iphone_launch_portrait_736h_3x_icon_support][:md5]

      iphone_launch_landscape_736h_3x = ProjectIcon.find_by(project: @project, type: IconType.iphone_launch_landscape_736h_3x)
      expect(iphone_launch_landscape_736h_3x.name).to eq @params[:check_iphone_launch_landscape_736h_3x_icon_support][:name]
      expect(iphone_launch_landscape_736h_3x.md5).to eq @params[:check_iphone_launch_landscape_736h_3x_icon_support][:md5]

      ipad_launch_portrait_1x = ProjectIcon.find_by(project: @project, type: IconType.ipad_launch_portrait_1x)
      expect(ipad_launch_portrait_1x.name).to eq @params[:check_ipad_launch_portrait_1x_icon_support][:name]
      expect(ipad_launch_portrait_1x.md5).to eq @params[:check_ipad_launch_portrait_1x_icon_support][:md5]

      ipad_launch_landscape_1x = ProjectIcon.find_by(project: @project, type: IconType.ipad_launch_landscape_1x)
      expect(ipad_launch_landscape_1x.name).to eq @params[:check_ipad_launch_landscape_1x_icon_support][:name]
      expect(ipad_launch_landscape_1x.md5).to eq @params[:check_ipad_launch_landscape_1x_icon_support][:md5]

      ipad_launch_portrait_2x = ProjectIcon.find_by(project: @project, type: IconType.ipad_launch_portrait_2x)
      expect(ipad_launch_portrait_2x.name).to eq @params[:check_ipad_launch_portrait_2x_icon_support][:name]
      expect(ipad_launch_portrait_2x.md5).to eq @params[:check_ipad_launch_portrait_2x_icon_support][:md5]

      ipad_launch_landscape_2x = ProjectIcon.find_by(project: @project, type: IconType.ipad_launch_landscape_2x)
      expect(ipad_launch_landscape_2x.name).to eq @params[:check_ipad_launch_landscape_2x_icon_support][:name]
      expect(ipad_launch_landscape_2x.md5).to eq @params[:check_ipad_launch_landscape_2x_icon_support][:md5]
    end

  end

  describe "update project" do
    before(:each) do
      @user = User.all.sample
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @project = @user.projects.first

      @params = {
          id: @project.id,
          name: Faker::Name.name,
          app_name: Faker::Name.name,
          zip_name: Faker::Name.name,
          last_version: '1.0.24',
          platform_version: '9.0',
          platform_build: '12B411',
          xcode_version: '0611',
          xcode_build: '12B412',
          bundle_id: 'com.mimedia.iOSv2',
          check_armv7_support: [true, false].sample,
          check_arm64_support: [true, false].sample,
          check_pie_support: [true, false].sample,
          with_extensions: [true, false].sample,
          check_provision: {enabled: false},
          check_configuration_armv7: {enabled: false},
          check_configuration_arm64: {enabled: false},
          check_iphone_1x_icon_support: {enabled: false},
          check_iphone_2x_icon_support: {enabled: false},
          check_iphone_3x_icon_support: {enabled: false},
          check_ipad_1x_icon_support: {enabled: false},
          check_ipad_2x_icon_support: {enabled: false},
          check_iphone_settings_1x_icon_support: {enabled: false},
          check_iphone_settings_2x_icon_support: {enabled: false},
          check_ipad_settings_1x_icon_support: {enabled: false},
          check_ipad_settings_2x_icon_support: {enabled: false},
          check_iphone_launch_1x_icon_support: {enabled: false},
          check_iphone_launch_2x_icon_support: {enabled: false},
          check_iphone_launch_568h_2x_icon_support: {enabled: false},
          check_iphone_launch_667h_2x_icon_support: {enabled: false},
          check_iphone_launch_portrait_736h_3x_icon_support: {enabled: false},
          check_iphone_launch_landscape_736h_3x_icon_support: {enabled: false},
          check_ipad_launch_portrait_1x_icon_support: {enabled: false},
          check_ipad_launch_landscape_1x_icon_support: {enabled: false},
          check_ipad_launch_portrait_2x_icon_support: {enabled: false},
          check_ipad_launch_landscape_2x_icon_support: {enabled: false}
      }
    end

    it "should update project" do
      sign_in @user
      post :update, @params
      expect(assigns(:project)).to eq @project

      check_response_with_message(response, true, I18n.t('project.success.updated'))

      expect(@project.reload.name).to eq @params[:name]
      expect(@project.reload.app_name).to eq @params[:app_name]
      expect(@project.reload.zip_name).to eq @params[:zip_name]
      expect(@project.reload.last_version).to eq @params[:last_version]
      expect(@project.reload.platform_version).to eq @params[:platform_version]
      expect(@project.reload.platform_build).to eq @params[:platform_build]
      expect(@project.reload.xcode_version).to eq @params[:xcode_version]
      expect(@project.reload.xcode_build).to eq @params[:xcode_build]
      expect(@project.reload.bundle_id).to eq @params[:bundle_id]
      expect(@project.reload.check_armv7_support).to eq to_bool!(@params[:check_armv7_support])
      expect(@project.reload.check_arm64_support).to eq to_bool!(@params[:check_arm64_support])
      expect(@project.reload.check_pie_support).to eq to_bool!(@params[:check_pie_support])
      expect(@project.reload.with_extensions).to eq to_bool!(@params[:with_extensions])

      expect(@project.reload.check_provision).to eq false
      expect(ProjectProvision.exists?(project: @project)).to eq false

      expect(@project.reload.check_configuration_armv7).to eq false
      expect(ProjectConfiguration.exists?(project: @project, type: ConfigurationType.armv7)).to eq false

      expect(@project.reload.check_configuration_arm64).to eq false
      expect(ProjectConfiguration.exists?(project: @project, type: ConfigurationType.arm64)).to eq false

      expect(@project.reload.configurations.count).to eq 0

      expect(@project.reload.check_iphone_1x_icon_support).to eq false
      expect(@project.reload.check_iphone_2x_icon_support).to eq false
      expect(@project.reload.check_iphone_3x_icon_support).to eq false

      expect(@project.reload.check_ipad_1x_icon_support).to eq false
      expect(@project.reload.check_ipad_2x_icon_support).to eq false

      expect(@project.reload.check_iphone_settings_1x_icon_support).to eq false
      expect(@project.reload.check_iphone_settings_2x_icon_support).to eq false
      expect(@project.reload.check_ipad_settings_1x_icon_support).to eq false
      expect(@project.reload.check_ipad_settings_2x_icon_support).to eq false

      expect(@project.reload.check_iphone_launch_1x_icon_support).to eq false
      expect(@project.reload.check_iphone_launch_2x_icon_support).to eq false
      expect(@project.reload.check_iphone_launch_568h_2x_icon_support).to eq false
      expect(@project.reload.check_iphone_launch_667h_2x_icon_support).to eq false
      expect(@project.reload.check_iphone_launch_portrait_736h_3x_icon_support).to eq false
      expect(@project.reload.check_iphone_launch_landscape_736h_3x_icon_support).to eq false

      expect(@project.reload.check_ipad_launch_portrait_1x_icon_support).to eq false
      expect(@project.reload.check_ipad_launch_landscape_1x_icon_support).to eq false
      expect(@project.reload.check_ipad_launch_portrait_2x_icon_support).to eq false
      expect(@project.reload.check_ipad_launch_landscape_2x_icon_support).to eq false

      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_1x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_2x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_3x)).to eq false

      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_1x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_2x)).to eq false

      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_settings_1x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_settings_2x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_settings_1x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_settings_2x)).to eq false

      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_launch_1x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_launch_2x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_launch_568h_2x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_launch_667h_2x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_launch_portrait_736h_3x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.iphone_launch_landscape_736h_3x)).to eq false

      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_launch_portrait_1x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_launch_landscape_1x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_launch_portrait_2x)).to eq false
      expect(ProjectIcon.exists?(project: @project, type: IconType.ipad_launch_landscape_2x)).to eq false
    end

  end

  private
  def check_response_with_message(response, result, message)
    expect(response).to have_http_status(:success)
    json = JSON.parse(response.body)
    expect(json['success']).to eq result
    expect(json['message']).to eq message
    expect(json['url']).to eq show_project_path(@project, message: message)
  end

end
