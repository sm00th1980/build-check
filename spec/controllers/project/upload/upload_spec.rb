# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project::UploadController, type: :controller do
  render_views

  #unsigned
  describe "upload file for unsigned" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil

      @filename = 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'
      @project = create(:project, user: @user)

      @params = {
          build: fixture_file_upload("appstore_checks/#{@filename}"),
          id: @project.id
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :upload, @params
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  describe "upload file" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil

      @project = create(:project, user: @user)
      expect(@project).to_not be_nil

      @filename = 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'

      @params = {
          build: fixture_file_upload("appstore_checks/#{@filename}"),
          id: @project.id
      }
    end

    it "should upload file with success" do
      sign_in @user
      assert_difference "StepFile.count" do
        post :upload, @params

        expect(response).to have_http_status(:success)

        file = assigns(:temp_filename)

        expect(file).to_not be_nil
        expect(File.exist?(file)).to be true

        step_file = StepFile.last
        expect(step_file.name).to eq file
        expect(step_file.project.id).to eq @params[:id]
        expect(step_file.checked).to eq false
        expect(step_file.original_name).to eq @filename

        expect(JSON.parse(response.body)).to eq ({"success" => true, "file_id" => step_file.id})

        FileUtils.rm(file)
      end
    end

  end

  describe "upload file with not his project" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil

      @project = create(:project, user: create(:user))
      expect(@project).to_not be_nil

      @filename = 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'

      @params = {
          build: fixture_file_upload("appstore_checks/#{@filename}"),
          id: @project.id
      }
    end

    it "should upload file with failure" do
      sign_in @user
      assert_difference "StepFile.count", 0 do
        post :upload, @params

        expect(response).to have_http_status(:success)
        expect(assigns(:temp_filename)).to be_nil

        check_response_with_message(response, false, I18n.t('upload.failure.project_not_found') % @project.id)
      end
    end

    private
    def check_response_with_message(response, result, message)
      expect(response).to have_http_status(:success)
      json = JSON.parse(response.body)
      expect(json['success']).to eq result
      expect(json['message']).to eq message
    end

  end

end
