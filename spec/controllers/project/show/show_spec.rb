# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Project::ShowController, type: :controller do
  render_views

  #unsigned
  describe "GET index for unsigned" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @params = {
          id: @user.projects.first.id
      }
    end

    it "should redirect to new_session if non-auth user" do
      assert_difference 'count_files_in_tmp_dir', 0 do
        get :index, @params
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe "GET index" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @params = {
          id: @user.projects.first.id
      }
    end

    it "should show project" do
      assert_difference 'count_files_in_tmp_dir', 0 do
        sign_in @user
        get :index, @params
        expect(response).to have_http_status(:success)

        expect(assigns(:project)).to_not be_nil
        expect(@params[:id]).to eq assigns(:project).id
      end
    end
  end

  describe "GET index" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @params = {
          id: @user.projects.first.id
      }
    end

    it "should not show project for other user" do
      assert_difference 'count_files_in_tmp_dir', 0 do
        other_user = create(:user)
        sign_in other_user
        get :index, @params

        expect(assigns(:project)).to be_nil
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(show_project_path(other_user.projects.first))

        expect(flash[:alert]).to eq I18n.t('project.failure.not_found') % @params[:id]
        expect(flash[:notice]).to be_nil
      end
    end
  end

  describe "GET index" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @params = {
          id: @user.projects.first.id
      }
    end

    it "should not show project for other user" do
      assert_difference 'count_files_in_tmp_dir', 0 do
        other_user = create(:user)
        other_user.projects.destroy_all
        expect(other_user.has_projects?).to eq false

        sign_in other_user
        get :index, @params

        expect(assigns(:project)).to be_nil
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_project_path)

        expect(flash[:alert]).to eq I18n.t('project.create_new')
        expect(flash[:notice]).to be_nil
      end
    end
  end

  private
  def count_files_in_tmp_dir
    Dir.entries('/tmp').count
  end

end
