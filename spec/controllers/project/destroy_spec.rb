# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ProjectController, type: :controller do
  render_views

  describe "DELETE project" do
    before(:each) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.projects.count).to be > 1

      @project = @user.projects.first
    end

    it "should destroy project" do
      sign_in @user

      assert_difference 'Project.count', -1 do
        delete :destroy, id: @project.id

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(show_project_path(@user.reload.projects.first))

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('project.success.deleted')

        expect(Project.exists?(id: @project)).to be false
      end
    end

    it "should not destroy project for non exists id" do
      sign_in @user

      assert_difference 'Project.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(show_project_path(@user.reload.projects.first))

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('project.failure.not_found') % -1
      end
    end

    it "should redirect to root page for unsigned user" do
      assert_difference 'Project.count', 0 do
        delete :destroy, id: @project.id
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_user_session_path)

        expect(Project.exists?(id: @project)).to be true
      end
    end

    it "should not destroy project not his own" do
      other_user = create(:user)
      sign_in other_user

      assert_difference 'Project.count', 0 do
        delete :destroy, id: @project.id

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(show_project_path(other_user.reload.projects.first))

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('project.failure.not_found') % @project.id
      end
    end

  end

end
