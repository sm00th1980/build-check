# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ProjectController, type: :controller do
  include ApplicationHelper

  render_views

  describe "create project" do
    before(:each) do
      @user = User.all.sample
      expect(@user).to_not be_nil

      @params = {
          name: Faker::Name.name,
          app_name: Faker::Name.name,
          zip_name: Faker::Name.name,
          last_version: '1.0.24',
          platform_version: '8.1',
          platform_build: '12B411',
          xcode_version: '0611',
          xcode_build: '12B412',
          bundle_id: 'com.mimedia.iOSv2',
          check_armv7_support: [true, false].sample,
          check_arm64_support: [true, false].sample,
          check_pie_support: [true, false].sample,
          with_extensions: [true, false].sample,
          check_provision: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_configuration_armv7: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_configuration_arm64: {
              enabled: true,
              value: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_1x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@1x.png',
              md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_2x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@2x.png',
              md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
          },
          check_iphone_3x_icon_support: {
              enabled: true,
              name: 'AppIcon60x60@3x.png',
              md5: 'd676993636dd01b57deb8f8d18df3299'
          },
          check_ipad_1x_icon_support: {
              enabled: true,
              name: 'AppIcon76x76~ipad.png',
              md5: 'efadc0a3dcfe7d8243ebaed6c05e3ef4'
          },
          check_ipad_2x_icon_support: {
              enabled: true,
              name: 'AppIcon76x76@2x~ipad.png',
              md5: '73533b632acafccbd51505c506085a76'
          },
          check_iphone_settings_1x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29.png',
              md5: '6270381645d8a22adab8af5a5f8f7c29'
          },
          check_iphone_settings_2x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29@2x.png',
              md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
          },
          check_ipad_settings_1x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29~ipad.png',
              md5: '6270381645d8a22adab8af5a5f8f7c29'
          },
          check_ipad_settings_2x_icon_support: {
              enabled: true,
              name: 'AppIcon29x29@2x~ipad.png',
              md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
          },
          check_iphone_launch_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage.png',
              md5: '89817fb95ab12d69b0f010450ad29428'
          },
          check_iphone_launch_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage@2x.png',
              md5: '278d5a9802193ae28d8698ef5dc7240a'
          },
          check_iphone_launch_568h_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-568h@2x.png',
              md5: '6e41359534fd8a60f4a419c5e2caa429'
          },
          check_iphone_launch_667h_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-667h@2x.png',
              md5: 'b7cf9b3955f9b49ff4ecd39577947861'
          },
          check_iphone_launch_portrait_736h_3x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-Portrait-736h@3x.png',
              md5: '45e1109fedc69abd44a8f3452659feb1'
          },
          check_iphone_launch_landscape_736h_3x_icon_support: {
              enabled: true,
              name: 'LaunchImage-800-Landscape-736h@3x.png',
              md5: '3a26e026cf7000a50d98f6ab54310b19'
          },
          check_ipad_launch_portrait_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Portrait~ipad.png',
              md5: 'dcf60180b258defb566de5042ce79bbd'
          },
          check_ipad_launch_landscape_1x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Landscape~ipad.png',
              md5: '36047b1203297e4c7288092ff3e9aa62'
          },
          check_ipad_launch_portrait_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Portrait@2x~ipad.png',
              md5: '2217217bac18a20935025fe19c27be93'
          },
          check_ipad_launch_landscape_2x_icon_support: {
              enabled: true,
              name: 'LaunchImage-Landscape@2x~ipad.png',
              md5: 'bf91364ae71b4dab8a0c79e64903b220'
          }
      }
    end

    it "should create new project with check_iphone_icons_support enabled" do
      sign_in @user

      assert_difference 'Project.count' do
        post :create, @params

        new_project = Project.last

        check_response_with_message(response, true, I18n.t('project.success.created'), show_project_path(new_project, message: I18n.t('project.success.created')))

        expect(new_project.name).to eq @params[:name]
        expect(new_project.app_name).to eq @params[:app_name]
        expect(new_project.zip_name).to eq @params[:zip_name]
        expect(new_project.last_version).to eq @params[:last_version]
        expect(new_project.platform_version).to eq @params[:platform_version]
        expect(new_project.platform_build).to eq @params[:platform_build]
        expect(new_project.xcode_version).to eq @params[:xcode_version]
        expect(new_project.xcode_build).to eq @params[:xcode_build]
        expect(new_project.bundle_id).to eq @params[:bundle_id]
        expect(new_project.check_armv7_support).to eq to_bool!(@params[:check_armv7_support])
        expect(new_project.check_arm64_support).to eq to_bool!(@params[:check_arm64_support])
        expect(new_project.check_pie_support).to eq to_bool!(@params[:check_pie_support])
        expect(new_project.with_extensions).to eq to_bool!(@params[:with_extensions])

        expect(new_project.check_provision).to eq true
        last_provision = ProjectProvision.last
        expect(last_provision.project).to eq new_project
        expect(last_provision.value).to eq @params[:check_provision][:value]

        expect(new_project.check_configuration_armv7).to eq true
        configuration = ProjectConfiguration.find_by!(project: new_project, type: ConfigurationType.armv7)
        expect(configuration.value).to eq @params[:check_configuration_armv7][:value]

        expect(new_project.check_configuration_arm64).to eq true
        configuration = ProjectConfiguration.find_by!(project: new_project, type: ConfigurationType.arm64)
        expect(configuration.value).to eq @params[:check_configuration_arm64][:value]

        expect(new_project.configurations.count).to eq ConfigurationType.count

        expect(new_project.check_iphone_1x_icon_support).to eq true
        expect(new_project.check_iphone_2x_icon_support).to eq true
        expect(new_project.check_iphone_3x_icon_support).to eq true

        expect(new_project.check_ipad_1x_icon_support).to eq true
        expect(new_project.check_ipad_2x_icon_support).to eq true

        expect(new_project.check_iphone_settings_1x_icon_support).to eq true
        expect(new_project.check_iphone_settings_2x_icon_support).to eq true
        expect(new_project.check_ipad_settings_1x_icon_support).to eq true
        expect(new_project.check_ipad_settings_2x_icon_support).to eq true

        expect(new_project.check_iphone_launch_1x_icon_support).to eq true
        expect(new_project.check_iphone_launch_2x_icon_support).to eq true
        expect(new_project.check_iphone_launch_568h_2x_icon_support).to eq true
        expect(new_project.check_iphone_launch_667h_2x_icon_support).to eq true
        expect(new_project.check_iphone_launch_portrait_736h_3x_icon_support).to eq true
        expect(new_project.check_iphone_launch_landscape_736h_3x_icon_support).to eq true

        expect(new_project.check_ipad_launch_portrait_1x_icon_support).to eq true
        expect(new_project.check_ipad_launch_landscape_1x_icon_support).to eq true
        expect(new_project.check_ipad_launch_portrait_2x_icon_support).to eq true
        expect(new_project.check_ipad_launch_landscape_2x_icon_support).to eq true

        iphone_1x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_1x)
        expect(iphone_1x.name).to eq @params[:check_iphone_1x_icon_support][:name]
        expect(iphone_1x.md5).to eq @params[:check_iphone_1x_icon_support][:md5]

        iphone_2x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_2x)
        expect(iphone_2x.name).to eq @params[:check_iphone_2x_icon_support][:name]
        expect(iphone_2x.md5).to eq @params[:check_iphone_2x_icon_support][:md5]

        iphone_3x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_3x)
        expect(iphone_3x.name).to eq @params[:check_iphone_3x_icon_support][:name]
        expect(iphone_3x.md5).to eq @params[:check_iphone_3x_icon_support][:md5]

        ipad_1x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_1x)
        expect(ipad_1x.name).to eq @params[:check_ipad_1x_icon_support][:name]
        expect(ipad_1x.md5).to eq @params[:check_ipad_1x_icon_support][:md5]

        ipad_2x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_2x)
        expect(ipad_2x.name).to eq @params[:check_ipad_2x_icon_support][:name]
        expect(ipad_2x.md5).to eq @params[:check_ipad_2x_icon_support][:md5]

        iphone_settings_1x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_settings_1x)
        expect(iphone_settings_1x.name).to eq @params[:check_iphone_settings_1x_icon_support][:name]
        expect(iphone_settings_1x.md5).to eq @params[:check_iphone_settings_1x_icon_support][:md5]

        iphone_settings_2x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_settings_2x)
        expect(iphone_settings_2x.name).to eq @params[:check_iphone_settings_2x_icon_support][:name]
        expect(iphone_settings_2x.md5).to eq @params[:check_iphone_settings_2x_icon_support][:md5]

        ipad_settings_1x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_settings_1x)
        expect(ipad_settings_1x.name).to eq @params[:check_ipad_settings_1x_icon_support][:name]
        expect(ipad_settings_1x.md5).to eq @params[:check_ipad_settings_1x_icon_support][:md5]

        ipad_settings_2x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_settings_2x)
        expect(ipad_settings_2x.name).to eq @params[:check_ipad_settings_2x_icon_support][:name]
        expect(ipad_settings_2x.md5).to eq @params[:check_ipad_settings_2x_icon_support][:md5]

        iphone_launch_1x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_launch_1x)
        expect(iphone_launch_1x.name).to eq @params[:check_iphone_launch_1x_icon_support][:name]
        expect(iphone_launch_1x.md5).to eq @params[:check_iphone_launch_1x_icon_support][:md5]

        iphone_launch_2x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_launch_2x)
        expect(iphone_launch_2x.name).to eq @params[:check_iphone_launch_2x_icon_support][:name]
        expect(iphone_launch_2x.md5).to eq @params[:check_iphone_launch_2x_icon_support][:md5]

        iphone_launch_568h_2x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_launch_568h_2x)
        expect(iphone_launch_568h_2x.name).to eq @params[:check_iphone_launch_568h_2x_icon_support][:name]
        expect(iphone_launch_568h_2x.md5).to eq @params[:check_iphone_launch_568h_2x_icon_support][:md5]

        iphone_launch_667h_2x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_launch_667h_2x)
        expect(iphone_launch_667h_2x.name).to eq @params[:check_iphone_launch_667h_2x_icon_support][:name]
        expect(iphone_launch_667h_2x.md5).to eq @params[:check_iphone_launch_667h_2x_icon_support][:md5]

        iphone_launch_portrait_736h_3x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_launch_portrait_736h_3x)
        expect(iphone_launch_portrait_736h_3x.name).to eq @params[:check_iphone_launch_portrait_736h_3x_icon_support][:name]
        expect(iphone_launch_portrait_736h_3x.md5).to eq @params[:check_iphone_launch_portrait_736h_3x_icon_support][:md5]

        iphone_launch_landscape_736h_3x = ProjectIcon.find_by(project: new_project, type: IconType.iphone_launch_landscape_736h_3x)
        expect(iphone_launch_landscape_736h_3x.name).to eq @params[:check_iphone_launch_landscape_736h_3x_icon_support][:name]
        expect(iphone_launch_landscape_736h_3x.md5).to eq @params[:check_iphone_launch_landscape_736h_3x_icon_support][:md5]

        ipad_launch_portrait_1x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_launch_portrait_1x)
        expect(ipad_launch_portrait_1x.name).to eq @params[:check_ipad_launch_portrait_1x_icon_support][:name]
        expect(ipad_launch_portrait_1x.md5).to eq @params[:check_ipad_launch_portrait_1x_icon_support][:md5]

        ipad_launch_landscape_1x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_launch_landscape_1x)
        expect(ipad_launch_landscape_1x.name).to eq @params[:check_ipad_launch_landscape_1x_icon_support][:name]
        expect(ipad_launch_landscape_1x.md5).to eq @params[:check_ipad_launch_landscape_1x_icon_support][:md5]

        ipad_launch_portrait_2x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_launch_portrait_2x)
        expect(ipad_launch_portrait_2x.name).to eq @params[:check_ipad_launch_portrait_2x_icon_support][:name]
        expect(ipad_launch_portrait_2x.md5).to eq @params[:check_ipad_launch_portrait_2x_icon_support][:md5]

        ipad_launch_landscape_2x = ProjectIcon.find_by(project: new_project, type: IconType.ipad_launch_landscape_2x)
        expect(ipad_launch_landscape_2x.name).to eq @params[:check_ipad_launch_landscape_2x_icon_support][:name]
        expect(ipad_launch_landscape_2x.md5).to eq @params[:check_ipad_launch_landscape_2x_icon_support][:md5]
      end
    end

  end

  describe "create project" do
    before(:each) do
      @user = User.all.sample
      expect(@user).to_not be_nil

      @params = {
          name: Faker::Name.name,
          app_name: Faker::Name.name,
          zip_name: Faker::Name.name,
          last_version: '1.0.24',
          platform_version: '8.1',
          platform_build: '12B411',
          xcode_version: '0611',
          xcode_build: '12B412',
          bundle_id: 'com.mimedia.iOSv2',
          check_armv7_support: [true, false].sample,
          check_arm64_support: [true, false].sample,
          check_pie_support: [true, false].sample,
          with_extensions: [true, false].sample,
          check_provision: {enabled: false},
          check_configuration_armv7: {enabled: false},
          check_configuration_arm64: {enabled: false},
          check_iphone_1x_icon_support: {enabled: false},
          check_iphone_2x_icon_support: {enabled: false},
          check_iphone_3x_icon_support: {enabled: false},
          check_ipad_1x_icon_support: {enabled: false},
          check_ipad_2x_icon_support: {enabled: false},
          check_iphone_settings_1x_icon_support: {enabled: false},
          check_iphone_settings_2x_icon_support: {enabled: false},
          check_ipad_settings_1x_icon_support: {enabled: false},
          check_ipad_settings_2x_icon_support: {enabled: false},
          check_iphone_launch_1x_icon_support: {enabled: false},
          check_iphone_launch_2x_icon_support: {enabled: false},
          check_iphone_launch_568h_2x_icon_support: {enabled: false},
          check_iphone_launch_667h_2x_icon_support: {enabled: false},
          check_iphone_launch_portrait_736h_3x_icon_support: {enabled: false},
          check_iphone_launch_landscape_736h_3x_icon_support: {enabled: false},
          check_ipad_launch_portrait_1x_icon_support: {enabled: false},
          check_ipad_launch_landscape_1x_icon_support: {enabled: false},
          check_ipad_launch_portrait_2x_icon_support: {enabled: false},
          check_ipad_launch_landscape_2x_icon_support: {enabled: false}
      }
    end

    it "should create new project with check_iphone_icons_support disabled" do
      sign_in @user

      assert_difference 'Project.count' do
        post :create, @params

        new_project = Project.last

        check_response_with_message(response, true, I18n.t('project.success.created'), show_project_path(new_project, message: I18n.t('project.success.created')))

        expect(new_project.name).to eq @params[:name]
        expect(new_project.app_name).to eq @params[:app_name]
        expect(new_project.zip_name).to eq @params[:zip_name]
        expect(new_project.last_version).to eq @params[:last_version]
        expect(new_project.platform_version).to eq @params[:platform_version]
        expect(new_project.platform_build).to eq @params[:platform_build]
        expect(new_project.xcode_version).to eq @params[:xcode_version]
        expect(new_project.xcode_build).to eq @params[:xcode_build]
        expect(new_project.bundle_id).to eq @params[:bundle_id]
        expect(new_project.check_armv7_support).to eq to_bool!(@params[:check_armv7_support])
        expect(new_project.check_arm64_support).to eq to_bool!(@params[:check_arm64_support])
        expect(new_project.check_pie_support).to eq to_bool!(@params[:check_pie_support])
        expect(new_project.with_extensions).to eq to_bool!(@params[:with_extensions])

        expect(new_project.check_provision).to eq false
        expect(ProjectProvision.exists?(project: new_project)).to eq false

        expect(new_project.check_configuration_armv7).to eq false
        expect(ProjectConfiguration.exists?(project: new_project, type: ConfigurationType.armv7)).to eq false

        expect(new_project.check_configuration_arm64).to eq false
        expect(ProjectConfiguration.exists?(project: new_project, type: ConfigurationType.arm64)).to eq false

        expect(new_project.configurations.count).to eq 0

        expect(new_project.check_iphone_1x_icon_support).to eq false
        expect(new_project.check_iphone_2x_icon_support).to eq false
        expect(new_project.check_iphone_3x_icon_support).to eq false

        expect(new_project.check_ipad_1x_icon_support).to eq false
        expect(new_project.check_ipad_2x_icon_support).to eq false

        expect(new_project.check_iphone_settings_1x_icon_support).to eq false
        expect(new_project.check_iphone_settings_2x_icon_support).to eq false
        expect(new_project.check_ipad_settings_1x_icon_support).to eq false
        expect(new_project.check_ipad_settings_2x_icon_support).to eq false

        expect(new_project.check_iphone_launch_1x_icon_support).to eq false
        expect(new_project.check_iphone_launch_2x_icon_support).to eq false
        expect(new_project.check_iphone_launch_568h_2x_icon_support).to eq false
        expect(new_project.check_iphone_launch_667h_2x_icon_support).to eq false
        expect(new_project.check_iphone_launch_portrait_736h_3x_icon_support).to eq false
        expect(new_project.check_iphone_launch_landscape_736h_3x_icon_support).to eq false

        expect(new_project.check_ipad_launch_portrait_1x_icon_support).to eq false
        expect(new_project.check_ipad_launch_landscape_1x_icon_support).to eq false
        expect(new_project.check_ipad_launch_portrait_2x_icon_support).to eq false
        expect(new_project.check_ipad_launch_landscape_2x_icon_support).to eq false

        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_1x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_2x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_3x)).to eq false

        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_1x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_2x)).to eq false

        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_settings_1x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_settings_2x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_settings_1x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_settings_2x)).to eq false

        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_launch_1x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_launch_2x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_launch_568h_2x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_launch_667h_2x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_launch_portrait_736h_3x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.iphone_launch_landscape_736h_3x)).to eq false

        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_launch_portrait_1x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_launch_landscape_1x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_launch_portrait_2x)).to eq false
        expect(ProjectIcon.exists?(project: new_project, type: IconType.ipad_launch_landscape_2x)).to eq false
      end
    end

  end

  private
  def check_response_with_message(response, result, message, path)
    expect(response).to have_http_status(:success)
    json = JSON.parse(response.body)
    expect(json['success']).to eq result
    expect(json['message']).to eq message
    expect(json['url']).to eq path
  end

end
