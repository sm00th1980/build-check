# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ProjectController, type: :controller do
  render_views

  describe "GET edit" do
    before(:each) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true

      @project = @user.projects.first
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @project
      expect(assigns(:project)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show project" do
      sign_in @user
      get :edit, id: @project
      expect(assigns(:project)).to eq @project
      expect(response).to have_http_status(:success)
    end

    it "should show project with check_iphone 2x_icon_support disabled" do
      expect(@project.check_iphone_2x_icon_support).to eq false
      sign_in @user
      get :edit, id: @project
      expect(assigns(:project)).to eq @project
      expect(response).to have_http_status(:success)
    end

    it "should not show project for not exist id" do
      sign_in @user

      get :edit, id: -1

      expect(assigns(:project)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(show_project_path(@user.projects.first))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('project.failure.not_found') % -1
    end

    it "should not show not his own project" do
      other_user = create(:user)

      sign_in other_user

      get :edit, id: @project

      expect(assigns(:project)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(show_project_path(other_user.projects.first))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('project.failure.not_found') % @project.id
    end
  end

end
