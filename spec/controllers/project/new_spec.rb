# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ProjectController, type: :controller do
  render_views

  describe "GET new" do
    before(:each) do
      @user = User.all.sample
      expect(@user).to_not be_nil
    end

    it "should redirect to new_session if non-auth user" do
      get :new
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show new project form for signed user" do
      sign_in @user
      get :new
      expect(response).to have_http_status(:success)
    end
  end

end
