# -*- encoding : utf-8 -*-
require 'rails_helper'

describe RootController, type: :controller do
  render_views

  #unsigned
  describe "GET index for unsigned" do
    it "should redirect to new_session if non-auth user" do
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  #user
  describe "GET index for signed user" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      expect(@user.has_projects?).to eq true
    end

    it "should redirect to first project for signed user" do
      sign_in @user
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(show_project_path(@user.projects.first))

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to be_nil
    end
  end

  #user
  describe "GET index for signed user" do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil
      @user.projects.destroy_all
      expect(@user.has_projects?).to eq false
    end

    it "should redirect to new project path for signed user" do
      sign_in @user
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_project_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('project.create_new')
    end
  end

end
