# -*- encoding : utf-8 -*-
# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../../config/environment', __FILE__)

# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'sidekiq/testing'
require 'webmock/rspec'


# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  config.include Rails.application.routes.url_helpers
  config.include FactoryGirl::Syntax::Methods
  config.include AssertDifference
  config.include Devise::TestHelpers, type: :controller

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  config.before(:all) do
    AppstoreCheck.delete_all
    StepFile.delete_all
    Project.delete_all
    User.delete_all
    Node.delete_all
    IconType.delete_all
    ConfigurationType.delete_all

    create(:user)
    create(:node)

    create(:configuration_type_armv7)
    create(:configuration_type_arm64)

    create(:icon_type_iphone_1x)
    create(:icon_type_iphone_2x)
    create(:icon_type_iphone_3x)
    create(:icon_type_ipad_1x)
    create(:icon_type_ipad_2x)
    create(:icon_type_iphone_settings_1x)
    create(:icon_type_iphone_settings_2x)
    create(:icon_type_ipad_settings_1x)
    create(:icon_type_ipad_settings_2x)
    create(:icon_type_iphone_launch_1x)
    create(:icon_type_iphone_launch_2x)
    create(:icon_type_iphone_launch_568h_2x)
    create(:icon_type_iphone_launch_667h_2x)
    create(:icon_type_iphone_launch_portrait_736h_3x)
    create(:icon_type_iphone_launch_landscape_736h_3x)
    create(:icon_type_ipad_launch_portrait_1x)
    create(:icon_type_ipad_launch_landscape_1x)
    create(:icon_type_ipad_launch_portrait_2x)
    create(:icon_type_ipad_launch_landscape_2x)

    create(:appstore_check_prepare)
    create(:appstore_check_step_1)
    create(:appstore_check_step_3)
    create(:appstore_check_step_4)
    create(:appstore_check_step_5)
    create(:appstore_check_step_6)
    create(:appstore_check_step_7)
    create(:appstore_check_step_8)
    create(:appstore_check_step_9)
    create(:appstore_check_step_10)
    create(:appstore_check_step_11)
    create(:appstore_check_step_12)
    create(:appstore_check_step_13)
    create(:appstore_check_step_14)
    create(:appstore_check_step_15)
    create(:appstore_check_step_16)
    create(:appstore_check_step_19)
    create(:appstore_check_step_20)
    create(:appstore_check_step_21)
    create(:appstore_check_step_22)
    create(:appstore_check_step_23)
    create(:appstore_check_step_24)
    create(:appstore_check_step_25)
    create(:appstore_check_step_26)
    create(:appstore_check_step_27)
    create(:appstore_check_step_28)
    create(:appstore_check_step_29)
    create(:appstore_check_step_30)
    create(:appstore_check_step_31)
    create(:appstore_check_step_32)
    create(:appstore_check_step_33)
    create(:appstore_check_step_34)
    create(:appstore_check_step_35)
    create(:appstore_check_step_36)
    create(:appstore_check_step_37)

  end
end
