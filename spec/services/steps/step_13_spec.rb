# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Steps::Step_13 do

  describe "check Steps::Step_13 with success" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file_with_mimedia, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'))
      expect(@step_file).to_not be_nil

      clean_remote_dir
      Steps::Prepare.new(nil, @step_file).run

      expect(@step_file.reload.local_path).to_not be_nil
      expect(@step_file.reload.project.icons.select { |icon| icon.type.iphone_settings_1x? }.first).to_not be_nil
    end

    after(:all) do
      clean_remote_dir
    end

    it "should check build for step_13 with success" do
      expect do
        step = Steps::Step_13.new(AppstoreCheck.step_13, @step_file)
        step.run

        expect(step.expected == step.got).to eq true
        expect(step.result).to eq true
      end.not_to raise_error
    end

    private
    def clean_remote_dir
      SshEndpoint.execute("rm -rf #{@node.upload_dir}")
    end

  end

end
