# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Steps::Step_20 do

  describe "check Steps::Step_20 with success" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file_with_mimedia, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'))
      expect(@step_file).to_not be_nil

      clean_remote_dir
      Steps::Prepare.new(nil, @step_file).run
    end

    after(:all) do
      clean_remote_dir
    end

    it "should check build for step_20 with success" do
      expect do
        step = Steps::Step_20.new(AppstoreCheck.step_20, @step_file)
        step.run

        expect(step.expected == step.got).to eq true
        expect(step.result).to eq true
      end.not_to raise_error
    end

    private
    def clean_remote_dir
      SshEndpoint.execute("rm -rf #{@node.upload_dir}")
    end

  end

  describe "check Steps::Step_20 with failure" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file_with_mimedia, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'))
      expect(@step_file).to_not be_nil

      clean_remote_dir
      Steps::Prepare.new(nil, @step_file).run

      @output = "%{executable} (architecture armv7):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\n   MH_MAGIC     ARM         V7  0x00     EXECUTE    52       5520   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK\n%{executable} (architecture arm64):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\nMH_MAGIC_64   ARM64        ALL  0x00     EXECUTE    52       6224   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK\n" % {executable: @step_file.project.app_path}
    end

    after(:all) do
      clean_remote_dir
    end

    it "should check build for step_20 with success" do
      expect do
        step = Steps::Step_20.new(AppstoreCheck.step_20, @step_file)
        allow(step).to receive(:output) { @output }
        expect(step.output).to eq @output
        step.run

        expect(step.expected == step.got).to eq false
        expect(step.result).to eq false
        expect(step.got).to eq "armv7 is present but not has PIE binary support\narm64 is present but not has PIE binary support"
      end.not_to raise_error
    end

    private
    def clean_remote_dir
      SshEndpoint.execute("rm -rf #{@node.upload_dir}")
    end

  end


end
