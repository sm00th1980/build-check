# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Steps::Prepare do

  describe "check Steps::Prepare with success" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file_with_mimedia, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'))
      expect(@step_file.local_path).to be_nil
      expect(File.exists?(@step_file.name)).to eq true

      clean_remote_dir
    end

    after(:all) do
      clean_remote_dir
    end

    it "should correct unzip and upload build" do
      expect(SshEndpoint.execute("ls #{@node.upload_dir}")).to eq ("ls: %s: No such file or directory\n" % @node.upload_dir)

      step = Steps::Prepare.new(nil, @step_file)
      step.run
      expect(step.result).to eq true
      expect(step.status).to eq 'passed'

      temp_dir = step.instance_variable_get("@temp_dir")

      expect(temp_dir).to_not be_nil
      expect(@step_file.reload.local_path).to eq temp_dir
      expect(Dir.exist?(temp_dir)).to be true

      content = SshEndpoint.execute("ls #{@node.upload_dir}")
      expect(content).to_not eq ("ls: %s: No such file or directory\n" % @node.upload_dir)
      expect(content.split.empty?).to eq false
    end
  end

  describe "check Steps::Prepare with failure" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.22-2015-08-26_corrupted.zip'))
      expect(@step_file.local_path).to be_nil
      expect(File.exists?(@step_file.name)).to eq true

      clean_remote_dir
    end

    after(:all) do
      clean_remote_dir
    end

    it "should not upload corrupted file" do
      expect(SshEndpoint.execute("ls #{@node.upload_dir}")).to eq ("ls: %s: No such file or directory\n" % @node.upload_dir)

      expect do
        step = Steps::Prepare.new(nil, @step_file)
        step.run
        expect(step.result).to eq false
        expect(step.status).to eq 'failed'

        temp_dir = step.instance_variable_get("@temp_dir")

        expect(temp_dir).to_not be_nil
        expect(@step_file.reload.local_path).to be_nil
        expect(Dir.exist?(temp_dir)).to be false

      end.not_to raise_error

      expect(SshEndpoint.execute("ls #{@node.upload_dir}")).to eq ("ls: %s: No such file or directory\n" % @node.upload_dir)
    end

  end

  describe "check Steps::Prepare with failure" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', '-1'))
      expect(@step_file.local_path).to be_nil

      expect(File.exists?(@step_file.name)).to eq false

      clean_remote_dir
    end

    after(:all) do
      clean_remote_dir
    end

    it "should raise exception if file not exist" do
      expect(SshEndpoint.execute("ls #{@node.upload_dir}")).to eq ("ls: %s: No such file or directory\n" % @node.upload_dir)

      expect do
        Steps::Prepare.new(nil, @step_file).run

        temp_dir = step.instance_variable_get("@temp_dir")

        expect(temp_dir).to_not be_nil
        expect(@step_file.reload.local_path).to be_nil
        expect(Dir.exist?(temp_dir)).to be false

      end.to raise_error("Filename with name <%s> not found" % @step_file.name)

      expect(SshEndpoint.execute("ls #{@node.upload_dir}")).to eq ("ls: %s: No such file or directory\n" % @node.upload_dir)
    end
  end

  private
  def clean_remote_dir
    SshEndpoint.execute("rm -rf #{@node.upload_dir}")
  end

end
