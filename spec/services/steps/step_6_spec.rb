# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Steps::Step_6 do

  describe "check Steps::Step_6 with success" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file_with_mimedia, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'))
      expect(@step_file).to_not be_nil

      clean_remote_dir
      Steps::Prepare.new(nil, @step_file).run
    end

    after(:all) do
      clean_remote_dir
    end

    it "should check build for step_6 with success" do
      content = SshEndpoint.execute("ls #{@node.upload_dir}")
      expect(content).to_not eq ("ls: %s: No such file or directory\n" % @node.upload_dir)
      expect(content.split.empty?).to eq false

      expect do
        step = Steps::Step_6.new(AppstoreCheck.step_6, @step_file)
        step.run

        expect(step.expected == step.got).to eq true
        expect(step.result).to eq true
      end.not_to raise_error
    end

    private
    def clean_remote_dir
      SshEndpoint.execute("rm -rf #{@node.upload_dir}")
    end

  end

end
