# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Steps::Prepare do

  describe "check Steps::Prepare with success" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file_with_oddsfinder, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'OddsFinder', 'OddsFinder-Paid-AppStore-1.1-20151022.zip'))
      expect(@step_file.local_path).to be_nil
      expect(File.exists?(@step_file.name)).to eq true

      clean_remote_dir
    end

    after(:all) do
      clean_remote_dir
    end

    it "should correct unzip and upload build" do
      expect(SshEndpoint.execute("ls #{@node.upload_dir}")).to eq ("ls: %s: No such file or directory\n" % @node.upload_dir)

      step = Steps::Prepare.new(nil, @step_file)
      step.run
      expect(step.result).to eq true
      expect(step.status).to eq 'passed'

      temp_dir = step.instance_variable_get("@temp_dir")

      expect(temp_dir).to_not be_nil
      expect(@step_file.reload.local_path).to eq temp_dir
      expect(Dir.exist?(temp_dir)).to be true

      content = SshEndpoint.execute("ls #{@node.upload_dir}")
      expect(content).to_not eq ("ls: %s: No such file or directory\n" % @node.upload_dir)
      expect(content.split.empty?).to eq false
    end
  end

  private
  def clean_remote_dir
    SshEndpoint.execute("rm -rf #{@node.upload_dir}")
  end

end
