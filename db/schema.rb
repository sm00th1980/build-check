# -*- encoding : utf-8 -*-
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151110111851) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appstore_checks", force: :cascade do |t|
    t.string   "name",                      null: false
    t.text     "command"
    t.text     "expected"
    t.integer  "sort",          default: 1, null: false
    t.string   "internal_name",             null: false
    t.string   "klass",                     null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "icon_type_id"
  end

  create_table "configuration_types", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "configuration_types", ["internal_name"], name: "index_configuration_types_on_internal_name", unique: true, using: :btree

  create_table "icon_types", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.string   "size",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "icon_types", ["internal_name"], name: "index_icon_types_on_internal_name", unique: true, using: :btree

  create_table "nodes", force: :cascade do |t|
    t.string   "name",                       null: false
    t.string   "host",                       null: false
    t.string   "login",                      null: false
    t.string   "password",                   null: false
    t.text     "upload_dir",                 null: false
    t.text     "home_dir",                   null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "disabled",   default: false, null: false
  end

  create_table "project_configurations", force: :cascade do |t|
    t.integer  "project_id", null: false
    t.integer  "type_id",    null: false
    t.text     "value",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "project_configurations", ["project_id", "type_id"], name: "index_project_configurations_on_project_id_and_type_id", unique: true, using: :btree

  create_table "project_icons", force: :cascade do |t|
    t.integer  "project_id", null: false
    t.integer  "type_id",    null: false
    t.string   "name",       null: false
    t.string   "md5",        null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "project_icons", ["project_id", "type_id"], name: "index_project_icons_on_project_id_and_type_id", unique: true, using: :btree

  create_table "project_provisions", force: :cascade do |t|
    t.integer  "project_id", null: false
    t.text     "value",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "project_provisions", ["project_id"], name: "index_project_provisions_on_project_id", unique: true, using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name",                                               null: false
    t.string   "app_name",                                           null: false
    t.integer  "user_id",                                            null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "last_version",                                       null: false
    t.string   "platform_version",                                   null: false
    t.string   "platform_build",                                     null: false
    t.string   "xcode_version",                                      null: false
    t.string   "xcode_build",                                        null: false
    t.string   "bundle_id",                                          null: false
    t.boolean  "check_armv7_support",                                null: false
    t.boolean  "check_arm64_support",                                null: false
    t.boolean  "check_pie_support",                                  null: false
    t.boolean  "check_iphone_2x_icon_support",                       null: false
    t.boolean  "check_iphone_3x_icon_support",                       null: false
    t.boolean  "check_iphone_1x_icon_support",                       null: false
    t.boolean  "check_ipad_2x_icon_support",                         null: false
    t.boolean  "check_ipad_1x_icon_support",                         null: false
    t.boolean  "check_iphone_settings_2x_icon_support",              null: false
    t.boolean  "check_ipad_settings_1x_icon_support",                null: false
    t.boolean  "check_ipad_settings_2x_icon_support",                null: false
    t.boolean  "check_iphone_settings_1x_icon_support",              null: false
    t.boolean  "check_iphone_launch_landscape_736h_3x_icon_support", null: false
    t.boolean  "check_iphone_launch_portrait_736h_3x_icon_support",  null: false
    t.boolean  "check_iphone_launch_667h_2x_icon_support",           null: false
    t.boolean  "check_iphone_launch_568h_2x_icon_support",           null: false
    t.boolean  "check_iphone_launch_2x_icon_support",                null: false
    t.boolean  "check_iphone_launch_1x_icon_support",                null: false
    t.boolean  "check_ipad_launch_landscape_2x_icon_support",        null: false
    t.boolean  "check_ipad_launch_portrait_2x_icon_support",         null: false
    t.boolean  "check_ipad_launch_landscape_1x_icon_support",        null: false
    t.boolean  "check_ipad_launch_portrait_1x_icon_support",         null: false
    t.boolean  "check_provision",                                    null: false
    t.string   "zip_name",                                           null: false
    t.boolean  "check_configuration_arm64",                          null: false
    t.boolean  "check_configuration_armv7",                          null: false
    t.boolean  "with_extensions",                                    null: false
  end

  create_table "step_files", force: :cascade do |t|
    t.string   "name",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "local_path"
    t.integer  "project_id",    null: false
    t.boolean  "checked",       null: false
    t.string   "original_name"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
