# -*- encoding : utf-8 -*-
class AddStep25 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad 2x icon',
        command: '-',
        expected: 'AppIcon76x76@2x~ipad.png (MD5 = 73533b632acafccbd51505c506085a76, SIZE = 152x152)',
        sort: 145,
        internal_name: 'step_25',
        klass: 'Steps::Step_25'
    )
  end
end
