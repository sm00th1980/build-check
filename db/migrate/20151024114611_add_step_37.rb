# -*- encoding : utf-8 -*-
class AddStep37 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Configuration ARM64',
        command: 'otool -L -vh - %{configuration_path}',
        expected: '%{configuration_value}',
        sort: 45,
        internal_name: 'step_37',
        klass: 'Steps::Step_37'
    )
  end
end
