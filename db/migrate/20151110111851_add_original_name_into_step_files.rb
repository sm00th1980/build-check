# -*- encoding : utf-8 -*-
class AddOriginalNameIntoStepFiles < ActiveRecord::Migration
  def change
    add_column :step_files, :original_name, :string
  end
end
