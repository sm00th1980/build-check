# -*- encoding : utf-8 -*-
class AddProjectIdIntoStepFiles < ActiveRecord::Migration
  def change
    add_column :step_files, :project_id, :integer, null: false
  end
end
