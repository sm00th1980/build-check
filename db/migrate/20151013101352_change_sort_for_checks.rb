# -*- encoding : utf-8 -*-
class ChangeSortForChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.step_7.update_attribute(:sort, 8)
    AppstoreCheck.step_8.update_attribute(:sort, 9)
    AppstoreCheck.step_9.update_attribute(:sort, 10)
    AppstoreCheck.step_10.update_attribute(:sort, 11)
    AppstoreCheck.step_11.update_attribute(:sort, 12)
    AppstoreCheck.step_12.update_attribute(:sort, 13)
    AppstoreCheck.step_13.update_attribute(:sort, 14)
    AppstoreCheck.step_14.update_attribute(:sort, 15)
    AppstoreCheck.step_15.update_attribute(:sort, 16)
    AppstoreCheck.step_16.update_attribute(:sort, 17)
    AppstoreCheck.step_18.update_attribute(:sort, 19)
    AppstoreCheck.step_19.update_attribute(:sort, 20)
    AppstoreCheck.step_20.update_attribute(:sort, 21)
    AppstoreCheck.step_21.update_attribute(:sort, 7)
  end
end
