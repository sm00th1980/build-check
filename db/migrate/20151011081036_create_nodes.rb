# -*- encoding : utf-8 -*-
class CreateNodes < ActiveRecord::Migration
  def change
    create_table :nodes do |t|
      t.string "name", null: false
      t.string "host", null: false
      t.string "login", null: false
      t.string "password", null: false
      t.text "upload_dir", null: false
      t.text "home_dir", null: false

      t.timestamps null: false
    end
  end
end
