# -*- encoding : utf-8 -*-
class DropVersionIntoStepFiles < ActiveRecord::Migration
  def up
    remove_column :step_files, :version
  end
end
