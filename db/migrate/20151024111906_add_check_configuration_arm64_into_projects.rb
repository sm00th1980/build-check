# -*- encoding : utf-8 -*-
class AddCheckConfigurationArm64IntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :check_configuration_arm64, :boolean

    Project.all.each do |project|
      project.check_configuration_arm64 = true
      project.save!
    end

    change_column :projects, :check_configuration_arm64, :boolean, null: false
  end

  def down
    remove_column :projects, :check_configuration_arm64
  end
end
