# -*- encoding : utf-8 -*-
class AddStep20 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check PIE binary',
        command: 'otool -vh %{executable}',
        expected: "armv7 is present and has PIE binary support\narm64 is present and has PIE binary support",
        sort: 20,
        internal_name: 'step_20',
        klass: 'Steps::Step_20'
    )
  end
end
