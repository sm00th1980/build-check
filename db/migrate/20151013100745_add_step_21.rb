# -*- encoding : utf-8 -*-
class AddStep21 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check short version',
        command: 'defaults read %{app}/Info.plist CFBundleShortVersionString',
        expected: "%{short_version}\n",
        sort: 21,
        internal_name: 'step_21',
        klass: 'Steps::Step_21'
    )
  end
end
