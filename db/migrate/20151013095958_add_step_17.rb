# -*- encoding : utf-8 -*-
class AddStep17 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check architecture #1',
        command: 'codesign -dvvv %{executable}',
        expected: "Executable=%{executable}\nIdentifier=com.mimedia.iOSv2\nFormat=bundle with Mach-O universal (armv7 arm64)\nCodeDirectory v=20200 size=117841 flags=0x0(none) hashes=5883+5 location=embedded\nHash type=sha1 size=20\nCDHash=bfcc52a63741369bd00bc019bf06b132972b35b2\nSignature size=4333\nAuthority=iPhone Distribution: Mimedia Inc. (958B23VCBE)\nAuthority=Apple Worldwide Developer Relations Certification Authority\nAuthority=Apple Root CA\nSigned Time=15 Sep 2015 16:30:05\nInfo.plist entries=38\nTeamIdentifier=958B23VCBE\nSealed Resources version=2 rules=12 files=980\nInternal requirements count=1 size=184\n",
        sort: 17,
        internal_name: 'step_17',
        klass: 'Steps::Step_17'
    )
  end
end
