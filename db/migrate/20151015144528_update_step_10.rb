# -*- encoding : utf-8 -*-
class UpdateStep10 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_10
    step.expected = '%{xcode_build}'
    step.save!
  end

  def down
    step = AppstoreCheck.step_10
    step.expected = '6A2008a'
    step.save!
  end
end
