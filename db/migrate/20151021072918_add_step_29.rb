# -*- encoding : utf-8 -*-
class AddStep29 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod launch landscape 736h 3x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 169,
        internal_name: 'step_29',
        klass: 'Steps::Step_29'
    )
  end
end
