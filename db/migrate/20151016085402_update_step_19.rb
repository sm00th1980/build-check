# -*- encoding : utf-8 -*-
class UpdateStep19 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_19
    step.name = 'Check architecture ARM64 support'
    step.expected = 'Architecture ARM64 has been supported'
    step.sort = 23
    step.save!
  end
end
