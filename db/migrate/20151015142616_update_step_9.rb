# -*- encoding : utf-8 -*-
class UpdateStep9 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_9
    step.expected = '%{xcode_version}'
    step.save!
  end

  def down
    step = AppstoreCheck.step_8
    step.expected = '0611'
    step.save!
  end
end
