# -*- encoding : utf-8 -*-
class AddPlatformVersionIntoProjects < ActiveRecord::Migration
  def change
    add_column :projects, :platform_version, :string
  end
end
