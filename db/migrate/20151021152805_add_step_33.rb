# -*- encoding : utf-8 -*-
class AddStep33 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod launch 2x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 165,
        internal_name: 'step_33',
        klass: 'Steps::Step_33'
    )
  end
end
