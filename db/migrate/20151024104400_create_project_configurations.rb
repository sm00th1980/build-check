# -*- encoding : utf-8 -*-
class CreateProjectConfigurations < ActiveRecord::Migration
  def change
    create_table :project_configurations do |t|
      t.integer :project_id, null: false
      t.integer :type_id, null: false
      t.text :value, null: false

      t.timestamps null: false
    end

    add_index :project_configurations, [:project_id, :type_id], unique: true
  end
end
