# -*- encoding : utf-8 -*-
class AddCheckIphone1xSupportIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :check_iphone_1x_icon_support, :boolean

    Project.all.each do |project|
      project.check_iphone_1x_icon_support = true
      project.save!
    end

    change_column :projects, :check_iphone_1x_icon_support, :boolean, null: false
  end

  def down
    remove_column :projects, :check_iphone_1x_icon_support
  end
end
