# -*- encoding : utf-8 -*-
class UpdateStep4Command < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_4
    step.command = 'openssl smime -inform der -verify -noverify -in %{provision_path}'
    step.expected = '%{provision_value}'
    step.save!
  end
end
