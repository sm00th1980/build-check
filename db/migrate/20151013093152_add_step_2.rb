# -*- encoding : utf-8 -*-
class AddStep2 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check code sign entitlements',
        command: 'codesign -dvvv --entitlements - %{app}',
        expected: "Executable=/Users/sm00th/.tmp/MiMedia.app/MiMedia\nIdentifier=com.mimedia.iOSv2\nFormat=bundle with Mach-O universal (armv7 arm64)\nCodeDirectory v=20200 size=117841 flags=0x0(none) hashes=5883+5 location=embedded\nHash type=sha1 size=20\nCDHash=bfcc52a63741369bd00bc019bf06b132972b35b2\nSignature size=4333\nAuthority=iPhone Distribution: Mimedia Inc. (958B23VCBE)\nAuthority=Apple Worldwide Developer Relations Certification Authority\nAuthority=Apple Root CA\nSigned Time=15 Sep 2015 16:30:05\nInfo.plist entries=38\nTeamIdentifier=958B23VCBE\nSealed Resources version=2 rules=12 files=980\nInternal requirements count=1 size=184\n??qqP<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n<plist version=\"1.0\">\n<dict>\n        <key>application-identifier</key>\n        <string>958B23VCBE.com.mimedia.iOSv2</string>\n        <key>aps-environment</key>\n        <string>production</string>\n        <key>beta-reports-active</key>\n        <true/>\n        <key>com.apple.developer.team-identifier</key>\n        <string>958B23VCBE</string>\n        <key>get-task-allow</key>\n        <false/>\n        <key>keychain-access-groups</key>\n        <array>\n                <string>958B23VCBE.com.mimedia.iOSv2</string>\n        </array>\n</dict>\n</plist>\n",
        sort: 2,
        internal_name: 'step_2',
        klass: 'Steps::Step_2'
    )
  end
end
