# -*- encoding : utf-8 -*-
class UpdateStep5 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_5
    step.expected = '%{bundle_id}'
    step.save!
  end

  def down
    step = AppstoreCheck.step_5
    step.expected = 'com.mimedia.iOSv2'
    step.save!
  end
end
