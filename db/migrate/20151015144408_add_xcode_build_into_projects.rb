# -*- encoding : utf-8 -*-
class AddXcodeBuildIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :xcode_build, :string

    Project.all.each do |project|
      project.xcode_build = '6A2008a'
      project.save!
    end

    change_column :projects, :xcode_build, :string, null: false
  end

  def down
    remove_column :projects, :xcode_build
  end
end
