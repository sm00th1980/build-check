# -*- encoding : utf-8 -*-
class UpdateStep15 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_15
    step.name = 'Check iPad launch portrait 1x image'
    step.expected = '%{name} (MD5 = %{md5}, SIZE = %{size})'
    step.sort = 170
    step.save!
  end
end
