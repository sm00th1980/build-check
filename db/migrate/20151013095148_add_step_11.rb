# -*- encoding : utf-8 -*-
class AddStep11 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod Icons',
        command: '-',
        expected: "AppIcon60x60@2x.png (MD5 = e73ebed50450ba4b742c23bfbdcfd49c, SIZE = 120x120)\nAppIcon60x60@3x.png (MD5 = d676993636dd01b57deb8f8d18df3299, SIZE = 180x180)",
        sort: 11,
        internal_name: 'step_11',
        klass: 'Steps::Step_11'
    )
  end
end
