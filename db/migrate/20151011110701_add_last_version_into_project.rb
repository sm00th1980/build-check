# -*- encoding : utf-8 -*-
class AddLastVersionIntoProject < ActiveRecord::Migration
  def change
    add_column :projects, :last_version, :string, null: false
  end
end
