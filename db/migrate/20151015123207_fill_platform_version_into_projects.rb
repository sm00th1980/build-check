# -*- encoding : utf-8 -*-
class FillPlatformVersionIntoProjects < ActiveRecord::Migration
  def up
    Project.all.each do |project|
      project.platform_version = '8.1'
      project.save!
    end

    change_column :projects, :platform_version, :string, null: false
  end
end
