# -*- encoding : utf-8 -*-
class FillIconTypes < ActiveRecord::Migration
  def up
    IconType.create!(name: 'iphone_2x', internal_name: 'iphone_2x', size: '120x120')
    IconType.create!(name: 'iphone_3x', internal_name: 'iphone_3x', size: '180x180')

    IconType.create!(name: 'ipad_1x', internal_name: 'ipad_1x', size: '76x76')
    IconType.create!(name: 'ipad_2x', internal_name: 'ipad_2x', size: '152x152')

    IconType.create!(name: 'iphone_settings_1x', internal_name: 'iphone_settings_1x', size: '29x29')
    IconType.create!(name: 'iphone_settings_2x', internal_name: 'iphone_settings_2x', size: '58x58')
    IconType.create!(name: 'ipad_settings_1x', internal_name: 'ipad_settings_1x', size: '29x29')
    IconType.create!(name: 'ipad_settings_2x', internal_name: 'ipad_settings_2x', size: '58x58')

    IconType.create!(name: 'iphone_launch_1x', internal_name: 'iphone_launch_1x', size: '320x480')
    IconType.create!(name: 'iphone_launch_2x', internal_name: 'iphone_launch_2x', size: '640x960')
    IconType.create!(name: 'iphone_launch_568h_2x', internal_name: 'iphone_launch_568h_2x', size: '640x1136')
    IconType.create!(name: 'iphone_launch_667h_2x', internal_name: 'iphone_launch_667h_2x', size: '750x1334')
    IconType.create!(name: 'iphone_launch_portrait_736h_3x', internal_name: 'iphone_launch_portrait_736h_3x', size: '1242x2208')
    IconType.create!(name: 'iphone_launch_landscape_736h_3x', internal_name: 'iphone_launch_landscape_736h_3x', size: '2208x1242')

    IconType.create!(name: 'ipad_launch_portrait_1x', internal_name: 'ipad_launch_portrait_1x', size: '768x1024')
    IconType.create!(name: 'ipad_launch_landscapet_1x', internal_name: 'ipad_launch_landscape_1x', size: '1024x768')
    IconType.create!(name: 'ipad_launch_portrait_2x', internal_name: 'ipad_launch_portrait_2x', size: '1536x2048')
    IconType.create!(name: 'ipad_launch_landscapet_2x', internal_name: 'ipad_launch_landscape_2x', size: '2048x1536')
  end

  def down
    IconType.delete_all
  end
end
