# -*- encoding : utf-8 -*-
class AddStep8 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check platform build',
        command: 'defaults read %{app}/Info.plist DTPlatformBuild',
        expected: "12B411\n",
        sort: 8,
        internal_name: 'step_8',
        klass: 'Steps::Step_8'
    )
  end
end
