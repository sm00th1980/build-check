# -*- encoding : utf-8 -*-
class AddStep18 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check architecture',
        command: 'otool -vh %{executable}',
        expected: "%{executable} (architecture armv7):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\n   MH_MAGIC     ARM         V7  0x00     EXECUTE    52       5520   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n%{executable} (architecture arm64):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\nMH_MAGIC_64   ARM64        ALL  0x00     EXECUTE    52       6224   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n",
        sort: 18,
        internal_name: 'step_18',
        klass: 'Steps::Step_18'
    )
  end
end
