# -*- encoding : utf-8 -*-
class AddStep22 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check architecture ARMv7 support',
        command: 'otool -vh %{executable}',
        expected: "Architecture ARMv7 has been supported",
        sort: 22,
        internal_name: 'step_22',
        klass: 'Steps::Step_22'
    )
  end
end
