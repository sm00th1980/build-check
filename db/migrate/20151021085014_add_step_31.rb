# -*- encoding : utf-8 -*-
class AddStep31 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod launch 667h 2x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 167,
        internal_name: 'step_31',
        klass: 'Steps::Step_31'
    )
  end
end
