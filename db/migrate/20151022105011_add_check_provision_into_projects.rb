# -*- encoding : utf-8 -*-
class AddCheckProvisionIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :check_provision, :boolean

    Project.all.each do |project|
      project.check_provision = true
      project.save!
    end

    change_column :projects, :check_provision, :boolean, null: false
  end

  def down
    remove_column :projects, :check_provision
  end
end
