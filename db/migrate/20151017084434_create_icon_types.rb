# -*- encoding : utf-8 -*-
class CreateIconTypes < ActiveRecord::Migration
  def change
    create_table :icon_types do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      t.string :size, null: false

      t.timestamps null: false
    end

    add_index :icon_types, :internal_name, unique: true
  end
end
