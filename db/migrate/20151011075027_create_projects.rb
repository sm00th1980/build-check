# -*- encoding : utf-8 -*-
class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name, null: false
      t.string :app_name, null: false
      t.integer :user_id, null: false

      t.timestamps null: false
    end
  end
end
