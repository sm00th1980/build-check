# -*- encoding : utf-8 -*-
class UpdateStep8 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_8
    step.expected = '%{platform_build}'
    step.save!
  end

  def down
    step = AppstoreCheck.step_8
    step.expected = '12B411'
    step.save!
  end
end
