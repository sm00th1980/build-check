# -*- encoding : utf-8 -*-
class CreateStepFiles < ActiveRecord::Migration
  def change
    create_table :step_files do |t|
      t.string "name", null: false

      t.timestamps null: false
    end
  end
end
