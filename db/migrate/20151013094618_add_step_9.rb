# -*- encoding : utf-8 -*-
class AddStep9 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check DTXcode',
        command: 'defaults read %{app}/Info.plist DTXcode',
        expected: "0611\n",
        sort: 9,
        internal_name: 'step_9',
        klass: 'Steps::Step_9'
    )
  end
end
