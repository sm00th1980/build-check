# -*- encoding : utf-8 -*-
class AddStep27 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad settings 1x icon',
        command: '-',
        expected: 'AppIcon29x29~ipad.png (MD5 = 6270381645d8a22adab8af5a5f8f7c29, SIZE = 29x29)',
        sort: 157,
        internal_name: 'step_27',
        klass: 'Steps::Step_27'
    )
  end
end
