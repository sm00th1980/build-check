# -*- encoding : utf-8 -*-
class AddXcodeVersionIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :xcode_version, :string

    Project.all.each do |project|
      project.xcode_version = '0611'
      project.save!
    end

    change_column :projects, :xcode_version, :string, null: false
  end

  def down
    remove_column :projects, :xcode_version
  end
end
