# -*- encoding : utf-8 -*-
class AddBundleIdIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :bundle_id, :string

    Project.all.each do |project|
      project.bundle_id = 'com.mimedia.iOSv2'
      project.save!
    end

    change_column :projects, :bundle_id, :string, null: false
  end

  def down
    remove_column :projects, :bundle_id
  end
end
