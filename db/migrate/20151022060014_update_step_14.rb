# -*- encoding : utf-8 -*-
class UpdateStep14 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_14
    step.name = 'Check iPhone/iPod launch 1x image'
    step.expected = '%{name} (MD5 = %{md5}, SIZE = %{size})'
    step.sort = 160
    step.save!
  end
end
