# -*- encoding : utf-8 -*-
class AddIconTypeIdIntoAppstoreChecks < ActiveRecord::Migration
  def change
    add_column :appstore_checks, :icon_type_id, :integer
  end
end
