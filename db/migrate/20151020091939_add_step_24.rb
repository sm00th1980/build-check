# -*- encoding : utf-8 -*-
class AddStep24 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod 1x Icon',
        command: '-',
        expected: 'AppIcon60x60@1x.png (MD5 = d676993636dd01b57deb8f8d18df3299, SIZE = 60x60)',
        sort: 115,
        internal_name: 'step_24',
        klass: 'Steps::Step_24'
    )

    AppstoreCheck.step_11.update_attribute(:sort, 120)
  end
end
