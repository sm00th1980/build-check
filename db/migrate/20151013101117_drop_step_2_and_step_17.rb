# -*- encoding : utf-8 -*-
class DropStep2AndStep17 < ActiveRecord::Migration
  def up
    AppstoreCheck.where(internal_name: ['step_2', 'step_17']).destroy_all
  end
end
