# -*- encoding : utf-8 -*-
class AddZipNameIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :zip_name, :string

    Project.all.each do |project|
      project.zip_name = project.app_name
      project.save!
    end

    change_column :projects, :zip_name, :string, null: false
  end

  def down
    remove_column :projects, :zip_name
  end
end
