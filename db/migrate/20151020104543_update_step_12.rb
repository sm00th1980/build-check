# -*- encoding : utf-8 -*-
class UpdateStep12 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_12
    step.name = 'Check iPad 1x icon'
    step.expected = 'AppIcon76x76~ipad.png (MD5 = efadc0a3dcfe7d8243ebaed6c05e3ef4, SIZE = 76x76)'
    step.save!
  end
end
