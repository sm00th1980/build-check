# -*- encoding : utf-8 -*-
class CreateProjectProvisions < ActiveRecord::Migration
  def change
    create_table :project_provisions do |t|
      t.integer :project_id, null: false
      t.text :value, null: false

      t.timestamps null: false
    end

    add_index :project_provisions, :project_id, unique: true
  end
end
