# -*- encoding : utf-8 -*-
class UpdateSortingForSteps < ActiveRecord::Migration
  def up
    AppstoreCheck.step_23.update_attribute(:sort, 130)
    AppstoreCheck.step_12.update_attribute(:sort, 140)
    AppstoreCheck.step_13.update_attribute(:sort, 150)
    AppstoreCheck.step_14.update_attribute(:sort, 160)
    AppstoreCheck.step_15.update_attribute(:sort, 170)
    AppstoreCheck.step_16.update_attribute(:sort, 180)
  end
end
