# -*- encoding : utf-8 -*-
class CreateConfigurationTypes < ActiveRecord::Migration
  def change
    create_table :configuration_types do |t|
      t.string :name, null: false
      t.string :internal_name, null: false

      t.timestamps null: false
    end

    add_index :configuration_types, :internal_name, unique: true
  end
end
