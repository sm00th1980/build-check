# -*- encoding : utf-8 -*-
class AddCheckArmv7SupportIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :check_armv7_support, :boolean

    Project.all.each do |project|
      project.check_armv7_support = true
      project.save!
    end

    change_column :projects, :check_armv7_support, :boolean, null: false
  end

  def down
    remove_column :projects, :check_armv7_support
  end
end
