# -*- encoding : utf-8 -*-
class AddStep35 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad launch portrait 2x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 178,
        internal_name: 'step_35',
        klass: 'Steps::Step_35'
    )
  end
end
