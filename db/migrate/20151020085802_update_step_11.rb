# -*- encoding : utf-8 -*-
class UpdateStep11 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_11
    step.name = 'Check iPhone/iPod 2x Icon'
    step.expected = 'AppIcon60x60@2x.png (MD5 = e73ebed50450ba4b742c23bfbdcfd49c, SIZE = 120x120)'
    step.save!
  end
end
