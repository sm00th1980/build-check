# -*- encoding : utf-8 -*-
class AddStepPrepare < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Unzipping',
        command: nil,
        expected: nil,
        sort: 0,
        internal_name: 'prepare',
        klass: 'Steps::Prepare'
    )
  end
end
