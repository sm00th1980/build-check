# -*- encoding : utf-8 -*-
class RenameCheckIphoneIconsSupportToCheckIphone2xIconSupport < ActiveRecord::Migration
  def change
    rename_column :projects, :check_iphone_icons_support, :check_iphone_2x_icon_support
  end
end
