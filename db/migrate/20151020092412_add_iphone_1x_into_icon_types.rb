# -*- encoding : utf-8 -*-
class AddIphone1xIntoIconTypes < ActiveRecord::Migration
  def up
    IconType.create!(name: 'iphone_1x', internal_name: 'iphone_1x', size: '60x60')
  end
end
