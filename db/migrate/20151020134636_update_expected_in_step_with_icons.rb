# -*- encoding : utf-8 -*-
class UpdateExpectedInStepWithIcons < ActiveRecord::Migration
  def up
    [
        AppstoreCheck.step_11,
        AppstoreCheck.step_12,
        AppstoreCheck.step_23,
        AppstoreCheck.step_24,
        AppstoreCheck.step_25,
        AppstoreCheck.step_26,
        AppstoreCheck.step_27
    ].each do |step|
      step.expected = '%{name} (MD5 = %{md5}, SIZE = %{size})'
      step.save!
    end
  end
end
