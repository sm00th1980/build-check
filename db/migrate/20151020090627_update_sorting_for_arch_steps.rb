# -*- encoding : utf-8 -*-
class UpdateSortingForArchSteps < ActiveRecord::Migration
  def up
    AppstoreCheck.step_22.update_attribute(:sort, 220)
    AppstoreCheck.step_19.update_attribute(:sort, 230)
    AppstoreCheck.step_20.update_attribute(:sort, 240)
  end
end
