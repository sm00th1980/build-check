# -*- encoding : utf-8 -*-
class AddStep1 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check code sign',
        internal_name: 'step_1',
        sort: 1,
        klass: 'Steps::Step_1',
        command: 'codesign --verify --no-strict -vvvv %{app}',
        expected: '%{app}: valid on disk
%{app}: satisfies its Designated Requirement
'
    )
  end
end
