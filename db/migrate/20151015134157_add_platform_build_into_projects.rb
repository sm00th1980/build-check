# -*- encoding : utf-8 -*-
class AddPlatformBuildIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :platform_build, :string

    Project.all.each do |project|
      project.platform_build = '12B411'
      project.save!
    end

    change_column :projects, :platform_build, :string, null: false
  end

  def down
    remove_column :projects, :platform_build
  end
end
