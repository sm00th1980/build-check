# -*- encoding : utf-8 -*-
class FillIconTypeIntoAppstoreChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.step_24.update_attribute(:icon_type_id, IconType.iphone_1x.id)
    AppstoreCheck.step_11.update_attribute(:icon_type_id, IconType.iphone_2x.id)
    AppstoreCheck.step_23.update_attribute(:icon_type_id, IconType.iphone_3x.id)

    AppstoreCheck.step_12.update_attribute(:icon_type_id, IconType.ipad_1x.id)
    AppstoreCheck.step_25.update_attribute(:icon_type_id, IconType.ipad_2x.id)

    AppstoreCheck.step_13.update_attribute(:icon_type_id, IconType.iphone_settings_1x.id)
    AppstoreCheck.step_26.update_attribute(:icon_type_id, IconType.iphone_settings_2x.id)

    AppstoreCheck.step_27.update_attribute(:icon_type_id, IconType.ipad_settings_1x.id)
    AppstoreCheck.step_28.update_attribute(:icon_type_id, IconType.ipad_settings_2x.id)

    AppstoreCheck.step_14.update_attribute(:icon_type_id, IconType.iphone_launch_1x.id)
    AppstoreCheck.step_33.update_attribute(:icon_type_id, IconType.iphone_launch_2x.id)
    AppstoreCheck.step_32.update_attribute(:icon_type_id, IconType.iphone_launch_568h_2x.id)
    AppstoreCheck.step_31.update_attribute(:icon_type_id, IconType.iphone_launch_667h_2x.id)
    AppstoreCheck.step_30.update_attribute(:icon_type_id, IconType.iphone_launch_portrait_736h_3x.id)
    AppstoreCheck.step_29.update_attribute(:icon_type_id, IconType.iphone_launch_landscape_736h_3x.id)

    AppstoreCheck.step_15.update_attribute(:icon_type_id, IconType.ipad_launch_portrait_1x.id)
    AppstoreCheck.step_36.update_attribute(:icon_type_id, IconType.ipad_launch_landscape_1x.id)
    AppstoreCheck.step_35.update_attribute(:icon_type_id, IconType.ipad_launch_portrait_2x.id)
    AppstoreCheck.step_34.update_attribute(:icon_type_id, IconType.ipad_launch_landscape_2x.id)
  end

  def down
    AppstoreCheck.all.each do |step|
      step.icon_type_id = nil
      step.save!
    end
  end
end
