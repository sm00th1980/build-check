# -*- encoding : utf-8 -*-
class UpdateStep3 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_3

    step.name = 'Configuration ARMv7'
    step.command = 'otool -L -vh - %{configuration_path}'
    step.expected = '%{configuration_value}'
    step.sort = 44
    step.save!
  end
end
