# -*- encoding : utf-8 -*-
class AddStep28 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad settings 2x icon',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 158,
        internal_name: 'step_28',
        klass: 'Steps::Step_28'
    )
  end
end
