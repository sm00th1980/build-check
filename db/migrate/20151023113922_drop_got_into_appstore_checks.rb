# -*- encoding : utf-8 -*-
class DropGotIntoAppstoreChecks < ActiveRecord::Migration
  def up
    remove_column :appstore_checks, :got
  end
end
