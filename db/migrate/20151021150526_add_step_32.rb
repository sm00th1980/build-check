# -*- encoding : utf-8 -*-
class AddStep32 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod launch 568h 2x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 166,
        internal_name: 'step_32',
        klass: 'Steps::Step_32'
    )
  end
end
