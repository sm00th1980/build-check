# -*- encoding : utf-8 -*-
class AddCheckIphoneIconsSupportIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :check_iphone_icons_support, :boolean

    Project.all.each do |project|
      project.check_iphone_icons_support = true
      project.save!
    end

    change_column :projects, :check_iphone_icons_support, :boolean, null: false
  end

  def down
    remove_column :projects, :check_iphone_icons_support
  end
end
