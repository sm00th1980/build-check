# -*- encoding : utf-8 -*-
class AddStep26 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone settings 2x icon',
        command: '-',
        expected: 'AppIcon29x29@2x.png (MD5 = bb39a9fbdee6ed15eccf4183ea64cea9, SIZE = 58x58)',
        sort: 155,
        internal_name: 'step_26',
        klass: 'Steps::Step_26'
    )
  end
end
