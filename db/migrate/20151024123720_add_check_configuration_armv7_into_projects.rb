# -*- encoding : utf-8 -*-
class AddCheckConfigurationArmv7IntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :check_configuration_armv7, :boolean

    Project.all.each do |project|
      project.check_configuration_armv7 = true
      project.save!
    end

    change_column :projects, :check_configuration_armv7, :boolean, null: false
  end

  def down
    remove_column :projects, :check_configuration_armv7
  end
end
