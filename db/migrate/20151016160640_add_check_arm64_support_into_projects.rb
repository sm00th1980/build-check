# -*- encoding : utf-8 -*-
class AddCheckArm64SupportIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :check_arm64_support, :boolean

    Project.all.each do |project|
      project.check_arm64_support = true
      project.save!
    end

    change_column :projects, :check_arm64_support, :boolean, null: false
  end

  def down
    remove_column :projects, :check_arm64_support
  end
end
