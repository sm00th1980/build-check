# -*- encoding : utf-8 -*-
class AddStep3 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check configuration',
        command: 'otool -L -vh - %{path}',
        expected: '%{path} (architecture armv7):
Mach header
      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags
   MH_MAGIC     ARM         V7  0x00     EXECUTE    52       5520   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE
	/System/Library/Frameworks/Photos.framework/Photos (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/QuickLook.framework/QuickLook (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreBluetooth.framework/CoreBluetooth (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreMotion.framework/CoreMotion (compatibility version 1.0.0, current version 1753.7.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AddressBook.framework/AddressBook (compatibility version 1.0.0, current version 30.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/MobileCoreServices.framework/MobileCoreServices (compatibility version 1.0.0, current version 66.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/MediaPlayer.framework/MediaPlayer (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/OpenGLES.framework/OpenGLES (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libicucore.A.dylib (compatibility version 1.0.0, current version 53.1.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libc++.1.dylib (compatibility version 1.0.0, current version 235.1.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/ImageIO.framework/ImageIO (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/GLKit.framework/GLKit (compatibility version 1.0.0, current version 21.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreText.framework/CoreText (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreData.framework/CoreData (compatibility version 1.0.0, current version 519.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AVFoundation.framework/AVFoundation (compatibility version 1.0.0, current version 2.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreLocation.framework/CoreLocation (compatibility version 1.0.0, current version 1753.7.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/MapKit.framework/MapKit (compatibility version 1.0.0, current version 14.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AssetsLibrary.framework/AssetsLibrary (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libz.1.dylib (compatibility version 1.0.0, current version 1.2.5)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libsqlite3.dylib (compatibility version 9.0.0, current version 162.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AdSupport.framework/AdSupport (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/QuartzCore.framework/QuartzCore (compatibility version 1.2.0, current version 1.10.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony (compatibility version 1.0.0, current version 2260.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration (compatibility version 1.0.0, current version 700.1.4)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/Security.framework/Security (compatibility version 1.0.0, current version 0.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreGraphics.framework/CoreGraphics (compatibility version 64.0.0, current version 600.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/UIKit.framework/UIKit (compatibility version 1.0.0, current version 3318.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/Foundation.framework/Foundation (compatibility version 300.0.0, current version 1140.11.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libobjc.A.dylib (compatibility version 1.0.0, current version 228.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1213.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/Accelerate.framework/Accelerate (compatibility version 1.0.0, current version 4.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CFNetwork.framework/CFNetwork (compatibility version 1.0.0, current version 711.0.6)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation (compatibility version 150.0.0, current version 1140.1.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreImage.framework/CoreImage (compatibility version 1.0.0, current version 4.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreMedia.framework/CoreMedia (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
%{path} (architecture arm64):
Mach header
      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags
MH_MAGIC_64   ARM64        ALL  0x00     EXECUTE    52       6224   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE
	/System/Library/Frameworks/Photos.framework/Photos (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/QuickLook.framework/QuickLook (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreBluetooth.framework/CoreBluetooth (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreMotion.framework/CoreMotion (compatibility version 1.0.0, current version 1753.7.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AddressBook.framework/AddressBook (compatibility version 1.0.0, current version 30.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/MobileCoreServices.framework/MobileCoreServices (compatibility version 1.0.0, current version 66.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/MediaPlayer.framework/MediaPlayer (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/OpenGLES.framework/OpenGLES (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libicucore.A.dylib (compatibility version 1.0.0, current version 53.1.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libc++.1.dylib (compatibility version 1.0.0, current version 235.1.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/ImageIO.framework/ImageIO (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/GLKit.framework/GLKit (compatibility version 1.0.0, current version 21.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreText.framework/CoreText (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreData.framework/CoreData (compatibility version 1.0.0, current version 519.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AVFoundation.framework/AVFoundation (compatibility version 1.0.0, current version 2.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreLocation.framework/CoreLocation (compatibility version 1.0.0, current version 1753.7.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/MapKit.framework/MapKit (compatibility version 1.0.0, current version 14.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AssetsLibrary.framework/AssetsLibrary (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libz.1.dylib (compatibility version 1.0.0, current version 1.2.5)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libsqlite3.dylib (compatibility version 9.0.0, current version 162.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/AdSupport.framework/AdSupport (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/QuartzCore.framework/QuartzCore (compatibility version 1.2.0, current version 1.10.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony (compatibility version 1.0.0, current version 2260.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration (compatibility version 1.0.0, current version 700.1.4)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/Security.framework/Security (compatibility version 1.0.0, current version 0.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreGraphics.framework/CoreGraphics (compatibility version 64.0.0, current version 600.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/UIKit.framework/UIKit (compatibility version 1.0.0, current version 3318.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/Foundation.framework/Foundation (compatibility version 300.0.0, current version 1140.11.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libobjc.A.dylib (compatibility version 1.0.0, current version 228.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1213.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/Accelerate.framework/Accelerate (compatibility version 1.0.0, current version 4.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CFNetwork.framework/CFNetwork (compatibility version 1.0.0, current version 711.0.6)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation (compatibility version 150.0.0, current version 1140.1.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreImage.framework/CoreImage (compatibility version 1.0.0, current version 4.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
	/System/Library/Frameworks/CoreMedia.framework/CoreMedia (compatibility version 1.0.0, current version 1.0.0)
	time stamp 2 Thu Jan  1 04:00:02 1970
',
        sort: 3,
        internal_name: 'step_3',
        klass: 'Steps::Step_3'
    )
  end
end
