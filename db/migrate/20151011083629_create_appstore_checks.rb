# -*- encoding : utf-8 -*-
class CreateAppstoreChecks < ActiveRecord::Migration
  def change
    create_table :appstore_checks do |t|
      t.string "name", null: false
      t.text "command"
      t.text "expected"
      t.text "got"
      t.integer "sort", default: 1, null: false
      t.string :internal_name, null: false
      t.string :klass, null: false

      t.timestamps null: false
    end
  end
end
