# -*- encoding : utf-8 -*-
class AddStep34 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad launch landscape 2x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 179,
        internal_name: 'step_34',
        klass: 'Steps::Step_34'
    )
  end
end
