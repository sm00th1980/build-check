# -*- encoding : utf-8 -*-
class AddStep19 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check ARM64 support',
        command: 'otool -vh %{executable}',
        expected: "arm64 has been supported",
        sort: 19,
        internal_name: 'step_19',
        klass: 'Steps::Step_19'
    )
  end
end
