# -*- encoding : utf-8 -*-
class RemoveCheckPrependIntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.where.not(id: AppstoreCheck.prepare).each do |check|
      new_name = check.name.split().reject { |word| word == 'Check' }.join(' ')

      check.name = new_name
      check.save!
    end
  end
end
