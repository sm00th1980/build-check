# -*- encoding : utf-8 -*-
class AddStep30 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod launch portrait 736h 3x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 168,
        internal_name: 'step_30',
        klass: 'Steps::Step_30'
    )
  end
end
