# -*- encoding : utf-8 -*-
class AddCheckedIntoStepFiles < ActiveRecord::Migration
  def up
    add_column :step_files, :checked, :boolean

    StepFile.find_each do |step|
      step.checked = true
      step.save!
    end

    change_column :step_files, :checked, :boolean, null: false
  end

  def down
    remove_column :step_files, :checked
  end
end
