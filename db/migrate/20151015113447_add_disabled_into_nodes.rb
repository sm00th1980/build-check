# -*- encoding : utf-8 -*-
class AddDisabledIntoNodes < ActiveRecord::Migration
  def change
    add_column :nodes, :disabled, :boolean, null: false, default: false
  end
end
