# -*- encoding : utf-8 -*-
class UpdateSortTo10Times < ActiveRecord::Migration
  def up
    AppstoreCheck.all.each do |check|
      check.sort = check.sort * 10
      check.save!
    end
  end
end
