# -*- encoding : utf-8 -*-
class CreateProjectIcons < ActiveRecord::Migration
  def change
    create_table :project_icons do |t|
      t.integer :project_id, null: false
      t.integer :type_id, null: false
      t.string :name, null: false
      t.string :md5, null: false

      t.timestamps null: false
    end

    add_index :project_icons, [:project_id, :type_id], unique: true
  end
end
