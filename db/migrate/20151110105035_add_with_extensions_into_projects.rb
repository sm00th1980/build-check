# -*- encoding : utf-8 -*-
class AddWithExtensionsIntoProjects < ActiveRecord::Migration
  def up
    add_column :projects, :with_extensions, :boolean

    Project.all.each do |project|
      project.with_extensions = false
      project.save!
    end

    change_column :projects, :with_extensions, :boolean, null: false
  end

  def down
    remove_column :projects, :with_extensions
  end
end
