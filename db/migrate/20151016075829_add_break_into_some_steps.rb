# -*- encoding : utf-8 -*-
class AddBreakIntoSomeSteps < ActiveRecord::Migration
  def up
    [AppstoreCheck.step_5, AppstoreCheck.step_7, AppstoreCheck.step_8, AppstoreCheck.step_9, AppstoreCheck.step_10].each do |step|
      step.expected = "#{step.expected}\n"
      step.save!
    end
  end
end
