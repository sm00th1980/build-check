# -*- encoding : utf-8 -*-
class UpdateStep13 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_13
    step.name = 'Check iPhone settings 1x icon'
    step.expected = '%{name} (MD5 = %{md5}, SIZE = %{size})'
    step.sort = 150
    step.save!
  end
end
