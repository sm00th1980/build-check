# -*- encoding : utf-8 -*-
class AddStep23 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod 3x Icon',
        command: '-',
        expected: 'AppIcon60x60@3x.png (MD5 = d676993636dd01b57deb8f8d18df3299, SIZE = 180x180)',
        sort: 23,
        internal_name: 'step_23',
        klass: 'Steps::Step_23'
    )
  end
end
