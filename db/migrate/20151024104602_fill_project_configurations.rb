# -*- encoding : utf-8 -*-
class FillProjectConfigurations < ActiveRecord::Migration
  def up
    Project.all.each do |project|
      ProjectConfiguration.create!(project_id: project.id, type_id: ConfigurationType.armv7.id, value: 'bchjsbvfbhvfd')
      ProjectConfiguration.create!(project_id: project.id, type_id: ConfigurationType.arm64.id, value: 'fgmklbmgfmbfg')
    end
  end

  def down
    ProjectConfiguration.destroy_all
  end
end
