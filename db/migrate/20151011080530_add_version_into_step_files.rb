# -*- encoding : utf-8 -*-
class AddVersionIntoStepFiles < ActiveRecord::Migration
  def change
    add_column :step_files, :version, :string
    add_column :step_files, :local_path, :string
  end
end
