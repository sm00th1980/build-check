# -*- encoding : utf-8 -*-
class AddStep36 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad launch landscape 1x image',
        command: '-',
        expected: '%{name} (MD5 = %{md5}, SIZE = %{size})',
        sort: 177,
        internal_name: 'step_36',
        klass: 'Steps::Step_36'
    )
  end
end
