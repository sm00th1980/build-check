# -*- encoding : utf-8 -*-
class AddStep16 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iTunes Artworks are not present',
        command: '-',
        expected: "iTunesArtwork file is not present\niTunesArtwork@2x file is not present",
        sort: 16,
        internal_name: 'step_16',
        klass: 'Steps::Step_16'
    )
  end
end
