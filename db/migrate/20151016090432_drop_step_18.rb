# -*- encoding : utf-8 -*-
class DropStep18 < ActiveRecord::Migration
  def up
    AppstoreCheck.where(internal_name: 'step_18').delete_all
  end
end
