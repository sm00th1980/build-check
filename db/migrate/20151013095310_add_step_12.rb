# -*- encoding : utf-8 -*-
class AddStep12 < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad Icons',
        command: '-',
        expected: "AppIcon76x76~ipad.png (MD5 = efadc0a3dcfe7d8243ebaed6c05e3ef4, SIZE = 76x76)\nAppIcon76x76@2x~ipad.png (MD5 = 73533b632acafccbd51505c506085a76, SIZE = 152x152)",
        sort: 12,
        internal_name: 'step_12',
        klass: 'Steps::Step_12'
    )
  end
end
