# -*- encoding : utf-8 -*-
class FillConfigurationTypes < ActiveRecord::Migration
  def up
    ConfigurationType.create!(name: 'ARMv7', internal_name: 'armv7')
    ConfigurationType.create!(name: 'ARM64', internal_name: 'arm64')
  end

  def down
    ConfigurationType.delete_all
  end
end
