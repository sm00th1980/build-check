# -*- encoding : utf-8 -*-
class UpdateStep7 < ActiveRecord::Migration
  def up
    step_7 = AppstoreCheck.step_7
    step_7.expected = '%{platform_version}'
    step_7.save!
  end

  def down
    step_7 = AppstoreCheck.step_7
    step_7.expected = '8.1'
    step_7.save!
  end
end
