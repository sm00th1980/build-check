source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.2.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'whenever', require: false

gem 'bootstrap-sass', '~> 3.3.5'
gem 'pg'
gem 'draper'
gem 'net-ssh'
gem 'rubyzip'
gem 'net-scp'
gem 'time_difference'
gem 'chronic_duration', '~> 0.10.6'
gem 'highlight_js-rails'
gem 'premailer-rails'
gem 'sidekiq'
gem 'highline'
gem 'typhoeus'
gem 'git'
gem 'react-rails', '~> 1.3.1'
gem 'underscore-rails'
gem 'bootstrap_notify', git: 'https://github.com/sm00th1980/bootstrap-notify-gem.git'
gem 'pngdefry'
gem 'devise'

group :development do
  gem 'puma'
  gem 'magic_encoding'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'pry-rails'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano3-unicorn'
  gem 'capistrano', '~> 3.1'
  gem 'capistrano-bundler', '~> 1.1.2'
  #gem 'rack-mini-profiler'
  gem 'capistrano-sidekiq'
end

group :production do
  # Use Unicorn as the app server
  gem 'unicorn'
  gem 'exception_notification'
  gem 'capistrano3-puma'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.0'
  gem 'factory_girl_rails'
  gem 'assert_difference'
  gem 'faker'
  gem 'webmock'
  gem 'timecop'
  gem 'simplecov', require: false
end