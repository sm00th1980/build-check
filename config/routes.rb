# -*- encoding : utf-8 -*-
Rails.application.routes.draw do
  #/login, /logout
  devise_for :users, :path => "/", :path_names => {:sign_in => 'login', :sign_out => 'logout'}, :controllers => {:sessions => "sessions"}
  #/login, /logout

  get '404', :to => 'application#page_not_found', as: :no_route

  get 'project/check' => 'project/check#check', as: :check_project

  resource :project, :only => [] do
    get 'new' => 'project', as: :new
    post '/' => 'project#create', as: :create
    get ':id' => 'project#edit', as: :edit
    post ':id' => 'project#update', as: :update
    delete ':id' => 'project#destroy', as: :destroy
    post ':id/upload' => 'project/upload#upload', as: :upload
    get ':id/show' => 'project/show#index', as: :show
  end

  root :to => 'root#index'
end
