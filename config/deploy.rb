# -*- encoding : utf-8 -*-
# config valid only for current version of Capistrano
lock '3.4.0'

app = 'build-check'

set :application, app
set :repo_url, 'git@bitbucket.org:sm00th1980/build-check.git'

set :deploy_to, "/srv/www/#{app}"

set :scm, :git
set :scm_passphrase, ""

set :stages, ["production"]
set :default_stage, "production"

set :puma_pid, "/srv/www/#{app}/shared/pids/puma.pid"
set :puma_conf, "/srv/www/#{app}/current/config/puma.rb"
set :puma_preload_app, true
set :puma_state, "/srv/www/#{app}/shared/pids/puma.state"

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do

    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do

    end
  end

end
