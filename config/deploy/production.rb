# -*- encoding : utf-8 -*-
server "build-check.mercury.office", user: 'mimedia-atest', port: 22, roles: %w{web app db}

after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'puma:restart'
  end
end
