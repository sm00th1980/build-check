# -*- encoding : utf-8 -*-
require 'net/ssh'
require 'net/scp'

class SshEndpoint

  def self.execute(command, endpoint=Node.appstore_node)
    output = nil
    Net::SSH.start(endpoint.host, endpoint.login, password: endpoint.password) do |ssh|
      output = ssh.exec!(command)
    end
    output
  end

  def self.upload(local_file, endpoint=Node.appstore_node)
    host = endpoint.host
    login = endpoint.login
    password = endpoint.password
    remote_file = [endpoint.upload_dir, local_file.to_s.split('/').last].join('/')

    execute("mkdir #{endpoint.upload_dir}")

    Net::SCP.upload!(host, login, local_file.to_s, remote_file.to_s, {recursive: true, ssh: {password: password}})
  end

end
