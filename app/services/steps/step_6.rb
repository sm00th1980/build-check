# -*- encoding : utf-8 -*-
module Steps
  class Step_6 < Base::StepWithDefaults

    private
    def params
      version = @step_file.project.last_version rescue nil
      {app: @step_file.project.app_dir, version: version}
    end

  end
end
