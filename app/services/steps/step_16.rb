# -*- encoding : utf-8 -*-
module Steps
  class Step_16 < Base::StepAbsenceIcons

    private
    def icons
      ['iTunesArtwork', 'iTunesArtwork@2x']
    end

  end
end
