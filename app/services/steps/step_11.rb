# -*- encoding : utf-8 -*-
module Steps
  class Step_11 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.iphone_2x? }.first
    end
    
  end
end
