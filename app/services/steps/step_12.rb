# -*- encoding : utf-8 -*-
module Steps
  class Step_12 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.ipad_1x? }.first
    end

  end
end
