# -*- encoding : utf-8 -*-
module Steps
  class Step_34 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.ipad_launch_landscape_2x? }.first
    end

  end
end
