# -*- encoding : utf-8 -*-
module Steps
  class Step_9 < Base::StepWithDefaults
    private
    def params
      {app: @step_file.project.app_dir, xcode_version: @step_file.project.xcode_version}
    end
  end
end
