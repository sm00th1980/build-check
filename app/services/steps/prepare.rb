# -*- encoding : utf-8 -*-
require 'zip'
require 'ssh_endpoint'

module Steps
  class Prepare

    attr_reader :result

    def initialize(check, step_file)
      @step_file = step_file
      @result = nil
    end

    def run
      @temp_dir = create_temp_dir
      
      if @step_file.name.blank? or !File.exists?(@step_file.name)
        raise("Filename with name <%s> not found" % @step_file.name)
      end

      @result = false

      begin
        if @step_file.project.with_extensions?
          unzip_with_extensions(@step_file.name, @temp_dir)
        else
          unzip_without_extensions(@step_file.name, @temp_dir)
        end
      rescue
        clean_on_failure
      else
        @step_file.local_path = @step_file.project.with_extensions? ? "%s/Payload" % @temp_dir : @temp_dir
        @step_file.save!

        begin
          clean_remote_dir

          if @step_file.project.with_extensions?
            SshEndpoint.upload(Rails.root.join("%s/Payload" % @temp_dir, "%s.app" % @step_file.project.app_name), Node.appstore_node)
          else
            SshEndpoint.upload(Rails.root.join(@temp_dir, "%s.app" % @step_file.project.app_name))
          end
        rescue
          clean_on_failure
        else
          @result = true
        end
      end

      @result
    end

    def expected
      "-"
    end

    def got
      "-"
    end

    def command
      "-"
    end

    def id
      AppstoreCheck.prepare.id
    end

    def name
      AppstoreCheck.prepare.name
    end

    def status
      return 'unknown' if @result.nil?
      return 'passed' if @result == true
      'failed'
    end

    private
    def unzip_without_extensions(source_file, dest_dir)
      #1. extract MiMedia.zip
      Zip::File.open(source_file) do |zip_file|
        zip_file.each do |entry|
          if entry.name == zipped_file
            entry.extract("#{dest_dir}/#{entry.name}")
          end
        end
      end

      #2. unzip MiMedia.zip
      if File.exist?(Rails.root.join(@temp_dir, zipped_file))
        Zip::File.open(Rails.root.join(@temp_dir, zipped_file)) do |zip_file|
          zip_file.each do |entry|
            entry.extract("#{dest_dir}/#{entry.name}")
          end
        end
      end

      #3. drop MiMedia.zip
      FileUtils.rm(Rails.root.join(@temp_dir, zipped_file))
    end

    def unzip_with_extensions(source_file, dest_dir)
      #1. extract MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.ipa
      Zip::File.open(source_file) do |zip_file|
        zip_file.each do |entry|
          if entry.name == zipped_file
            entry.extract("#{dest_dir}/#{entry.name}")
          end
        end
      end

      #2. unzip MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.ipa
      if File.exist?(Rails.root.join(@temp_dir, zipped_file))
        Zip::File.open(Rails.root.join(@temp_dir, zipped_file)) do |zip_file|
          zip_file.each do |entry|
            entry.extract("#{dest_dir}/#{entry.name}")
          end
        end
      end

      #3. drop MiMedia.zip
      FileUtils.rm(Rails.root.join(@temp_dir, zipped_file))
    end

    def clean_remote_dir
      SshEndpoint.execute("rm -rf #{Node.appstore_node.upload_dir}")
    end

    def create_temp_dir
      temp_dir_ = File.join('/tmp', Tempfile.new('').path.split('/').last)
      FileUtils.remove_entry(temp_dir_) if Dir.exist?(temp_dir_) or File.exist?(temp_dir_)
      Dir.mkdir(temp_dir_, 0700)
      temp_dir_
    end

    def clean_on_failure
      FileUtils.remove_entry(@temp_dir)
    end

    def zipped_file
      if @step_file.project.with_extensions?
        #remove extension by last dot -> remain only name of file
        "%s.ipa" % @step_file.original_name.split('.')[0..-2].join('.')
      else
        "%s.zip" % @step_file.project.zip_name
      end

    end

  end
end
