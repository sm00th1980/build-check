# -*- encoding : utf-8 -*-
module Steps
  class Step_30 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.iphone_launch_portrait_736h_3x? }.first
    end

  end
end
