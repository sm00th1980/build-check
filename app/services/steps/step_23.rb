# -*- encoding : utf-8 -*-
module Steps
  class Step_23 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.iphone_3x? }.first
    end

  end
end
