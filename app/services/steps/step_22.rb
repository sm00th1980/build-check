# -*- encoding : utf-8 -*-
module Steps
  class Step_22 < Base::StepWithDefaults

    def got
      if output.include?('(architecture armv7):')
        return 'Architecture ARMv7 has been supported'
      end

      'Architecture ARMv7 has not been supported'
    end

    private
    def params
      {executable: @step_file.project.app_path}
    end

  end
end
