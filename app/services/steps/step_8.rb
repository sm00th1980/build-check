# -*- encoding : utf-8 -*-
module Steps
  class Step_8 < Base::StepWithDefaults
    private
    def params
      {app: @step_file.project.app_dir, platform_build: @step_file.project.platform_build}
    end
  end
end
