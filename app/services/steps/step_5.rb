# -*- encoding : utf-8 -*-
module Steps
  class Step_5 < Base::StepWithDefaults
    private
    def params
      {app: @step_file.project.app_dir, bundle_id: @step_file.project.bundle_id}
    end
  end
end
