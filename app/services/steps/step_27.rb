# -*- encoding : utf-8 -*-
module Steps
  class Step_27 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.ipad_settings_1x? }.first
    end

  end
end
