# -*- encoding : utf-8 -*-
module Steps
  class Step_21 < Base::StepWithDefaults

    private
    def params
      short_version = @step_file.project.short_version rescue nil
      {app: @step_file.project.app_dir, short_version: short_version}
    end

  end
end
