# -*- encoding : utf-8 -*-
module Steps
  class Step_28 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.ipad_settings_2x? }.first
    end

  end
end
