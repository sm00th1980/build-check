# -*- encoding : utf-8 -*-
module Steps
  class Step_7 < Base::StepWithDefaults
    private
    def params
      {app: @step_file.project.app_dir, platform_version: @step_file.project.platform_version}
    end
  end
end
