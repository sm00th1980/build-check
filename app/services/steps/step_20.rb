# -*- encoding : utf-8 -*-
module Steps
  class Step_20 < Base::StepWithDefaults

    def got
      result = []
      ['armv7', 'arm64'].each do |arch|
        result << check_pie(output.split(params[:executable]).select { |el| !el.blank? }, arch)
      end

      result.join("\n")
    end

    private
    def params
      {executable: @step_file.project.app_path}
    end

    def check_pie(output, arch)
      exist = false
      pie_present = false

      if output.select { |el| el.include? arch }.count == 1
        exist = true
        pie_present = output.select { |el| el.include? arch }.first.split(//).last(4).join == "PIE\n" ? true : false
      end

      return "#{arch} is present and has PIE binary support" if exist and pie_present
      return "#{arch} is present but not has PIE binary support" if exist and !pie_present
      "#{arch} is not present"
    end

  end
end
