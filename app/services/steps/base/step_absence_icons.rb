# -*- encoding : utf-8 -*-
require 'pngdefry'

module Steps
  module Base
    class StepAbsenceIcons

      attr_reader :got, :expected, :result

      def initialize(check, step_file)
        @check = check
        @result = nil
        @step_file = step_file
      end

      def id
        @check.id
      end

      def name
        @check.name
      end

      def command
        @check.read_attribute_before_type_cast('command')
      end

      def run
        #begin
        @expected = @check.read_attribute_before_type_cast('expected')
        @got = check_icons
        #rescue
        #else
        #no exception
        if @expected == @got
          @result = true
        else
          @result = false
        end
        #end
      end

      def status
        return 'failed' if result == false
        return 'passed' if result == true
        'unknown'
      end

      private
      def path
        "%s/%s" % [@step_file.local_path, ("%s.app" % @step_file.project.app_name)]
      end

      def check_icons
        outputs = []

        icons.each do |icon|
          if icon_exists?(icon)
            outputs << "#{icon} file is present"
          else
            outputs << "#{icon} file is not present"
          end
        end

        outputs.join("\n")
      end

      def icon_exists?(icon_name)
        File.exist?("#{path}/#{icon_name}")
      end

      def icons
        raise "Should be overrided in subclasses"
      end
    end
  end
end
