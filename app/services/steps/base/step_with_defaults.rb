# -*- encoding : utf-8 -*-
module Steps
  module Base
    class StepWithDefaults

      attr_reader :expected

      def initialize(check, step_file)
        @check = check
        @step_file = step_file
      end

      def id
        @check.id
      end

      def name
        @check.name
      end

      def command
        @check.read_attribute_before_type_cast('command') % params
      end

      def run
        begin
          @expected = @check.read_attribute_before_type_cast('expected') % params
          @got = got
        rescue
        end
      end

      def got
        output
      end

      def output
        SshEndpoint.execute(command)
      end

      def result
        if @expected.nil? or @got.nil?
          return nil
        else
          return true if @expected == @got
        end

        false
      end

      def status
        return 'failed' if result == false
        return 'passed' if result == true
        'unknown'
      end

      private
      def params
        {app: "#{@step_file.project.app_path}"}
      end

    end
  end
end
