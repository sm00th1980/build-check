# -*- encoding : utf-8 -*-
require 'pngdefry'

module Steps
  module Base
    class StepExistIcon

      attr_reader :got, :expected, :result

      def initialize(check, step_file)
        @check = check
        @result = nil
        @step_file = step_file
      end

      def id
        @check.id
      end

      def name
        @check.name
      end

      def command
        @check.read_attribute_before_type_cast('command')
      end

      def run
        #begin
          @expected = @check.read_attribute_before_type_cast('expected') % params
          @got = perform_got
        #rescue
        #else
          #no exception
          if @expected == @got
            @result = true
          else
            @result = false
          end
        #end
      end

      def status
        return 'failed' if result == false
        return 'passed' if result == true
        'unknown'
      end

      private
      def path
        "%s/%s" % [@step_file.local_path, ("%s.app" % @step_file.project.app_name)]
      end

      def check_icon(icon_name)
        icon_path = "#{path}/#{icon_name}"

        if File.exist?(icon_path)
          md5 = Digest::MD5.file(icon_path).to_s

          width = Pngdefry.dimensions(icon_path).first
          height = Pngdefry.dimensions(icon_path).last
        else
          md5 = nil
          width = nil
          height = nil
        end

        {exist: File.exist?(icon_path), md5: md5, width: width, height: height}
      end

      def perform_got
        result = check_icon(icon.name)
        if result[:exist]
          output = "#{icon.name} (MD5 = #{result[:md5]}, SIZE = #{result[:width]}x#{result[:height]})"
        else
          output = "#{icon.name} not exist"
        end

        output
      end

      def icon
        raise "Should be overrided in subclasses"
      end

      def params
        {name: icon.name, md5: icon.md5, size: icon.type.size}
      end
    end
  end
end
