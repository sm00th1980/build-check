# -*- encoding : utf-8 -*-
module Steps
  class Step_19 < Base::StepWithDefaults

    def got
      if output.include?('(architecture arm64):')
        return 'Architecture ARM64 has been supported'
      end

      'Architecture ARM64 has not been supported'
    end

    private
    def params
      {executable: @step_file.project.app_path}
    end

  end
end
