# -*- encoding : utf-8 -*-
module Steps
  class Step_14 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.iphone_launch_1x? }.first
    end

  end
end
