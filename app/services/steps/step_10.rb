# -*- encoding : utf-8 -*-
module Steps
  class Step_10 < Base::StepWithDefaults
    private
    def params
      {app: @step_file.project.app_dir, xcode_build: @step_file.project.xcode_build}
    end
  end
end
