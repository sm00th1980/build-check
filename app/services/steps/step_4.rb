# -*- encoding : utf-8 -*-
module Steps
  class Step_4 < Base::StepWithDefaults

    def got
      output.gsub("\t", '        ')
    end

    private
    def params
      {provision_path: @step_file.project.provision.path, provision_value: @step_file.project.provision.value}
    end

  end
end
