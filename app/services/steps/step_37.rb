# -*- encoding : utf-8 -*-
module Steps
  class Step_37 < Base::StepWithDefaults

    def got
      output.split("#{@step_file.project.app_path} (architecture arm64):\n").last.strip
    end

    private
    def params
      configuration = @step_file.project.configurations.select { |configuration| configuration.type.arm64? }.first
      {configuration_path: @step_file.project.app_path, configuration_value: configuration.value}
    end

  end
end
