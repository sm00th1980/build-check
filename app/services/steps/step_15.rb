# -*- encoding : utf-8 -*-
module Steps
  class Step_15 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.ipad_launch_portrait_1x? }.first
    end


  end
end
