# -*- encoding : utf-8 -*-
module Steps
  class Step_36 < Base::StepExistIcon

    private
    def icon
      @step_file.project.icons.select { |icon| icon.type.ipad_launch_landscape_1x? }.first
    end

  end
end
