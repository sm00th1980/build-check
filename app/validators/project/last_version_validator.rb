# -*- encoding : utf-8 -*-
class Project::LastVersionValidator < ActiveModel::Validator

  def validate(project)
    if !valid_last_version?(project.last_version)
      project.errors.add(:base, I18n.t('validation.project.last_version_is_invalid') % project.last_version)
    end
  end

  private
  def valid_last_version?(last_version)
    reassembled_last_version = last_version.split('.').map { |s| s.to_i }.join('.') rescue nil
    if last_version.present? and last_version == reassembled_last_version and last_version.split('.').count >= 2
      return true
    end

    false
  end

end
