# -*- encoding : utf-8 -*-
module ApplicationHelper
  def root_page?
    [root_path].include? request.original_fullpath
  end

  def to_bool!(value)
    val_ = value.to_s.downcase rescue nil

    return true if val_.present? and val_ == 'true'
    return false if val_.present? and val_ == 'false'

    nil
  end
end
