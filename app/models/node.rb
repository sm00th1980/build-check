# -*- encoding : utf-8 -*-
class Node < ActiveRecord::Base
  def self.appstore_node
    where(disabled: false).first
  end
end
