# -*- encoding : utf-8 -*-
class Project < ActiveRecord::Base
  belongs_to :user
  has_many :step_files
  has_many :icons, class_name: 'ProjectIcon'
  has_one :provision, class_name: 'ProjectProvision'
  has_many :configurations, class_name: 'ProjectConfiguration'

  validates :name, :app_name, :zip_name, :platform_version, :platform_build, :xcode_version, :xcode_build, :bundle_id, presence: true
  validates :check_armv7_support,
            :check_arm64_support,
            :check_pie_support,
            :with_extensions,
            :check_iphone_1x_icon_support,
            :check_iphone_2x_icon_support,
            :check_iphone_3x_icon_support,
            :check_ipad_1x_icon_support,
            :check_ipad_2x_icon_support,
            :check_iphone_settings_1x_icon_support,
            :check_iphone_settings_2x_icon_support,
            :check_ipad_settings_1x_icon_support,
            :check_ipad_settings_2x_icon_support,
            :check_iphone_launch_1x_icon_support,
            :check_iphone_launch_2x_icon_support,
            :check_iphone_launch_568h_2x_icon_support,
            :check_iphone_launch_667h_2x_icon_support,
            :check_iphone_launch_portrait_736h_3x_icon_support,
            :check_iphone_launch_landscape_736h_3x_icon_support,
            :check_ipad_launch_portrait_1x_icon_support,
            :check_ipad_launch_landscape_1x_icon_support,
            :check_ipad_launch_portrait_2x_icon_support,
            :check_ipad_launch_landscape_2x_icon_support,
            :check_provision,
            :check_configuration_armv7,
            :check_configuration_arm64,
            inclusion: {in: [true, false], message: "can't be blank"}, allow_nil: false

  validates_with Project::LastVersionValidator

  def app_dir
    "#{Node.first.upload_dir}/#{app_name}.app"
  end

  def app_path
    "#{Node.first.upload_dir}/#{app_name}.app/#{app_name}"
  end

  def short_version
    last_version.split('.').first(3).join('.') rescue nil
  end

  def steps
    steps = AppstoreCheck.streaming_checks
    steps = steps.reject { |step| step.step_22? } if !check_armv7_support?
    steps = steps.reject { |step| step.step_19? } if !check_arm64_support?
    steps = steps.reject { |step| step.step_20? } if !check_pie_support?

    steps = steps.reject { |step| step.step_24? } if !check_iphone_1x_icon_support?
    steps = steps.reject { |step| step.step_11? } if !check_iphone_2x_icon_support?
    steps = steps.reject { |step| step.step_23? } if !check_iphone_3x_icon_support?

    steps = steps.reject { |step| step.step_12? } if !check_ipad_1x_icon_support?
    steps = steps.reject { |step| step.step_25? } if !check_ipad_2x_icon_support?

    steps = steps.reject { |step| step.step_13? } if !check_iphone_settings_1x_icon_support?
    steps = steps.reject { |step| step.step_26? } if !check_iphone_settings_2x_icon_support?
    steps = steps.reject { |step| step.step_27? } if !check_ipad_settings_1x_icon_support?
    steps = steps.reject { |step| step.step_28? } if !check_ipad_settings_2x_icon_support?

    steps = steps.reject { |step| step.step_14? } if !check_iphone_launch_1x_icon_support?
    steps = steps.reject { |step| step.step_33? } if !check_iphone_launch_2x_icon_support?
    steps = steps.reject { |step| step.step_32? } if !check_iphone_launch_568h_2x_icon_support?
    steps = steps.reject { |step| step.step_31? } if !check_iphone_launch_667h_2x_icon_support?
    steps = steps.reject { |step| step.step_30? } if !check_iphone_launch_portrait_736h_3x_icon_support?
    steps = steps.reject { |step| step.step_29? } if !check_iphone_launch_landscape_736h_3x_icon_support?

    steps = steps.reject { |step| step.step_15? } if !check_ipad_launch_portrait_1x_icon_support?
    steps = steps.reject { |step| step.step_36? } if !check_ipad_launch_landscape_1x_icon_support?
    steps = steps.reject { |step| step.step_35? } if !check_ipad_launch_portrait_2x_icon_support?
    steps = steps.reject { |step| step.step_34? } if !check_ipad_launch_landscape_2x_icon_support?

    steps = steps.reject { |step| step.step_4? } if !check_provision?

    steps = steps.reject { |step| step.step_3? } if !check_configuration_armv7?
    steps = steps.reject { |step| step.step_37? } if !check_configuration_arm64?

    steps
  end

end
