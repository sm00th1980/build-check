# -*- encoding : utf-8 -*-
class AppstoreCheck < ActiveRecord::Base

  scope :streaming_checks, -> { where.not(id: [prepare]).order(:sort) }

  def self.prepare
    find_by(internal_name: 'prepare')
  end

  def self.step_1
    find_by(internal_name: 'step_1')
  end

  def self.step_3
    find_by(internal_name: 'step_3')
  end

  def step_3?
    self == self.class.step_3
  end

  def self.step_4
    find_by(internal_name: 'step_4')
  end

  def step_4?
    self == self.class.step_4
  end

  def self.step_5
    find_by(internal_name: 'step_5')
  end

  def self.step_6
    find_by(internal_name: 'step_6')
  end

  def self.step_7
    find_by(internal_name: 'step_7')
  end

  def self.step_8
    find_by(internal_name: 'step_8')
  end

  def self.step_9
    find_by(internal_name: 'step_9')
  end

  def self.step_10
    find_by(internal_name: 'step_10')
  end

  def self.step_11
    find_by(internal_name: 'step_11')
  end

  def step_11?
    self == self.class.step_11
  end

  def self.step_12
    find_by(internal_name: 'step_12')
  end

  def step_12?
    self == self.class.step_12
  end

  def self.step_13
    find_by(internal_name: 'step_13')
  end

  def step_13?
    self == self.class.step_13
  end

  def self.step_14
    find_by(internal_name: 'step_14')
  end

  def step_14?
    self == self.class.step_14
  end

  def self.step_15
    find_by(internal_name: 'step_15')
  end

  def step_15?
    self == self.class.step_15
  end

  def self.step_16
    find_by(internal_name: 'step_16')
  end

  def self.step_19
    find_by(internal_name: 'step_19')
  end

  def step_19?
    self == self.class.step_19
  end

  def self.step_20
    find_by(internal_name: 'step_20')
  end

  def step_20?
    self == self.class.step_20
  end

  def self.step_21
    find_by(internal_name: 'step_21')
  end

  def self.step_22
    find_by(internal_name: 'step_22')
  end

  def step_22?
    self == self.class.step_22
  end

  def self.step_23
    find_by(internal_name: 'step_23')
  end

  def step_23?
    self == self.class.step_23
  end

  def self.step_24
    find_by(internal_name: 'step_24')
  end

  def step_24?
    self == self.class.step_24
  end

  def self.step_25
    find_by(internal_name: 'step_25')
  end

  def step_25?
    self == self.class.step_25
  end

  def self.step_26
    find_by(internal_name: 'step_26')
  end

  def step_26?
    self == self.class.step_26
  end

  def self.step_27
    find_by(internal_name: 'step_27')
  end

  def step_27?
    self == self.class.step_27
  end

  def self.step_28
    find_by(internal_name: 'step_28')
  end

  def step_28?
    self == self.class.step_28
  end

  def self.step_29
    find_by(internal_name: 'step_29')
  end

  def step_29?
    self == self.class.step_29
  end

  def self.step_30
    find_by(internal_name: 'step_30')
  end

  def step_30?
    self == self.class.step_30
  end

  def self.step_31
    find_by(internal_name: 'step_31')
  end

  def step_31?
    self == self.class.step_31
  end

  def self.step_32
    find_by(internal_name: 'step_32')
  end

  def step_32?
    self == self.class.step_32
  end

  def self.step_33
    find_by(internal_name: 'step_33')
  end

  def step_33?
    self == self.class.step_33
  end

  def self.step_34
    find_by(internal_name: 'step_34')
  end

  def step_34?
    self == self.class.step_34
  end

  def self.step_35
    find_by(internal_name: 'step_35')
  end

  def step_35?
    self == self.class.step_35
  end

  def self.step_36
    find_by(internal_name: 'step_36')
  end

  def step_36?
    self == self.class.step_36
  end

  def self.step_37
    find_by(internal_name: 'step_37')
  end

  def step_37?
    self == self.class.step_37
  end

  def step(step_file)
    klass.constantize.new(self, step_file)
  end

  def self.showing_steps(project)
    where(id: [prepare.id, project.steps.map { |step| step.id }].flatten.uniq).order(:sort).map { |check| check.step(StepFile.new(project: project)) }
  end

  def icon_type
    IconType.find_by(id: icon_type_id)
  end

end
