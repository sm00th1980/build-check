# -*- encoding : utf-8 -*-
class ConfigurationType < ActiveRecord::Base
  has_many :configurations, class_name: 'ProjectConfiguration', foreign_key: :type_id

  validates :name, :internal_name, presence: true

  def self.armv7
    find_by(internal_name: 'armv7')
  end

  def armv7?
    self == self.class.armv7
  end

  def self.arm64
    find_by(internal_name: 'arm64')
  end

  def arm64?
    self == self.class.arm64
  end

end
