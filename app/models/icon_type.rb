# -*- encoding : utf-8 -*-
class IconType < ActiveRecord::Base

  has_many :icons, class_name: 'ProjectIcon', foreign_key: :type_id

  def self.iphone_1x
    find_by(internal_name: 'iphone_1x')
  end

  def iphone_1x?
    self == self.class.iphone_1x
  end

  def self.iphone_2x
    find_by(internal_name: 'iphone_2x')
  end

  def iphone_2x?
    self == self.class.iphone_2x
  end

  def self.iphone_3x
    find_by(internal_name: 'iphone_3x')
  end

  def iphone_3x?
    self == self.class.iphone_3x
  end

  def self.ipad_1x
    find_by(internal_name: 'ipad_1x')
  end

  def ipad_1x?
    self == self.class.ipad_1x
  end

  def self.ipad_2x
    find_by(internal_name: 'ipad_2x')
  end

  def ipad_2x?
    self == self.class.ipad_2x
  end

  def self.iphone_settings_1x
    find_by(internal_name: 'iphone_settings_1x')
  end

  def iphone_settings_1x?
    self == self.class.iphone_settings_1x
  end

  def self.iphone_settings_2x
    find_by(internal_name: 'iphone_settings_2x')
  end

  def iphone_settings_2x?
    self == self.class.iphone_settings_2x
  end

  def self.ipad_settings_1x
    find_by(internal_name: 'ipad_settings_1x')
  end

  def ipad_settings_1x?
    self == self.class.ipad_settings_1x
  end

  def self.ipad_settings_2x
    find_by(internal_name: 'ipad_settings_2x')
  end

  def ipad_settings_2x?
    self == self.class.ipad_settings_2x
  end

  def self.iphone_launch_1x
    find_by(internal_name: 'iphone_launch_1x')
  end

  def iphone_launch_1x?
    self == self.class.iphone_launch_1x
  end

  def self.iphone_launch_2x
    find_by(internal_name: 'iphone_launch_2x')
  end

  def iphone_launch_2x?
    self == self.class.iphone_launch_2x
  end

  def self.iphone_launch_568h_2x
    find_by(internal_name: 'iphone_launch_568h_2x')
  end

  def iphone_launch_568h_2x?
    self == self.class.iphone_launch_568h_2x
  end

  def self.iphone_launch_667h_2x
    find_by(internal_name: 'iphone_launch_667h_2x')
  end

  def iphone_launch_667h_2x?
    self == self.class.iphone_launch_667h_2x
  end

  def self.iphone_launch_portrait_736h_3x
    find_by(internal_name: 'iphone_launch_portrait_736h_3x')
  end

  def iphone_launch_portrait_736h_3x?
    self == self.class.iphone_launch_portrait_736h_3x
  end

  def self.iphone_launch_landscape_736h_3x
    find_by(internal_name: 'iphone_launch_landscape_736h_3x')
  end

  def iphone_launch_landscape_736h_3x?
    self == self.class.iphone_launch_landscape_736h_3x
  end

  def self.ipad_launch_portrait_1x
    find_by(internal_name: 'ipad_launch_portrait_1x')
  end

  def ipad_launch_portrait_1x?
    self == self.class.ipad_launch_portrait_1x
  end

  def self.ipad_launch_landscape_1x
    find_by(internal_name: 'ipad_launch_landscape_1x')
  end

  def ipad_launch_landscape_1x?
    self == self.class.ipad_launch_landscape_1x
  end

  def self.ipad_launch_portrait_2x
    find_by(internal_name: 'ipad_launch_portrait_2x')
  end

  def ipad_launch_portrait_2x?
    self == self.class.ipad_launch_portrait_2x
  end

  def self.ipad_launch_landscape_2x
    find_by(internal_name: 'ipad_launch_landscape_2x')
  end

  def ipad_launch_landscape_2x?
    self == self.class.ipad_launch_landscape_2x
  end

end
