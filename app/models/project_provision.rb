# -*- encoding : utf-8 -*-
class ProjectProvision < ActiveRecord::Base
  belongs_to :project

  def path
    "#{project.app_dir}/embedded.mobileprovision"
  end
end
