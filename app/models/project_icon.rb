# -*- encoding : utf-8 -*-
class ProjectIcon < ActiveRecord::Base
  belongs_to :project
  belongs_to :type, class_name: 'IconType'
end
