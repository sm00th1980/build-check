# -*- encoding : utf-8 -*-
class ProjectConfiguration < ActiveRecord::Base
  belongs_to :project
  belongs_to :type, class_name: 'ConfigurationType'
end
