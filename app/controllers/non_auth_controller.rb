# -*- encoding : utf-8 -*-
class NonAuthController < ApplicationController
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format.json? }
end
