# -*- encoding : utf-8 -*-
class SessionsController < Devise::SessionsController

  def create
    if user and password_valid?
      #auth
      sign_in user
      redirect_to root_path
    else
      #non auth
      redirect_to new_user_session_path, alert: I18n.t('devise.failure.user.invalid')
    end
  end

  def destroy
    sign_out current_user if current_user.present?
    redirect_to new_user_session_path, notice: I18n.t('devise.sessions.user.signed_out')
  end

  private
  def password_valid?
    return true if user and user.valid_password?(password)
    false
  end

  def sanitize_params
    params.permit(:username, :password)
  end

  def username
    sanitize_params[:username] rescue nil
  end

  def password
    sanitize_params[:password] rescue nil
  end

  def user
    User.find_by(email: username)
  end

end
