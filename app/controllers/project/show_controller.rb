# -*- encoding : utf-8 -*-
class Project::ShowController < AuthController
  before_action :find_project!, only: :index

  def index
    flash[:notice] = project_params[:message]
    @steps = AppstoreCheck.showing_steps(@project)

    @general_steps = @steps.select { |step| general_steps.include? step.id }
    @icon_steps = StepDecorator.decorate_collection(@steps.reject { |step| general_steps.include?(step.id) })
  end

  private
  def project_params
    params.permit(:id, :message)
  end

  def find_project!
    project = Project.find_by(id: project_params[:id], user: current_user)
    if project.blank?
      if current_user.has_projects?
        flash[:alert] = I18n.t('project.failure.not_found') % project_params[:id]
        path = show_project_path(current_user.projects.first)
      else
        flash[:alert] = I18n.t('project.create_new') % project_params[:id]
        path = new_project_path
      end

      redirect_to path
    else
      @project = project.decorate
    end
  end

  def general_steps
    [
        AppstoreCheck.prepare,
        AppstoreCheck.step_1,
        AppstoreCheck.step_3,
        AppstoreCheck.step_4,
        AppstoreCheck.step_5,
        AppstoreCheck.step_6,
        AppstoreCheck.step_21,
        AppstoreCheck.step_7,
        AppstoreCheck.step_8,
        AppstoreCheck.step_9,
        AppstoreCheck.step_10,
        AppstoreCheck.step_19, #arm64
        AppstoreCheck.step_20, #pie
        AppstoreCheck.step_22, #armv7
        AppstoreCheck.step_37 #configuration armv7
    ].map { |step| step.id }
  end

end
