# -*- encoding : utf-8 -*-
class Project::CheckController < AuthController
  include ActionController::Live

  before_action :find_file!, only: :check

  def check
    response.headers['Content-Type'] = 'text/event-stream'
    @sse = ServerSide::SSE.new(response.stream)
    begin
      streaming_steps if streaming_prepare
    rescue IOError
    ensure
      @file.checked = true
      @file.save!

      @sse.write({finished: true})
      @sse.close
    end
  end

  private
  def streaming_steps
    @file.project.steps.each do |check|
      step = StepDecorator.decorate(check.step(@file))
      @sse.write({
                     id: step.id,
                     status: 'running',
                     name: step.name,
                     expected: nil,
                     command: step.command,
                     got: nil
                 })

      step.run
      @sse.write({
                     id: step.id,
                     status: step.status,
                     name: step.name,
                     expected: step.expected,
                     command: step.command,
                     got: step.got
                 })
    end
  end

  def streaming_prepare
    step = Steps::Prepare.new(nil, @file)
    @sse.write({
                   id: step.id,
                   status: 'running',
                   name: step.name,
                   expected: nil,
                   command: step.command,
                   got: nil
               })
    step.run
    @sse.write({
                   id: step.id,
                   status: step.status,
                   name: step.name,
                   expected: step.expected,
                   command: step.command,
                   got: step.got
               })

    step.result
  end

  def check_params
    params.permit(:id)
  end

  def find_file!
    @file = StepFile.find_by(id: check_params[:id], checked: false)
    if @file.blank?
      render json: {success: false, error: I18n.t('step_file.failure.not_found_or_already_checked') % check_params[:id]} and return
    end
  end

end


