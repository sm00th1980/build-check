# -*- encoding : utf-8 -*-
class ProjectController < AuthController

  before_action :find_project!, only: [:destroy, :edit, :update]
  before_action :find_icons!, only: [:create, :update]
  before_action :find_provision!, only: [:create, :update]
  before_action :find_configuration_armv7!, only: [:create, :update]
  before_action :find_configuration_arm64!, only: [:create, :update]

  def create
    new_project = Project.new(user: current_user)
    update_project_with_params(new_project)

    if new_project.save
      save_icons(new_project)
      save_provision(new_project)
      save_configuration_armv7(new_project)
      save_configuration_arm64(new_project)

      result = {success: true, message: I18n.t('project.success.created'), url: show_project_path(new_project, message: I18n.t('project.success.created'))}
    else
      result = {success: false, message: new_project.errors.full_messages}
    end

    render json: result
  end

  def edit
    @project = @project.decorate
  end

  def new
    @project = Project.new(
        platform_version: '8.1',
        platform_build: '12B411',
        xcode_version: '0611',
        xcode_build: '12B411',
        check_armv7_support: true,
        check_arm64_support: true,
        check_pie_support: true,
        with_extensions: false,
        check_provision: true,
        check_configuration_armv7: false,
        check_configuration_arm64: false,
        check_iphone_1x_icon_support: false,
        check_iphone_2x_icon_support: true,
        check_iphone_3x_icon_support: true,
        check_ipad_1x_icon_support: true,
        check_ipad_2x_icon_support: true,
        check_iphone_settings_1x_icon_support: true,
        check_iphone_settings_2x_icon_support: true,
        check_ipad_settings_1x_icon_support: true,
        check_ipad_settings_2x_icon_support: true,
        check_iphone_launch_1x_icon_support: true,
        check_iphone_launch_2x_icon_support: true,
        check_iphone_launch_568h_2x_icon_support: true,
        check_iphone_launch_667h_2x_icon_support: true,
        check_iphone_launch_portrait_736h_3x_icon_support: true,
        check_iphone_launch_landscape_736h_3x_icon_support: true,
        check_ipad_launch_portrait_1x_icon_support: true,
        check_ipad_launch_landscape_1x_icon_support: true,
        check_ipad_launch_portrait_2x_icon_support: true,
        check_ipad_launch_landscape_2x_icon_support: true
    ).decorate
  end

  def destroy
    @project.destroy
    flash[:notice] = I18n.t('project.success.deleted')
    path = current_user.reload.has_projects? ? show_project_path(current_user.reload.projects.first) : new_project_path
    redirect_to path
  end

  def update
    update_project_with_params(@project)

    if @project.save
      save_icons(@project)
      save_provision(@project)
      save_configuration_armv7(@project)
      save_configuration_arm64(@project)

      result = {success: true, message: I18n.t('project.success.updated'), url: show_project_path(@project, message: I18n.t('project.success.updated'))}
    else
      result = {success: false, message: @project.errors.full_messages}
    end

    render json: result
  end

  private
  def project_params
    params.permit(:id, :name, :app_name, :zip_name, :last_version, :platform_version, :platform_build, :xcode_version,
                  :xcode_build, :bundle_id, :check_armv7_support, :check_arm64_support, :check_pie_support,
                  :with_extensions,
                  :check_provision => [:enabled, :value],
                  :check_configuration_armv7 => [:enabled, :value],
                  :check_configuration_arm64 => [:enabled, :value],
                  :check_iphone_1x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_2x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_3x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_1x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_2x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_settings_1x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_settings_2x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_settings_1x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_settings_2x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_launch_1x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_launch_2x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_launch_568h_2x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_launch_667h_2x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_launch_portrait_736h_3x_icon_support => [:enabled, :name, :md5],
                  :check_iphone_launch_landscape_736h_3x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_launch_portrait_1x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_launch_landscape_1x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_launch_portrait_2x_icon_support => [:enabled, :name, :md5],
                  :check_ipad_launch_landscape_2x_icon_support => [:enabled, :name, :md5]
    )
  end

  def find_project!
    @project = Project.find_by(id: project_params[:id], user: current_user)
    if @project.blank?
      flash[:alert] = I18n.t('project.failure.not_found') % project_params[:id]
      path = show_project_path(current_user.projects.first)

      redirect_to path
    end
  end

  def find_icon!(property)
    if to_bool!((project_params[property][:enabled] rescue nil))
      name = project_params[property][:name] rescue nil
      md5 = project_params[property][:md5] rescue nil

      if name.present? and md5.present?
        return Struct.new(:name, :md5).new(name, md5)
      else
        render json: {success: false, message: [I18n.t('project.%s.failure' % property)]}
      end
    end
  end

  def find_provision!
    if to_bool!((project_params[:check_provision][:enabled] rescue nil))
      value = project_params[:check_provision][:value] rescue nil

      if value.present?
        return Struct.new(:value).new(value)
      else
        render json: {success: false, message: [I18n.t('project.%s.failure' % :check_provision)]}
      end
    end
  end

  def find_configuration_armv7!
    if to_bool!((project_params[:check_configuration_armv7][:enabled] rescue nil))
      value = project_params[:check_configuration_armv7][:value] rescue nil

      if value.present?
        return Struct.new(:value).new(value)
      else
        render json: {success: false, message: [I18n.t('project.%s.failure' % :check_configuration_armv7)]}
      end
    end
  end

  def find_configuration_arm64!
    if to_bool!((project_params[:check_configuration_arm64][:enabled] rescue nil))
      value = project_params[:check_configuration_arm64][:value] rescue nil

      if value.present?
        return Struct.new(:value).new(value)
      else
        render json: {success: false, message: [I18n.t('project.%s.failure' % :check_configuration_arm64)]}
      end
    end
  end

  def find_icons!
    [
        :check_iphone_1x_icon_support,
        :check_iphone_2x_icon_support,
        :check_iphone_3x_icon_support,

        :check_ipad_1x_icon_support,
        :check_ipad_2x_icon_support,

        :check_iphone_settings_1x_icon_support,
        :check_iphone_settings_2x_icon_support,
        :check_ipad_settings_1x_icon_support,
        :check_ipad_settings_2x_icon_support,

        :check_iphone_launch_1x_icon_support,
        :check_iphone_launch_2x_icon_support,
        :check_iphone_launch_568h_2x_icon_support,
        :check_iphone_launch_667h_2x_icon_support,
        :check_iphone_launch_portrait_736h_3x_icon_support,
        :check_iphone_launch_landscape_736h_3x_icon_support,

        :check_ipad_launch_portrait_1x_icon_support,
        :check_ipad_launch_landscape_1x_icon_support,
        :check_ipad_launch_portrait_2x_icon_support,
        :check_ipad_launch_landscape_2x_icon_support

    ].map { |property| find_icon!(property) }
  end

  def save_icons(project)
    save_icon(project, :iphone_1x)
    save_icon(project, :iphone_2x)
    save_icon(project, :iphone_3x)

    save_icon(project, :ipad_1x)
    save_icon(project, :ipad_2x)

    save_icon(project, :iphone_settings_1x)
    save_icon(project, :iphone_settings_2x)
    save_icon(project, :ipad_settings_1x)
    save_icon(project, :ipad_settings_2x)

    save_icon(project, :iphone_launch_1x)
    save_icon(project, :iphone_launch_2x)
    save_icon(project, :iphone_launch_568h_2x)
    save_icon(project, :iphone_launch_667h_2x)
    save_icon(project, :iphone_launch_portrait_736h_3x)
    save_icon(project, :iphone_launch_landscape_736h_3x)

    save_icon(project, :ipad_launch_portrait_1x)
    save_icon(project, :ipad_launch_landscape_1x)
    save_icon(project, :ipad_launch_portrait_2x)
    save_icon(project, :ipad_launch_landscape_2x)
  end

  def save_icon(project, icon)
    _check_icon_support_with_question_mark = ("check_%s_icon_support?" % icon).to_sym
    _check_icon_support = ("check_%s_icon_support" % icon).to_sym

    ProjectIcon.where(project: project, type: IconType.send(icon)).delete_all #clean icons
    if project.send(_check_icon_support_with_question_mark)
      ProjectIcon.create!(project: project, type: IconType.send(icon), name: find_icon!(_check_icon_support).name, md5: find_icon!(_check_icon_support).md5)
    end
  end

  def save_provision(project)
    ProjectProvision.where(project: project).delete_all #clean icons
    if project.check_provision?
      ProjectProvision.create!(project: project, value: find_provision!.value)
    end
  end

  def save_configuration_armv7(project)
    ProjectConfiguration.where(project: project, type: ConfigurationType.armv7).delete_all #clean icons
    if project.check_configuration_armv7?
      ProjectConfiguration.create!(project: project, type: ConfigurationType.armv7, value: find_configuration_armv7!.value)
    end
  end

  def save_configuration_arm64(project)
    ProjectConfiguration.where(project: project, type: ConfigurationType.arm64).delete_all #clean icons
    if project.check_configuration_arm64?
      ProjectConfiguration.create!(project: project, type: ConfigurationType.arm64, value: find_configuration_arm64!.value)
    end
  end

  def update_project_with_params(project)
    project.name = project_params[:name]
    project.app_name = project_params[:app_name]
    project.zip_name = project_params[:zip_name]
    project.last_version = project_params[:last_version]
    project.platform_version = project_params[:platform_version]
    project.platform_build = project_params[:platform_build]
    project.xcode_version = project_params[:xcode_version]
    project.xcode_build = project_params[:xcode_build]
    project.bundle_id = project_params[:bundle_id]
    project.check_armv7_support = to_bool!(project_params[:check_armv7_support])
    project.check_arm64_support = to_bool!(project_params[:check_arm64_support])
    project.check_pie_support = to_bool!(project_params[:check_pie_support])
    project.with_extensions = to_bool!(project_params[:with_extensions])
    project.check_provision = to_bool!((project_params[:check_provision][:enabled] rescue nil))

    project.check_configuration_armv7 = to_bool!((project_params[:check_configuration_armv7][:enabled] rescue nil))
    project.check_configuration_arm64 = to_bool!((project_params[:check_configuration_arm64][:enabled] rescue nil))

    project.check_iphone_1x_icon_support = to_bool!((project_params[:check_iphone_1x_icon_support][:enabled] rescue nil))
    project.check_iphone_2x_icon_support = to_bool!((project_params[:check_iphone_2x_icon_support][:enabled] rescue nil))
    project.check_iphone_3x_icon_support = to_bool!((project_params[:check_iphone_3x_icon_support][:enabled] rescue nil))

    project.check_ipad_1x_icon_support = to_bool!((project_params[:check_ipad_1x_icon_support][:enabled] rescue nil))
    project.check_ipad_2x_icon_support = to_bool!((project_params[:check_ipad_2x_icon_support][:enabled] rescue nil))

    project.check_iphone_settings_1x_icon_support = to_bool!((project_params[:check_iphone_settings_1x_icon_support][:enabled] rescue nil))
    project.check_iphone_settings_2x_icon_support = to_bool!((project_params[:check_iphone_settings_2x_icon_support][:enabled] rescue nil))
    project.check_ipad_settings_1x_icon_support = to_bool!((project_params[:check_ipad_settings_1x_icon_support][:enabled] rescue nil))
    project.check_ipad_settings_2x_icon_support = to_bool!((project_params[:check_ipad_settings_2x_icon_support][:enabled] rescue nil))

    project.check_iphone_launch_1x_icon_support = to_bool!((project_params[:check_iphone_launch_1x_icon_support][:enabled] rescue nil))
    project.check_iphone_launch_2x_icon_support = to_bool!((project_params[:check_iphone_launch_2x_icon_support][:enabled] rescue nil))
    project.check_iphone_launch_568h_2x_icon_support = to_bool!((project_params[:check_iphone_launch_568h_2x_icon_support][:enabled] rescue nil))
    project.check_iphone_launch_667h_2x_icon_support = to_bool!((project_params[:check_iphone_launch_667h_2x_icon_support][:enabled] rescue nil))
    project.check_iphone_launch_portrait_736h_3x_icon_support = to_bool!((project_params[:check_iphone_launch_portrait_736h_3x_icon_support][:enabled] rescue nil))
    project.check_iphone_launch_landscape_736h_3x_icon_support = to_bool!((project_params[:check_iphone_launch_landscape_736h_3x_icon_support][:enabled] rescue nil))

    project.check_ipad_launch_portrait_1x_icon_support = to_bool!((project_params[:check_ipad_launch_portrait_1x_icon_support][:enabled] rescue nil))
    project.check_ipad_launch_landscape_1x_icon_support = to_bool!((project_params[:check_ipad_launch_landscape_1x_icon_support][:enabled] rescue nil))
    project.check_ipad_launch_portrait_2x_icon_support = to_bool!((project_params[:check_ipad_launch_portrait_2x_icon_support][:enabled] rescue nil))
    project.check_ipad_launch_landscape_2x_icon_support = to_bool!((project_params[:check_ipad_launch_landscape_2x_icon_support][:enabled] rescue nil))
  end

end
