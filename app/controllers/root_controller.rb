# -*- encoding : utf-8 -*-
class RootController < ApplicationController
  before_action :check_user!

  def index
    if current_user.has_projects?
      redirect_to show_project_path(current_user.projects.first)
    else
      redirect_to new_project_path, notice: I18n.t('project.create_new')
    end
  end

  private
  def check_user!
    if not user_signed_in?
      redirect_to new_user_session_path
    end
  end
end
