# -*- encoding : utf-8 -*-

class ProjectDecorator < ApplicationDecorator
  decorates :project
  delegate_all

  def subtitle
    "Application name: %s, Last version: %s, Platform version: %s, Platform build: %s, XCode version: %s, XCode build: %s, Bundle ID: %s" % [
        model.app_name,
        model.last_version,
        model.platform_version,
        model.platform_build,
        model.xcode_version,
        model.xcode_build,
        model.bundle_id
    ]
  end

  def check_provision
    if model.check_provision?

      provision = model.provision
      if provision
        {enabled: true, value: provision.value}
      else
        {enabled: true, value: 'e73ebed50450ba4b742c23bfbdcfd49c'}
      end
    else
      {enabled: false, value: nil}
    end
  end

  def check_configuration_armv7
    if model.check_configuration_armv7?

      configuration = model.configurations.select { |c| c.type.armv7? }.first
      if configuration
        {enabled: true, value: configuration.value}
      else
        {enabled: true, value: 'e73ebed50450ba4b742c23bfbdcfd49c'}
      end
    else
      {enabled: false, value: nil}
    end
  end

  def check_configuration_arm64
    if model.check_configuration_arm64?

      configuration = model.configurations.select { |c| c.type.arm64? }.first
      if configuration
        {enabled: true, value: configuration.value}
      else
        {enabled: true, value: 'e73ebed50450ba4b742c23bfbdcfd49c'}
      end
    else
      {enabled: false, value: nil}
    end
  end

  def check_iphone_1x_icon_support
    if model.check_iphone_1x_icon_support
      icon = model.icons.select { |icon| icon.type.iphone_1x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon60x60@1x.png',
            md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_iphone_2x_icon_support
    if model.check_iphone_2x_icon_support
      icon = model.icons.select { |icon| icon.type.iphone_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon60x60@2x.png',
            md5: 'e73ebed50450ba4b742c23bfbdcfd49c'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end
  end

  def check_iphone_3x_icon_support
    if model.check_iphone_3x_icon_support
      icon = model.icons.select { |icon| icon.type.iphone_3x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon60x60@3x.png',
            md5: 'd676993636dd01b57deb8f8d18df3299'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_1x_icon_support
    if model.check_ipad_1x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_1x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon76x76~ipad.png',
            md5: 'efadc0a3dcfe7d8243ebaed6c05e3ef4'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_2x_icon_support
    if model.check_ipad_2x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon76x76@2x~ipad.png',
            md5: '73533b632acafccbd51505c506085a76'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end
  end

  def check_iphone_settings_1x_icon_support
    if model.check_iphone_settings_1x_icon_support
      icon = model.icons.select { |icon| icon.type.iphone_settings_1x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon29x29.png',
            md5: '6270381645d8a22adab8af5a5f8f7c29'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_iphone_settings_2x_icon_support
    if model.check_iphone_settings_2x_icon_support
      icon = model.icons.select { |icon| icon.type.iphone_settings_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon29x29@2x.png',
            md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_settings_1x_icon_support
    if model.check_ipad_settings_1x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_settings_1x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon29x29~ipad.png',
            md5: '6270381645d8a22adab8af5a5f8f7c29'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_settings_2x_icon_support
    if model.check_ipad_settings_2x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_settings_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'AppIcon29x29@2x~ipad.png',
            md5: 'bb39a9fbdee6ed15eccf4183ea64cea9'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_iphone_launch_1x_icon_support
    if model.check_iphone_launch_1x_icon_support?
      icon = model.icons.select { |icon| icon.type.iphone_launch_1x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage.png',
            md5: '89817fb95ab12d69b0f010450ad29428'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_iphone_launch_2x_icon_support
    if model.check_iphone_launch_2x_icon_support?
      icon = model.icons.select { |icon| icon.type.iphone_launch_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage@2x.png',
            md5: '278d5a9802193ae28d8698ef5dc7240a'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_iphone_launch_568h_2x_icon_support
    if model.check_iphone_launch_568h_2x_icon_support?
      icon = model.icons.select { |icon| icon.type.iphone_launch_568h_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-568h@2x.png',
            md5: '6e41359534fd8a60f4a419c5e2caa429'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_iphone_launch_667h_2x_icon_support
    if model.check_iphone_launch_667h_2x_icon_support?
      icon = model.icons.select { |icon| icon.type.iphone_launch_667h_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-800-667h@2x.png',
            md5: 'b7cf9b3955f9b49ff4ecd39577947861'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_iphone_launch_portrait_736h_3x_icon_support
    if model.check_iphone_launch_portrait_736h_3x_icon_support
      icon = model.icons.select { |icon| icon.type.iphone_launch_portrait_736h_3x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-800-Portrait-736h@3x.png',
            md5: '45e1109fedc69abd44a8f3452659feb1'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end


  def check_iphone_launch_landscape_736h_3x_icon_support
    if model.check_iphone_launch_landscape_736h_3x_icon_support
      icon = model.icons.select { |icon| icon.type.iphone_launch_landscape_736h_3x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-800-Landscape-736h@3x.png',
            md5: '3a26e026cf7000a50d98f6ab54310b19'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_launch_portrait_1x_icon_support
    if model.check_ipad_launch_portrait_1x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_launch_portrait_1x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-Portrait~ipad.png',
            md5: 'dcf60180b258defb566de5042ce79bbd'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_launch_landscape_1x_icon_support
    if model.check_ipad_launch_landscape_1x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_launch_landscape_1x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-Landscape~ipad.png',
            md5: '36047b1203297e4c7288092ff3e9aa62'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_launch_portrait_2x_icon_support
    if model.check_ipad_launch_portrait_2x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_launch_portrait_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-Portrait@2x~ipad.png',
            md5: '2217217bac18a20935025fe19c27be93'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

  def check_ipad_launch_landscape_2x_icon_support
    if model.check_ipad_launch_landscape_2x_icon_support
      icon = model.icons.select { |icon| icon.type.ipad_launch_landscape_2x? }.first

      if icon
        {
            enabled: true,
            name: icon.name,
            md5: icon.md5
        }
      else
        {
            enabled: true,
            name: 'LaunchImage-Landscape@2x~ipad.png',
            md5: 'bf91364ae71b4dab8a0c79e64903b220'
        }
      end
    else
      {
          enabled: false,
          name: nil,
          md5: nil
      }
    end

  end

end
