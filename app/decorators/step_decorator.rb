# -*- encoding : utf-8 -*-
class StepDecorator < ApplicationDecorator
  delegate_all

  def name
    check = AppstoreCheck.find_by!(id: model.id)
    if check.icon_type.present?
      "%s (%s)" % [model.name, check.icon_type.size]
    else
      model.name
    end
  end

end
