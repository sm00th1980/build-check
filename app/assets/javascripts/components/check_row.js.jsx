var CheckRow = React.createClass({

    _status: function () {
        switch (this.props.check.status) {
            case 'running':
                return (<span className="label bg-primary">RUNNING</span>);
                break;
            case 'passed':
                return (<span className="label bg-success">PASSED</span>);
                break;
            case 'failed':
                return (<span className="label bg-danger">FAILED</span>);
                break;
            default:
                return (<span className="label bg-dark">UNKNOWN</span>);
        }
    },

    render: function () {
        return (
            <tr onClick={this.props.onClick}>
                <td>{this.props.check.name}</td>
                <td>{this._status()}</td>
            </tr>
        )
    }

});
