var InputField = React.createClass({

    _onChange: function (e) {
        this.props.onChange(this.props.property, e.target.value); //send value to parent component
    },

    render: function () {
        return (
            <input type="text" className="form-control" required="" value={this.props.value} onChange={this._onChange}/>
        )
    }
});