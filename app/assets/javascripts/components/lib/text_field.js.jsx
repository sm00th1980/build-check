var TextField = React.createClass({

    _onChange: function (e) {
        this.props.onChange(this.props.property, e.target.value); //send value to parent component
    },

    render: function () {
        return (
            <textarea className="form-control" rows={this.props.rows} placeholder={this.props.placeholder}
                      value={this.props.value} onChange={this._onChange}/>
        )
    }
});