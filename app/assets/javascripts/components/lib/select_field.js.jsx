var SelectField = React.createClass({

    _onChange: function (e) {
        this.props.onChange(this.props.property, e.target.value); //send value to parent component
    },

    //_findByID: function (id) {
    //    var self = this;
    //    return $.grep(self.props.params.options, function (el) {
    //        return el.id == id;
    //    })[0];
    //},

    render: function () {
        var option = function (el) {
            return <option value={el.id}>{el.name}</option>
        };

        return (
            <select className="form-control" value={this.props.value} onChange={this._onChange}>
                {this.props.options.map(option)}
            </select>
        )
    }
});