var CheckComponent = React.createClass({

    getInitialState: function () {
        return {
            general_checks: this.props.general_checks,
            icon_checks: this.props.icon_checks,
            reset_general_checks: $.extend(true, [], this.props.general_checks), //need a clone for resetting checks
            reset_icon_checks: $.extend(true, [], this.props.icon_checks), //need a clone for resetting checks
            show_result: false,
            selected_check: null
        };
    },

    componentDidMount: function () {
        EventSystem.subscribe('checking.start', this._startChecking);
        EventSystem.subscribe('file.selected', this._checksReset);
    },

    _checksReset: function () {
        this.setState({
            general_checks: this.state.reset_general_checks,
            icon_checks: this.state.reset_icon_checks,
            selected_check: null
        }); //reset old results
    },

    _startChecking: function (obj) {
        var self = this;

        $.eventsource("close", "event-source-label");

        $.eventsource({
            label: "event-source-label",
            url: self.props.live_url,
            data: {id: obj.file_id},
            dataType: "json",
            message: function (response) {
                if (response.finished) {
                    Notification.notice('Checking completed.');

                    //closing stream
                    $.eventsource("close", "event-source-label");
                } else {
                    self.setState({general_checks: self._updateChecks(self.state.general_checks, response)});
                    self.setState({icon_checks: self._updateChecks(self.state.icon_checks, response)});
                }
            }

        });
    },

    _updateChecks: function (checks, new_check) {
        var checks_copy = $.extend(true, [], checks);
        var index = _.findIndex(checks_copy, function (check) {
            return check.id === new_check.id;
        });

        if (index >= 0) {
            checks_copy[index] = new_check;
        }

        return checks_copy;
    },

    _checkSelected: function () {
        var self = this;
        return function (check) {
            self.setState({show_result: true, selected_check: check});
        }
    },

    _showCheck: function () {
        this.setState({show_result: false, selected_check: null});
    },

    render: function () {
        if (this.state.show_result) {
            return (
                <CheckResult check={this.state.selected_check} onClick={this._showCheck}/>
            )
        } else {
            return (
                <div className="row">
                    <div className="col-sm-6">
                        <CheckTable
                            header="General checks"
                            checks={this.state.general_checks}
                            onClick={this._checkSelected()}
                            />
                    </div>

                    <div className="col-sm-6">
                        <CheckTable
                            header="Icon checks"
                            checks={this.state.icon_checks}
                            onClick={this._checkSelected()}
                            />
                    </div>
                </div>
            )
        }
    }

});

