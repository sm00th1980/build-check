var UploadForm = React.createClass({

    getInitialState: function () {
        return {
            version: this.props.version,
            file: null,
            disabled: true, //disable button for start checking
            file_id: null
        };
    },

    _onFileSelect: function (e) {
        var file = e.target.value.replace(/\\/g, '/').replace(/.*\//, '');
        this.setState({file: file, disabled: false});
        this.handleSubmit(file);
    },

    handleSubmit: function (event) {
        var file = $('input[id="file"]')[0].files[0];
        var self = this;

        var data = new FormData();
        data.append('build', file);

        var notify = Notification.upload('Uploading started.', function () {
            Notification.notice('Start checking...');
            EventSystem.publish('checking.start', {file_id: self.state.file_id});
        });

        $.ajax({
            url: this.props.upload_url,
            type: "POST",
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            xhr: function () {
                jqxhr = $.ajaxSettings.xhr();
                jqxhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        notify.update({'progress': percentComplete * 100.0});
                    }
                }, false);

                return jqxhr;
            },
            success: function (response) {
                if (response.success) {
                    self.setState({file_id: response.file_id});

                    notify.update({'message': 'Uploading completed.'});
                    notify.close();
                } else {
                    if ('message' in response) {
                        Notification.error(response.message);
                    } else {
                        Notification.error('Error upload file to backend. Possible backend failure.');
                    }
                }
            },
            error: function () {
                Notification.error('Error upload file to backend. Possible network failure.');
            }
        });

        this._resetFileInput();
    },

    _resetFileInput: function () {
        var input = $('input[id="file"]');
        input.replaceWith(input.val('').clone(true));
    },

    render: function () {
        return (
            <form method="post" encType="multipart/form-data">
                <div className="bg-light lter b-b wrapper-md">

                    <a className="btn btn-danger pull-right fileFormButton"
                       data-confirm="Are you sure to delete project?"
                       rel="nofollow" data-method="delete" href={this.props.delete_url}>Delete</a>

                    <a className="btn btn-default pull-right m-r-sm fileFormButton"
                       href={this.props.edit_url}>Edit</a>

                    <div className="fileUpload btn btn-primary pull-right fileFormButton">
                        <span>Upload build</span>
                        <input type="file" id="file" accept=".zip" className="upload" onChange={this._onFileSelect}/>
                    </div>

                    <h1 className="m-n font-thin h3">{this.props.title}</h1>
                    <small className="text-muted">{this.props.subtitle}</small>
                </div>
            </form>
        )
    }

});