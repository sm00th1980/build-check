var ProjectPlatform = React.createClass({

    render: function () {
        return (
            <div className="row">
                <div className="col-md-3">
                    <label>Platform version <em className="text-muted">(example: 8.1)</em></label>
                    <InputField value={this.props.project.platform_version}
                                onChange={this.props.onChange}
                                property='platform_version'/>
                </div>

                <div className="col-md-3">
                    <label>Platform build <em className="text-muted">(example: 12B411)</em></label>
                    <InputField value={this.props.project.platform_build}
                                onChange={this.props.onChange}
                                property='platform_build'/>
                </div>

                <div className="col-md-3">
                    <label>XCode version <em className="text-muted">(example: 0611)</em></label>
                    <InputField value={this.props.project.xcode_version}
                                onChange={this.props.onChange}
                                property='xcode_version'/>
                </div>

                <div className="col-md-3">
                    <label>XCode build <em className="text-muted">(example: 12B411)</em></label>
                    <InputField value={this.props.project.xcode_build}
                                onChange={this.props.onChange}
                                property='xcode_build'/>
                </div>
            </div>
        )
    }

});

