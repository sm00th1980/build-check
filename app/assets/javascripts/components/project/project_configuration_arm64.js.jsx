var ProjectConfigurationArm64 = React.createClass({

    getInitialState: function () {
        return {
            check_configuration: $.extend(true, {}, this.props.check_configuration)
        };
    },

    _input: function () {
        if (this.props.check_configuration.enabled) {
            return (
                <div>
                    <div className="col-md-8">
                        <label>Value</label>
                        <TextField value={this.state.check_configuration.value} onChange={this._onChangeValue}
                                   placehoolder="Configuration ARM64 value" rows="50"/>
                    </div>
                </div>
            )
        } else {
            return null;
        }
    },

    _onChangeValue: function (property, value) {
        var updated_check_configuration = this.state.check_configuration;
        updated_check_configuration['value'] = value;

        this.props.onChange(this.props.property, updated_check_configuration);
    },

    _onDisable: function (property, value) {
        var updated_check_configuration = this.state.check_configuration;
        updated_check_configuration['enabled'] = (value === 'true'); //convert string to boolean
        this.props.onChange(this.props.property, updated_check_configuration);
    },

    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col-md-4">
                        <label>{this.props.label}</label>
                        <SelectField
                            value={this.props.check_configuration.enabled}
                            options={this.props.options}
                            onChange={this._onDisable}
                            />
                    </div>

                    {this._input()}
                </div>
            </div>
        )
    }
});
