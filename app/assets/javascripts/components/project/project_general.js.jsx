var ProjectGeneral = React.createClass({

    render: function () {
        return (
            <div className="row">
                <div className="col-md-2">
                    <label>Project name <em className="text-muted">(allow 'a-zA-Z0-9')</em></label>
                    <InputField value={this.props.project.name}
                                onChange={this.props.onChange}
                                property='name'/>
                </div>

                <div className="col-md-2">
                    <label>Zip name <em className="text-muted">(allow 'a-zA-Z0-9')</em></label>
                    <InputField value={this.props.project.zip_name}
                                onChange={this.props.onChange}
                                property='zip_name'/>
                </div>

                <div className="col-md-2">
                    <label>Application name <em className="text-muted">(allow 'a-zA-Z0-9')</em></label>
                    <InputField value={this.props.project.app_name}
                                onChange={this.props.onChange}
                                property='app_name'/>
                </div>

                <div className="col-md-2">
                    <label>Application last version <em className="text-muted">(example: 1.0.24)</em></label>
                    <InputField value={this.props.project.last_version}
                                onChange={this.props.onChange}
                                property='last_version'/>
                </div>

                <div className="col-md-2">
                    <label>Bundle ID <em className="text-muted">(example: com.mimedia.iOSv2)</em></label>
                    <InputField value={this.props.project.bundle_id}
                                onChange={this.props.onChange}
                                property='bundle_id'/>
                </div>

                <div className="col-md-2">
                    <label>Use extensions <em className="text-muted">(example: YES or NO)</em></label>
                    <SelectField
                        property='with_extensions'
                        value={this.props.project.with_extensions}
                        options={this.props.options}
                        onChange={this.props.onChange}
                        />
                </div>
            </div>
        )
    }

});

