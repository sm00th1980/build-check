var ProjectIcon = React.createClass({

    getInitialState: function () {
        return {
            check_icon: $.extend(true, {}, this.props.check_icon)
        };
    },

    _input: function () {
        if (this.props.check_icon.enabled) {
            return (
                <div>
                    <div className="col-md-4">
                        <label>Name <em className="text-muted">(example: {this.props.helpName})</em></label>
                        <InputField value={this.state.check_icon.name} onChange={this._onChangeName}/>
                    </div>

                    <div className="col-md-4">
                        <label>MD5</label>
                        <InputField value={this.state.check_icon.md5} onChange={this._onChangeMD5}/>
                    </div>
                </div>
            )
        } else {
            return null;
        }
    },

    _onChangeName: function (property, value) {
        var updated_check_icon = this.state.check_icon;
        updated_check_icon['name'] = value;
        this.props.onChange(this.props.property, updated_check_icon);
    },

    _onChangeMD5: function (property, value) {
        var updated_check_icon = this.state.check_icon;
        updated_check_icon['md5'] = value;
        this.props.onChange(this.props.property, updated_check_icon);
    },

    _onDisable: function (property, value) {
        var updated_check_icon = this.state.check_icon;
        updated_check_icon['enabled'] = (value === 'true'); //convert string to boolean
        this.props.onChange(this.props.property, updated_check_icon);
    },

    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col-md-4">
                        <label>{this.props.label}</label>
                        <SelectField
                            value={this.props.check_icon.enabled}
                            options={this.props.options}
                            onChange={this._onDisable}
                            />
                    </div>

                    {this._input()}
                </div>
            </div>
        )
    }
});
