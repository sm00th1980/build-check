var ProjectPanel = React.createClass({

    getInitialState: function () {
        return {
            project: $.extend(true, {}, this.props.project)
        };
    },

    _onChange: function (property, value) {
        var updated_project = this.state.project;
        updated_project[property] = value;

        this.setState({project: updated_project});
    },

    _save: function () {
        $.ajax({
            url: this.props.save_url,
            type: "POST",
            data: this.state.project,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    window.location = response.url;
                } else {
                    Notification.error(response.message);
                }
            },
            error: function () {
                Notification.error('Error saving project to backend. Possible backend failure.');
            }
        });
    },

    render: function () {
        return (
            <div>
                <div className="bg-light lter b-b wrapper-md">
                    <button className="btn btn-primary pull-right btn-sm"
                            onClick={this._save}>{this.props.button_name}</button>
                    <h1 className="m-n font-thin h3">{this.props.header}</h1>
                </div>

                <div className="wrapper-md">
                    <div className="tab-container">
                        <ul className="nav nav-tabs" data-tabs="tabs">
                            <li className="active">
                                <a href="#general" data-toggle="tab">General</a>
                            </li>
                            <li>
                                <a href="#platform" data-toggle="tab">Platform</a>
                            </li>
                            <li>
                                <a href="#architecture" data-toggle="tab">Architecture</a>
                            </li>
                            <li>
                                <a href="#provision" data-toggle="tab">Provision</a>
                            </li>
                            <li>
                                <a href="#configuration_armv7" data-toggle="tab">Configuration ARMv7</a>
                            </li>
                            <li>
                                <a href="#configuration_arm64" data-toggle="tab">Configuration ARM64</a>
                            </li>
                            <li>
                                <a href="#iphone_icons" data-toggle="tab">iPhone icons</a>
                            </li>
                            <li>
                                <a href="#ipad_icons" data-toggle="tab">iPad icons</a>
                            </li>
                            <li>
                                <a href="#iphone_settings_icons" data-toggle="tab">iPhone icons for settings</a>
                            </li>
                            <li>
                                <a href="#ipad_settings_icons" data-toggle="tab">iPad icons for settings</a>
                            </li>
                            <li>
                                <a href="#iphone_launch_icons" data-toggle="tab">iPhone images for launch</a>
                            </li>
                            <li>
                                <a href="#ipad_launch_icons" data-toggle="tab">iPad images for launch</a>
                            </li>
                        </ul>

                        <div className="tab-content">
                            <div className="tab-pane active" id="general">
                                <ProjectGeneral project={this.state.project}
                                                options={this.props.options}
                                                onChange={this._onChange}/>
                            </div>

                            <div className="tab-pane" id="platform">
                                <ProjectPlatform project={this.state.project} onChange={this._onChange}/>
                            </div>

                            <div className="tab-pane" id="architecture">
                                <ProjectArchitecture project={this.state.project}
                                                     options={this.props.options}
                                                     onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="provision">
                                <ProjectProvision check_provision={this.state.project.check_provision}
                                                  label='Check provision'
                                                  property='check_provision'
                                                  options={this.props.options}
                                                  onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="configuration_armv7">
                                <ProjectConfigurationArmv7
                                    check_configuration={this.state.project.check_configuration_armv7}
                                    label='Check configuration ARMv7'
                                    property='check_configuration_armv7'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="configuration_arm64">
                                <ProjectConfigurationArm64
                                    check_configuration={this.state.project.check_configuration_arm64}
                                    label='Check configuration ARM64'
                                    property='check_configuration_arm64'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="iphone_icons">
                                <ProjectIcon check_icon={this.state.project.check_iphone_1x_icon_support}
                                             label='Check iPhone 1x icon support'
                                             helpName='AppIcon60x60@1x.png'
                                             helpMD5='e73ebed50450ba4b742c23bfbdcfd49c'
                                             property='check_iphone_1x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon check_icon={this.state.project.check_iphone_2x_icon_support}
                                             label='Check iPhone 2x icon support'
                                             helpName='AppIcon60x60@2x.png'
                                             helpMD5='e73ebed50450ba4b742c23bfbdcfd49c'
                                             property='check_iphone_2x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon check_icon={this.state.project.check_iphone_3x_icon_support}
                                             label='Check iPhone 3x icon support'
                                             helpName='AppIcon60x60@3x.png'
                                             helpMD5='d676993636dd01b57deb8f8d18df3299'
                                             property='check_iphone_3x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="ipad_icons">
                                <ProjectIcon check_icon={this.state.project.check_ipad_1x_icon_support}
                                             label='Check iPad 1x icon support'
                                             helpName='AppIcon76x76~ipad.png'
                                             helpMD5='efadc0a3dcfe7d8243ebaed6c05e3ef4'
                                             property='check_ipad_1x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon check_icon={this.state.project.check_ipad_2x_icon_support}
                                             label='Check iPad 2x icon support'
                                             helpName='AppIcon76x76@2x~ipad.png'
                                             helpMD5='73533b632acafccbd51505c506085a76'
                                             property='check_ipad_2x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="iphone_settings_icons">
                                <ProjectIcon check_icon={this.state.project.check_iphone_settings_1x_icon_support}
                                             label='Check iPhone settings 1x icon support'
                                             helpName='AppIcon29x29.png'
                                             helpMD5='6270381645d8a22adab8af5a5f8f7c29'
                                             property='check_iphone_settings_1x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon check_icon={this.state.project.check_iphone_settings_2x_icon_support}
                                             label='Check iPhone settings 2x icon support'
                                             helpName='AppIcon29x29@2x.png'
                                             helpMD5='bb39a9fbdee6ed15eccf4183ea64cea9'
                                             property='check_iphone_settings_2x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="ipad_settings_icons">
                                <ProjectIcon check_icon={this.state.project.check_ipad_settings_1x_icon_support}
                                             label='Check iPad settings 1x icon support'
                                             helpName='AppIcon29x29~ipad.png'
                                             helpMD5='6270381645d8a22adab8af5a5f8f7c29'
                                             property='check_ipad_settings_1x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon check_icon={this.state.project.check_ipad_settings_2x_icon_support}
                                             label='Check iPad settings 2x icon support'
                                             helpName='AppIcon29x29@2x~ipad.png'
                                             helpMD5='bb39a9fbdee6ed15eccf4183ea64cea9'
                                             property='check_ipad_settings_2x_icon_support'
                                             options={this.props.options}
                                             onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="iphone_launch_icons">
                                <ProjectIcon
                                    check_icon={this.state.project.check_iphone_launch_1x_icon_support}
                                    label='Check iPhone launch 1x image support'
                                    helpName='LaunchImage.png'
                                    helpMD5='89817fb95ab12d69b0f010450ad29428'
                                    property='check_iphone_launch_1x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_iphone_launch_2x_icon_support}
                                    label='Check iPhone launch 2x image support'
                                    helpName='LaunchImage@2x.png'
                                    helpMD5='278d5a9802193ae28d8698ef5dc7240a'
                                    property='check_iphone_launch_2x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_iphone_launch_568h_2x_icon_support}
                                    label='Check iPhone launch 568h 2x image support'
                                    helpName='LaunchImage-568h@2x.png'
                                    helpMD5='6e41359534fd8a60f4a419c5e2caa429'
                                    property='check_iphone_launch_568h_2x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_iphone_launch_667h_2x_icon_support}
                                    label='Check iPhone launch 667h 2x image support'
                                    helpName='LaunchImage-800-667h@2x.png'
                                    helpMD5='b7cf9b3955f9b49ff4ecd39577947861'
                                    property='check_iphone_launch_667h_2x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_iphone_launch_portrait_736h_3x_icon_support}
                                    label='Check iPhone launch portrait 736h 3x image support'
                                    helpName='LaunchImage-800-Portrait-736h@3x.png'
                                    helpMD5='45e1109fedc69abd44a8f3452659feb1'
                                    property='check_iphone_launch_portrait_736h_3x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_iphone_launch_landscape_736h_3x_icon_support}
                                    label='Check iPhone launch landscape 736h 3x image support'
                                    helpName='LaunchImage-800-Landscape-736h@3x.png'
                                    helpMD5='3a26e026cf7000a50d98f6ab54310b19'
                                    property='check_iphone_launch_landscape_736h_3x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />
                            </div>

                            <div className="tab-pane" id="ipad_launch_icons">
                                <ProjectIcon
                                    check_icon={this.state.project.check_ipad_launch_portrait_1x_icon_support}
                                    label='Check iPad launch portrait 1x image support'
                                    helpName='LaunchImage-Portrait~ipad.png'
                                    helpMD5='2217217bac18a20935025fe19c27be93'
                                    property='check_ipad_launch_portrait_1x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_ipad_launch_landscape_1x_icon_support}
                                    label='Check iPad launch landscape 1x image support'
                                    helpName='LaunchImage-Landscape~ipad.png'
                                    helpMD5='36047b1203297e4c7288092ff3e9aa62'
                                    property='check_ipad_launch_landscape_1x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_ipad_launch_portrait_2x_icon_support}
                                    label='Check iPad launch portrait 2x image support'
                                    helpName='LaunchImage-Portrait@2x~ipad.png'
                                    helpMD5='2217217bac18a20935025fe19c27be93'
                                    property='check_ipad_launch_portrait_2x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />

                                <br/>

                                <ProjectIcon
                                    check_icon={this.state.project.check_ipad_launch_landscape_2x_icon_support}
                                    label='Check iPad launch landscape 2x image support'
                                    helpName='LaunchImage-Landscape@2x~ipad.png'
                                    helpMD5='bf91364ae71b4dab8a0c79e64903b220'
                                    property='check_ipad_launch_landscape_2x_icon_support'
                                    options={this.props.options}
                                    onChange={this._onChange}
                                    />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});

