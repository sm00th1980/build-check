var ProjectProvision = React.createClass({

    getInitialState: function () {
        return {
            check_provision: $.extend(true, {}, this.props.check_provision)
        };
    },

    _input: function () {
        if (this.props.check_provision.enabled) {
            return (
                <div>
                    <div className="col-md-8">
                        <label>Value</label>
                        <TextField value={this.state.check_provision.value} onChange={this._onChangeValue}
                                   placehoolder="Provision value" rows="50"/>
                    </div>
                </div>
            )
        } else {
            return null;
        }
    },

    _onChangeValue: function (property, value) {
        var updated_check_provision = this.state.check_provision;
        updated_check_provision['value'] = value;

        this.props.onChange(this.props.property, updated_check_provision);
    },

    _onDisable: function (property, value) {
        var updated_check_provision = this.state.check_provision;
        updated_check_provision['enabled'] = (value === 'true'); //convert string to boolean
        this.props.onChange(this.props.property, updated_check_provision);
    },

    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col-md-4">
                        <label>{this.props.label}</label>
                        <SelectField
                            value={this.props.check_provision.enabled}
                            options={this.props.options}
                            onChange={this._onDisable}
                            />
                    </div>

                    {this._input()}
                </div>
            </div>
        )
    }
});
