var ProjectArchitecture = React.createClass({

    render: function () {
        return (
            <div className="row">
                <div className="col-md-4">
                    <label>Check ARMv7 support</label>
                    <SelectField
                        property='check_armv7_support'
                        value={this.props.project.check_armv7_support}
                        options={this.props.options}
                        onChange={this.props.onChange}
                        />
                </div>

                <div className="col-md-4">
                    <label>Check ARM64 support</label>
                    <SelectField
                        property='check_arm64_support'
                        value={this.props.project.check_arm64_support}
                        options={this.props.options}
                        onChange={this.props.onChange}
                        />
                </div>

                <div className="col-md-4">
                    <label>Check PIE support</label>
                    <SelectField property='check_pie_support'
                                 value={this.props.project.check_pie_support}
                                 options={this.props.options}
                                 onChange={this.props.onChange}
                        />
                </div>
            </div>
        )
    }

});

