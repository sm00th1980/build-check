var CheckResult = React.createClass({

    _status: function () {
        switch (this.props.check.status) {
            case 'running':
                return (<span className="pull-right label bg-primary inline m-t-sm">RUNNING</span>);
                break;
            case 'passed':
                return (<span className="pull-right label bg-success inline m-t-sm">PASSED</span>);
                break;
            case 'failed':
                return (<span className="pull-right label bg-danger inline m-t-sm">FAILED</span>);
                break;
            default:
                return (<span className="pull-right label bg-dark inline m-t-sm">UNKNOWN</span>);
        }
    },

    render: function () {
        return (
            <div>

                <div className="row">

                    <div className="col-md-12">

                        <div className="panel no-border">
                            <div className="panel-heading wrapper b-b b-light">
                                <a href="#" onClick={this.props.onClick} className="text-muted pull-right text-lg"><i
                                    className="icon-close"></i></a>
                                <h4 className="font-thin m-t-none m-b-none text-muted">Step</h4>
                            </div>

                            <div className="panel-body">
              <span className="clear">
                <span>{this.props.check.name}</span>
                  {this._status()}
                  <small className="text-muted clear text-ellipsis">{this.props.check.command}</small>
              </span>
                            </div>

                        </div>

                    </div>
                </div>

                <div className="row">

                    <div className="col-md-6">

                        <div className="panel no-border">
                            <div className="panel-heading wrapper b-b b-light">
                                <h4 className="font-thin m-t-none m-b-none text-muted">Expected</h4>
                            </div>

                            <div className="panel-body">
                                <div className="highlight">
                                    <pre><code className="xml hljs">{this.props.check.expected}</code></pre>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div className="col-md-6">

                        <div className="panel no-border">
                            <div className="panel-heading wrapper b-b b-light">
                                <h4 className="font-thin m-t-none m-b-none text-muted">Got</h4>
                            </div>

                            <div className="panel-body">
                                <div className="highlight">
                                    <pre><code className="xml hljs">{this.props.check.got}</code></pre>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        )
    }

});