var QueueTable = React.createClass({

    getInitialState: function () {
        return {
            jobs: this.props.jobs
        };
    },

    _unschedule: function (job_id) {
        var self = this;

        $.ajax({
            url: this.props.unschedule_url,
            type: "POST",
            data: {
                id: job_id
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    Notification.notice(response.message);
                    var jobs_ = _.filter(self.state.jobs, function (job) {
                        return job.id != job_id;
                    });

                    self.setState({jobs: jobs_});
                } else {
                    if ('message' in response) {
                        Notification.error(response.message);
                    } else {
                        Notification.error('Error unschedule job. Possible backend failure.');
                    }
                }
            },
            error: function () {
                Notification.error('Error unschedule job. Possible network failure.');
            }
        });
    },

    _newRow: function (job) {
        return (
            <QueueRow
                job={job}
                onUnschedule={this._unschedule}
                />
        )
    },

    render: function () {
        return (
            <table className="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Testrun</th>
                    <th>Job name</th>
                    <th>Status</th>
                    <th>Start</th>
                    <th>Duration</th>
                    <th>Scenario</th>
                    <th>Node</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {this.state.jobs.map(this._newRow, this)}
                </tbody>
            </table>
        )
    }

});