var CheckTable = React.createClass({

    _onClick: function (check) {
        var self = this;
        return function () {
            self.props.onClick(check);
        }
    },

    _newRow: function (check, index) {
        return (
            <CheckRow check={check} onClick={this._onClick(check)}/>
        )
    },

    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    {this.props.header}
                </div>
                <div className="table-responsive">
                    <table className="table b-t b-light table-hover">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th style={{width: '100px'}}>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.checks.map(this._newRow, this)}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

});

